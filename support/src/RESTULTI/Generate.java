package RESTULTI;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import com.entities.*;

public class Generate {
    public static String randomId() {
        return UUID.randomUUID().toString();
    }
    public static String strCode() {
        String[] random= UUID.randomUUID().toString().split("-");
        return (random[0] + random[random.length-1]);
    }

    public static String md5(String password) {
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (Byte aByte :bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }

            //Get complete hashed password in hex format
           return sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return password;
    }

    public static String getTimeUTC() {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(tz);
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    public static String formetDate(long currentTimeMillis) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date resultdate = new Date(currentTimeMillis);
        return sdf.format(resultdate);
    }
    public static String formetDateTime(long currentTimeMillis) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Date resultdate = new Date(currentTimeMillis);
        return sdf.format(resultdate);
    }

    public static String formatfromLong(long time, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);
        return dateFormat.format(cal.getTime());
    }

    public static long currentTime() {
        return System.currentTimeMillis();
    }
    public static String stringTime() {
        return  String.valueOf(System.currentTimeMillis());
    }

    public static String numberRandom() {
        Random rnd = new Random();
        char[] digits = new char[9];
        digits[0] = (char) (rnd.nextInt(9) + '1');
        for (int i = 1; i < digits.length; i++) {
            digits[i] = (char) (rnd.nextInt(10) + '0');
        }
        return new String(digits);
    }

    public static int intRandom() {
        Random rnd = new Random();
        char[] digits = new char[6];
        digits[0] = (char) (rnd.nextInt(9) + '1');
        for (int i = 1; i < digits.length; i++) {
            digits[i] = (char) (rnd.nextInt(10) + '0');
        }
        return Integer.parseInt(new String(digits));
    }

    public static long getEndOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);
        return cal.getTimeInMillis();
    }

    public static long getBeginningOfDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    //    SAU NAY SE UPDATE CACH CODE = GENERIC CLASS
    public static class MyComparator implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            Transaction p1 = (Transaction) o1;
            Transaction p2 = (Transaction) o2;
            if (p1.getPrice() > p2.getPrice()) return -1;
            if (p1.getPrice() == p2.getPrice()) return 0;
            else return 1;
        }
    }

    public static class MyComparator_Order_UpdatedTime implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            Order p1 = (Order) o1;
            Order p2 = (Order) o2;
            if (p1.getUpdated_at() > p2.getUpdated_at()) return -1;
            if (p1.getUpdated_at() == p2.getUpdated_at()) return 0;
            else return 1;
        }
    }


}

package entitiesSupport.publicAPI;

public class Ticker {
    private double Bid;
    private double Ask;
    private double Last;

    public Ticker(double bid, double ask, double last) {
        Bid = bid;
        Ask = ask;
        Last = last;
    }

    public Ticker() {
    }

    public double getBid() {
        return Bid;
    }

    public void setBid(double bid) {
        Bid = bid;
    }

    public double getAsk() {
        return Ask;
    }

    public void setAsk(double ask) {
        Ask = ask;
    }

    public double getLast() {
        return Last;
    }

    public void setLast(double last) {
        Last = last;
    }
}

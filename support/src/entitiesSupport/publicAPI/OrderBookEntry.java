package entitiesSupport.publicAPI;

public class OrderBookEntry {
    private double Quantity;
    private double Rate;

    public double getQuantity() {
        return Quantity;
    }

    public void setQuantity(double quantity) {
        Quantity = quantity;
    }

    public double getRate() {
        return Rate;
    }

    public void setRate(double rate) {
        Rate = rate;
    }
}

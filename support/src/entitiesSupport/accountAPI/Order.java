package entitiesSupport.accountAPI;

public class Order {
    private String OrderUuid;
    private String AccountId;
    private String Exchange;
    private String Type;
    private double Quantity;
    private double QuantityRemaining;
    private double Limit;
    private double Reserved;
    private double ReserveRemaining;
    private double CommissionReserved;
    private double CommissionReserveRemaining;
    private double CommissionPaid;
    private double Price;
    private String PricePerUnit;
    private String Opened;
    private String Closed;
    private Boolean IsOpen;
    private String Sentinel;
    private Boolean CancelInitiated;
    private Boolean ImmediateOrCancel;
    private Boolean IsConditional;
    private String Condition;
    private String ConditionTarget;

    public Order(String orderUuid, String accountId, String exchange, String type, double quantity, double quantityRemaining, double limit, double reserved, double reserveRemaining, double commissionReserved, double commissionReserveRemaining, double commissionPaid, double price, String pricePerUnit, String opened, String closed, Boolean isOpen, String sentinel, Boolean cancelInitiated, Boolean immediateOrCancel, Boolean isConditional, String condition, String conditionTarget) {
        OrderUuid = orderUuid;
        AccountId = accountId;
        Exchange = exchange;
        Type = type;
        Quantity = quantity;
        QuantityRemaining = quantityRemaining;
        Limit = limit;
        Reserved = reserved;
        ReserveRemaining = reserveRemaining;
        CommissionReserved = commissionReserved;
        CommissionReserveRemaining = commissionReserveRemaining;
        CommissionPaid = commissionPaid;
        Price = price;
        PricePerUnit = pricePerUnit;
        Opened = opened;
        Closed = closed;
        IsOpen = isOpen;
        Sentinel = sentinel;
        CancelInitiated = cancelInitiated;
        ImmediateOrCancel = immediateOrCancel;
        IsConditional = isConditional;
        Condition = condition;
        ConditionTarget = conditionTarget;
    }

    public Order() {
    }

    public String getOrderUuid() {
        return OrderUuid;
    }

    public void setOrderUuid(String orderUuid) {
        OrderUuid = orderUuid;
    }

    public String getAccountId() {
        return AccountId;
    }

    public void setAccountId(String accountId) {
        AccountId = accountId;
    }

    public String getExchange() {
        return Exchange;
    }

    public void setExchange(String exchange) {
        Exchange = exchange;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public double getQuantity() {
        return Quantity;
    }

    public void setQuantity(double quantity) {
        Quantity = quantity;
    }

    public double getQuantityRemaining() {
        return QuantityRemaining;
    }

    public void setQuantityRemaining(double quantityRemaining) {
        QuantityRemaining = quantityRemaining;
    }

    public double getLimit() {
        return Limit;
    }

    public void setLimit(double limit) {
        Limit = limit;
    }

    public double getReserved() {
        return Reserved;
    }

    public void setReserved(double reserved) {
        Reserved = reserved;
    }

    public double getReserveRemaining() {
        return ReserveRemaining;
    }

    public void setReserveRemaining(double reserveRemaining) {
        ReserveRemaining = reserveRemaining;
    }

    public double getCommissionReserved() {
        return CommissionReserved;
    }

    public void setCommissionReserved(double commissionReserved) {
        CommissionReserved = commissionReserved;
    }

    public double getCommissionReserveRemaining() {
        return CommissionReserveRemaining;
    }

    public void setCommissionReserveRemaining(double commissionReserveRemaining) {
        CommissionReserveRemaining = commissionReserveRemaining;
    }

    public double getCommissionPaid() {
        return CommissionPaid;
    }

    public void setCommissionPaid(double commissionPaid) {
        CommissionPaid = commissionPaid;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public String getPricePerUnit() {
        return PricePerUnit;
    }

    public void setPricePerUnit(String pricePerUnit) {
        PricePerUnit = pricePerUnit;
    }

    public String getOpened() {
        return Opened;
    }

    public void setOpened(String opened) {
        Opened = opened;
    }

    public String getClosed() {
        return Closed;
    }

    public void setClosed(String closed) {
        Closed = closed;
    }

    public Boolean getOpen() {
        return IsOpen;
    }

    public void setOpen(Boolean open) {
        IsOpen = open;
    }

    public String getSentinel() {
        return Sentinel;
    }

    public void setSentinel(String sentinel) {
        Sentinel = sentinel;
    }

    public Boolean getCancelInitiated() {
        return CancelInitiated;
    }

    public void setCancelInitiated(Boolean cancelInitiated) {
        CancelInitiated = cancelInitiated;
    }

    public Boolean getImmediateOrCancel() {
        return ImmediateOrCancel;
    }

    public void setImmediateOrCancel(Boolean immediateOrCancel) {
        ImmediateOrCancel = immediateOrCancel;
    }

    public Boolean getConditional() {
        return IsConditional;
    }

    public void setConditional(Boolean conditional) {
        IsConditional = conditional;
    }

    public String getCondition() {
        return Condition;
    }

    public void setCondition(String condition) {
        Condition = condition;
    }

    public String getConditionTarget() {
        return ConditionTarget;
    }

    public void setConditionTarget(String conditionTarget) {
        ConditionTarget = conditionTarget;
    }
}

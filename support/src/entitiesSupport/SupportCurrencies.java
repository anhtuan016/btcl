package entitiesSupport;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class SupportCurrencies {
    @Id
    private long id;
    @Index
    private String name;
    @Index
    private String type;
    @Index
    private int confirmation;
    @Index
    private double TxFee;

    public SupportCurrencies() {
        id = System.currentTimeMillis();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(int confirmation) {
        this.confirmation = confirmation;
    }

    public double getTxFee() {
        return TxFee;
    }

    public void setTxFee(double txFee) {
        TxFee = txFee;
    }
}

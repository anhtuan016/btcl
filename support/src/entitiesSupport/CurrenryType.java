package entitiesSupport;

public enum CurrenryType {
    Ethereum(1, "ETH"),
    Bitcoin(2, "BTC"),
    Ripple(3, "XRP"),
    Bitcoin_Cash(4, "BCH"),
    Tether(5, "USDT");

    private final int number;
    private final String currenry;

    CurrenryType(int number, String currenry) {
        this.number = number;
        this.currenry = currenry;

    }

    public int getNumber() {
        return number;
    }

    public String getCurrenry() {
        return currenry;
    }

    public static String getCurrery(int number) {
        for (CurrenryType type : CurrenryType.values()) {
            if (type.number == number) {
                return type.getCurrenry();
            }
        }
        return null;
    }
    public static String getSymbol(int number) {
        for (CurrenryType type : CurrenryType.values()) {
            if (type.number == number) {
                return type.toString();
            }
        }
        return null;
    }
    public static int getint(String number) {
        for (CurrenryType type : CurrenryType.values()) {
            if (type.currenry.equals(number)) {
                return type.number;
            }
        }
        return 0;
    }

}

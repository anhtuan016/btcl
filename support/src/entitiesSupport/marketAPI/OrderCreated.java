package entitiesSupport.marketAPI;

public class OrderCreated {
    private long uuid;

    public long getUuid() {
        return uuid;
    }

    public void setUuid(long uuid) {
        this.uuid = uuid;
    }
}

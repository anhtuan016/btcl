package entitiesSupport;

import RESTULTI.Generate;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.UUID;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
public class APICredentical {
    @Id
    private String APIKey;
    @Index
    private String APISecretKey;
    @Index
    private String userEmail;
    @Index
    private long createdTime;

    public APICredentical() {

    }


    public APICredentical(String userEmail) {
        APIKey = Generate.strCode();
        APISecretKey = Generate.strCode() + String.valueOf(Math.random());
        this.userEmail = userEmail;
        createdTime = System.currentTimeMillis();
    }

    public String getAPIKey() {
        return APIKey;
    }

    public void setAPIKey(String APIKey) {
        this.APIKey = APIKey;
    }

    public String getUserId() {
        return userEmail;
    }

    public void setUserId(String userId) {
        this.userEmail = userId;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public String getAPISecretKey() {

        return APISecretKey;
    }

    public void setAPISecretKey(String APISecretKey) {
        this.APISecretKey = APISecretKey;
    }

    public static APICredentical loadAPICredential(String apikey) {
        if (apikey == null) {
            return null;
        }
        APICredentical credentical = ofy().load().type(APICredentical.class).id(apikey).now();
        if (credentical == null) {
            return null;
        }
        return credentical;
    }

}

package filter;

import RESTULTI.RespMsg;
import RESTULTI.RESTUtil;
import entitiesSupport.APICredentical;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class AuthFilter implements Filter {
    Logger logger = Logger.getLogger(AuthFilter.class.getSimpleName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        request.setCharacterEncoding("UTF-8");
        String API_Key = req.getParameter("apikey");
        if (API_Key==null){
            logger.warning("API_Key is missing");
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Bad Request", "Missing API_Key? Please check again!");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        logger.info("APIKey: " + API_Key);
        APICredentical APIKey = APICredentical.loadAPICredential(API_Key);
        if (APIKey == null) {
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "API key is invalid");
            response.getWriter().print(RESTUtil.gson.toJson(msg));
            logger.severe(API_Key + " is invalid");
            return;
        }
        request.setAttribute("APIKey",APIKey);

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}

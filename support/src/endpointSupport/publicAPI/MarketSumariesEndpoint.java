package endpointSupport.publicAPI;

import RESTULTI.RESTUtil;
import com.entities.MarketPair;
import entitiesSupport.ResponseAPI;
import entitiesSupport.publicAPI.Market;
import entitiesSupport.publicAPI.MarketSumary;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import static com.googlecode.objectify.ObjectifyService.ofy;

//@javax.servlet.annotation.WebServlet(name = "MarketSumariesEndpoint")
public class MarketSumariesEndpoint extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<MarketPair> list = ofy().load().type(MarketPair.class).list();
        List<MarketSumary> list1 = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
        MarketSumary marketSumary = null;
        for (MarketPair marketPair : list){
            marketSumary = new MarketSumary();
            marketSumary.setMarketName(marketPair.getId());
            marketSumary.setHigh(marketPair.getHigh_24h());
            marketSumary.setLow(marketPair.getLow_24h());
            marketSumary.setVolume(marketPair.getVolume());
            marketSumary.setLast(marketPair.getLast_price());
            marketSumary.setBaseVolume(marketPair.getVolume() * marketPair.getLast_price());
            marketSumary.setTimeStamp(format.format(System.currentTimeMillis()));
            marketSumary.setBid(marketPair.getBid_price());
            marketSumary.setAsk(marketPair.getAsk_price());
            marketSumary.setOpenBuyOrders(0);
            marketSumary.setOpenSellOrders(0);
            marketSumary.setPrevDay(0);
            marketSumary.setCreated(format.format(marketPair.getCreated_at()));
            marketSumary.setDisplayMarketName(null);
            list1.add(marketSumary);
        }
        ResponseAPI responseAPI = new ResponseAPI(true, "", list1);
        response.setStatus(200);
        response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
    }
}

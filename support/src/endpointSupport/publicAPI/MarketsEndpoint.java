package endpointSupport.publicAPI;

import RESTULTI.RESTUtil;
import com.entities.MarketPair;
import entitiesSupport.ResponseAPI;
import entitiesSupport.publicAPI.Market;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class MarketsEndpoint extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<MarketPair> marketPairList = ofy().load().type(MarketPair.class).list();
        ResponseAPI responseAPI = null;
        if(marketPairList == null){
            response.setStatus(404);
            responseAPI = new ResponseAPI(false, "Not found!", "");
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        Market market = null;
        List<Market> marketList = new ArrayList<>();
        String[] splitCurrency = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
        for (MarketPair marketPair : marketPairList){
            market = new Market();
            if (marketPair.getId().equals("BTC-BCH")){
                splitCurrency = marketPair.getId().split("-");
                market.setMarketCurrency(splitCurrency[1]);
                market.setBaseCurrency(splitCurrency[0]);
                market.setMarketCurrencyLong("Bitcoin Cash");
                market.setBaseCurrencyLong("Bitcoin");
                market.setMarketName(marketPair.getId());
                market.setIsActive(true);
                market.setCreated(format.format(marketPair.getCreated_at()));
            }
            if (marketPair.getId().equals("BTC-ETH")){
                splitCurrency = marketPair.getId().split("-");
                market.setMarketCurrency(splitCurrency[1]);
                market.setBaseCurrency(splitCurrency[0]);
                market.setMarketCurrencyLong("Ethereum");
                market.setBaseCurrencyLong("Bitcoin");
                market.setMarketName(marketPair.getId());
                market.setIsActive(true);
                market.setCreated(format.format(marketPair.getCreated_at()));
            }
            if (marketPair.getId().equals("BTC-XRP")){
                splitCurrency = marketPair.getId().split("-");
                market.setMarketCurrency(splitCurrency[1]);
                market.setBaseCurrency(splitCurrency[0]);
                market.setMarketCurrencyLong("Ripple");
                market.setBaseCurrencyLong("Bitcoin");
                market.setMarketName(marketPair.getId());
                market.setIsActive(true);
                market.setCreated(format.format(marketPair.getCreated_at()));
            }
            if (marketPair.getId().equals("ETH-BCH")){
                splitCurrency = marketPair.getId().split("-");
                market.setMarketCurrency(splitCurrency[1]);
                market.setBaseCurrency(splitCurrency[0]);
                market.setMarketCurrencyLong("Bitcoin Cash");
                market.setBaseCurrencyLong("Ethereum");
                market.setMarketName(marketPair.getId());
                market.setIsActive(true);
                market.setCreated(format.format(marketPair.getCreated_at()));
            }
            if (marketPair.getId().equals("ETH-XRP")){
                splitCurrency = marketPair.getId().split("-");
                market.setMarketCurrency(splitCurrency[1]);
                market.setBaseCurrency(splitCurrency[0]);
                market.setMarketCurrencyLong("Ripple");
                market.setBaseCurrencyLong("Ethereum");
                market.setMarketName(marketPair.getId());
                market.setIsActive(true);
                market.setCreated(format.format(marketPair.getCreated_at()));
            }
            if (marketPair.getId().equals("USDT-BCH")){
                splitCurrency = marketPair.getId().split("-");
                market.setMarketCurrency(splitCurrency[1]);
                market.setBaseCurrency(splitCurrency[0]);
                market.setMarketCurrencyLong("Bitcoin Cash");
                market.setBaseCurrencyLong("Tether USDT");
                market.setMarketName(marketPair.getId());
                market.setIsActive(true);
                market.setCreated(format.format(marketPair.getCreated_at()));
            }
            if (marketPair.getId().equals("USDT-BTC")){
                splitCurrency = marketPair.getId().split("-");
                market.setMarketCurrency(splitCurrency[1]);
                market.setBaseCurrency(splitCurrency[0]);
                market.setMarketCurrencyLong("Bitcoin");
                market.setBaseCurrencyLong("Tether USDT");
                market.setMarketName(marketPair.getId());
                market.setIsActive(true);
                market.setCreated(format.format(marketPair.getCreated_at()));
            }
            if (marketPair.getId().equals("USDT-ETH")){
                splitCurrency = marketPair.getId().split("-");
                market.setMarketCurrency(splitCurrency[1]);
                market.setBaseCurrency(splitCurrency[0]);
                market.setMarketCurrencyLong("Ethereum");
                market.setBaseCurrencyLong("Tether USDT");
                market.setMarketName(marketPair.getId());
                market.setIsActive(true);
                market.setCreated(format.format(marketPair.getCreated_at()));
            }
            if (marketPair.getId().equals("USDT-XRP")){
                splitCurrency = marketPair.getId().split("-");
                market.setMarketCurrency(splitCurrency[1]);
                market.setBaseCurrency(splitCurrency[0]);
                market.setMarketCurrencyLong("Ripple");
                market.setBaseCurrencyLong("Tether USDT");
                market.setMarketName(marketPair.getId());
                market.setIsActive(true);
                market.setCreated(format.format(marketPair.getCreated_at()));
            }
            marketList.add(market);
        }

        if(marketList == null){
            response.setStatus(404);
            responseAPI = new ResponseAPI(false, "No market!", "");
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        response.setStatus(200);
        responseAPI = new ResponseAPI(true, "", marketList);
        response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
    }
}

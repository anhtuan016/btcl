package endpointSupport.publicAPI;

import RESTULTI.RESTUtil;
import com.entities.Wallet;
import entitiesSupport.ResponseAPI;
import entitiesSupport.SupportCurrencies;
import entitiesSupport.publicAPI.Currency;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CurrenciesEndpoint extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<SupportCurrencies> list = ofy().load().type(SupportCurrencies.class).list();
        Currency currency = null;
        List<Currency> currenciesList = new ArrayList<>();
        for (SupportCurrencies currencies : list){
            currency = new Currency();
            if(currencies.getType().equals("BTC")){
                currency.setCurrencyLong(currencies.getName());
                currency.setCurrency(currencies.getType());
                currency.setCoinType(currencies.getName().toUpperCase());
                currency.setBaseAddress(null);
                currency.setIsActive(true);
                currency.setMinConfirmation(1);
            }
            if(currencies.getType().equals("ETH")){
                currency.setCurrencyLong(currencies.getName());
                currency.setCurrency(currencies.getType());
                currency.setCoinType(currencies.getName().toUpperCase());
                currency.setBaseAddress(null);
                currency.setIsActive(true);
                currency.setMinConfirmation(1);
            }
            if(currencies.getType().equals("XRP")){
                currency.setCurrencyLong(currencies.getName());
                currency.setCurrency(currencies.getType());
                currency.setCoinType(currencies.getName().toUpperCase());
                currency.setBaseAddress(null);
                currency.setIsActive(true);
                currency.setMinConfirmation(1);
            }
            if(currencies.getType().equals("BCH")){
                currency.setCurrencyLong(currencies.getName());
                currency.setCurrency(currencies.getType());
                currency.setCoinType(currencies.getName().toUpperCase());
                currency.setBaseAddress(null);
                currency.setIsActive(true);
                currency.setMinConfirmation(1);
            }
            if(currencies.getType().equals("USDT")){
                currency.setCurrencyLong(currencies.getName());
                currency.setCurrency(currencies.getType());
                currency.setCoinType(currencies.getName().toUpperCase());
                currency.setBaseAddress(null);
                currency.setIsActive(true);
                currency.setMinConfirmation(1);
            }
            currenciesList.add(currency);
        }

        ResponseAPI responseAPI = null;
        if(currenciesList == null){
            response.setStatus(404);
            responseAPI = new ResponseAPI(false, "Not found!", "");
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        responseAPI = new ResponseAPI(false, "", currenciesList);
        response.setStatus(200);
        response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
    }
}

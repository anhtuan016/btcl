package endpointSupport.publicAPI;

import RESTULTI.RESTUtil;
import com.entities.MarketPair;
import com.entities.Order;
import entitiesSupport.ResponseAPI;
import entitiesSupport.publicAPI.OrderBook;
import entitiesSupport.publicAPI.OrderBookEntry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

import static com.googlecode.objectify.ObjectifyService.ofy;

//@javax.servlet.annotation.WebServlet(name = "OrderBookEndpoint")
public class OrderBookEndpoint extends HttpServlet {

    Logger logger = Logger.getLogger(OrderBookEndpoint.class.getSimpleName());


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String market = request.getParameter("market");
        String type = request.getParameter("type");
        if (market == null | market == "" | type == null | type == "") {
            logger.info("Invalid parameter!");
            response.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        MarketPair marketPair = ofy().load().type(MarketPair.class).id(market).now();
        if (marketPair == null) {
            logger.info("This market does not exist!");
            response.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "This market does not exist!", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        List<Order> list = null;
        switch (type) {
            case "buy":
                list = ofy().load().type(Order.class).filter("pair", marketPair.getId()).filter("type", "buy").limit(100).list();
                BuyOrderBook(request, response, list);
                break;
            case "sell":
                list = ofy().load().type(Order.class).filter("pair", marketPair.getId()).filter("type", "sell").limit(100).list();
                SellOrderBook(request, response, list);
                break;
            case "both":
                list = ofy().load().type(Order.class).filter("pair", marketPair.getId()).list();
                BothOrderBook(request, response, list);
                break;
            default:
                response.setStatus(403);
                ResponseAPI responseAPI = new ResponseAPI(false, "Invalid type parameter!", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
        }
    }

    protected void BuyOrderBook(HttpServletRequest request, HttpServletResponse response, List<Order> list) throws ServletException, IOException {
        OrderBook orderBook = new OrderBook();
        List<OrderBookEntry> orderBookList = new ArrayList<>();
        OrderBookEntry buy = null;
        for (Order order : list) {
            buy = new OrderBookEntry();
            buy.setQuantity(order.getAmount());
            buy.setRate(order.getActual_rate());
            orderBookList.add(buy);
        }
        orderBook.setBuy(orderBookList);
        response.setStatus(200);
        ResponseAPI responseAPI = new ResponseAPI(true, "", orderBook);
        response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
    }

    protected void SellOrderBook(HttpServletRequest request, HttpServletResponse response, List<Order> list) throws ServletException, IOException {
        OrderBook orderBook = new OrderBook();
        List<OrderBookEntry> orderBookList = new ArrayList<>();
        OrderBookEntry sell = null;
        for (Order order : list) {
            sell = new OrderBookEntry();
            sell.setQuantity(order.getAmount());
            sell.setRate(order.getActual_rate());
            orderBookList.add(sell);
        }
        orderBook.setSell(orderBookList);
        response.setStatus(200);
        ResponseAPI responseAPI = new ResponseAPI(true, "", orderBook);
        response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
    }

    protected void BothOrderBook(HttpServletRequest request, HttpServletResponse response, List<Order> list) throws ServletException, IOException {
        OrderBook orderBook = new OrderBook();
        List<OrderBookEntry> orderBookListSell = new ArrayList<>();
        List<OrderBookEntry> orderBookListBuy = new ArrayList<>();
        OrderBookEntry sell = null;
        OrderBookEntry buy = null;
        for (Order order : list) {
            if (order.getType().equals("buy")) {
                buy = new OrderBookEntry();
                buy.setQuantity(order.getAmount());
                buy.setRate(order.getPrice());
                orderBookListBuy.add(buy);
                continue;
            }
            sell = new OrderBookEntry();
            sell.setQuantity(order.getAmount());
            sell.setRate(order.getPrice());
            orderBookListSell.add(sell);
        }
        orderBook.setSell(orderBookListSell);
        orderBook.setBuy(orderBookListBuy);
        response.setStatus(200);
        ResponseAPI responseAPI = new ResponseAPI(true, "", orderBook);
        response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
    }
}

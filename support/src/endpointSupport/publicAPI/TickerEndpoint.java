package endpointSupport.publicAPI;

import RESTULTI.RESTUtil;
import com.entities.MarketPair;
import entitiesSupport.ResponseAPI;
import entitiesSupport.publicAPI.Ticker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;
import static com.googlecode.objectify.ObjectifyService.ofy;

//@javax.servlet.annotation.WebServlet(name = "TickerEndpoint")
public class TickerEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(TickerEndpoint.class.getSimpleName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String market = request.getParameter("market");
        if (market == null | market == ""){
            logger.info("Invalid parameter!");
            response.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        MarketPair marketPair = ofy().load().type(MarketPair.class).id(market).now();
        if(marketPair == null){
            logger.info("This ticker does not exist!");
            response.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "This ticker does not exist!", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        Ticker ticker = new Ticker(marketPair.getBid_price(), marketPair.getAsk_price(), marketPair.getLast_price());
        ResponseAPI responseAPI = new ResponseAPI(true, "", ticker);
        response.setStatus(200);
        response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
    }
}

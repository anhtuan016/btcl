package endpointSupport.publicAPI;

import RESTULTI.RESTUtil;
import com.entities.MarketPair;
import entitiesSupport.ResponseAPI;
import entitiesSupport.publicAPI.Market;
import entitiesSupport.publicAPI.MarketSumary;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;
import static com.googlecode.objectify.ObjectifyService.ofy;

//@javax.servlet.annotation.WebServlet(name = "MarketSumaryEndpoint")
public class MarketSumaryEndpoint extends HttpServlet {

    Logger logger = Logger.getLogger(MarketSumaryEndpoint.class.getSimpleName());


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String market = request.getParameter("market");
        if (market == null | market == ""){
            logger.info("Invalid parameter!");
            response.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        MarketPair marketPair = ofy().load().type(MarketPair.class).id(market).now();
        if(marketPair == null) {
            logger.info("This market does not exist!");
            response.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "This market does not exist!", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
        MarketSumary marketSumary = new MarketSumary();
        marketSumary.setMarketName(marketPair.getId());
        marketSumary.setHigh(marketPair.getHigh_24h());
        marketSumary.setLow(marketPair.getLow_24h());
        marketSumary.setVolume(marketPair.getVolume());
        marketSumary.setLast(marketPair.getLast_price());
        marketSumary.setBaseVolume(marketPair.getVolume() * marketPair.getLast_price());
        marketSumary.setTimeStamp(format.format(System.currentTimeMillis()));
        marketSumary.setBid(marketPair.getBid_price());
        marketSumary.setAsk(marketPair.getAsk_price());
        marketSumary.setOpenBuyOrders(0);
        marketSumary.setOpenSellOrders(0);
        marketSumary.setPrevDay(0);
        marketSumary.setCreated(format.format(marketPair.getCreated_at()));
        marketSumary.setDisplayMarketName(null);
        response.setStatus(200);
        ResponseAPI responseAPI = new ResponseAPI(true, "", marketSumary);
        response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
    }
}

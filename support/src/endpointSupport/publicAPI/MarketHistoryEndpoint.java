package endpointSupport.publicAPI;

import RESTULTI.RESTUtil;
import com.entities.MarketPair;
import com.entities.Order;
import entitiesSupport.ResponseAPI;
import entitiesSupport.publicAPI.MarketHistoryEntry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import static com.googlecode.objectify.ObjectifyService.ofy;

import static com.googlecode.objectify.ObjectifyService.ofy;

//@javax.servlet.annotation.WebServlet(name = "MarketHistoryEndpoint")
public class MarketHistoryEndpoint extends HttpServlet {

    Logger logger = Logger.getLogger(MarketHistoryEndpoint.class.getSimpleName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String market = request.getParameter("market");
        if (market == null | market == ""){
            logger.info("Invalid parameter!");
            response.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        MarketPair marketPair = ofy().load().type(MarketPair.class).id(market).now();
        if (marketPair == null) {
            logger.info("This market does not exist!");
            response.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "This market does not exist!", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        List<Order> list = ofy().load().type(Order.class).filter("pair", marketPair.getId()).list();
        if(list == null){
            logger.info("Not found!");
            response.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Not found!", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
        List<MarketHistoryEntry> entryList = new ArrayList<>();
        MarketHistoryEntry marketHistoryEntry = null;
        for (Order order : list){
            marketHistoryEntry = new MarketHistoryEntry();
            marketHistoryEntry.setId(order.getId());
            marketHistoryEntry.setTimeStamp(format.format(order.getUpdated_at()));
            marketHistoryEntry.setQuantity(order.getAmount());
            marketHistoryEntry.setPrice(order.getActual_rate());
            marketHistoryEntry.setTotal(order.getAmount() * order.getActual_rate());
            marketHistoryEntry.setFillType(null);
            marketHistoryEntry.setOrderType(order.getType().toUpperCase());
            entryList.add(marketHistoryEntry);
        }
        ResponseAPI responseAPI = new ResponseAPI(true, "", entryList);
        response.setStatus(200);
        response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
    }
}

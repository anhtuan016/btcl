package endpointSupport;

import RESTULTI.RESTUtil;
import RESTULTI.RespMsg;
import RESTULTI.TwoFactorUtils;
import entitiesSupport.APICredentical;
import com.entities.*;
import org.json.JSONException;
import org.json.JSONObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;
import static com.googlecode.objectify.ObjectifyService.ofy;
public class APIKeyEndpoind extends HttpServlet {
    Logger logger = Logger.getLogger(APICredentical.class.getSimpleName());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            JSONObject obj = new JSONObject(RESTUtil.readStringInput(req.getInputStream()));
            if (obj==null){
                resp.setStatus(500);
                RESTUtil.showInternalError(req,resp, "ERROR LINE 32");
                return;
            }
            UserCredential credential = ofy().load().type(UserCredential.class).filter("tokenKey", req.getHeader("Authentication")).first().now();
            if (credential == null) {
                logger.info("Credential null");
                return;
            }
            User user = ofy().load().type(User.class).id(credential.getUserEmail()).now();
            if (user == null) {
                resp.setStatus(404);
                RESTUtil.showEmptyResult(req, resp);
                return;
            }
            if (user.getTwofactorauth() == 0) {
                resp.setStatus(403);
                RespMsg ms = new RespMsg(403, "Tow factorauth disable", "You can enable two factorauth to using this service!");
                resp.getWriter().println(RESTUtil.gson.toJson(ms));
                return;
            }
            int totp;
            try{
                totp = obj.getInt("totp");
            }catch (JSONException e){
                logger.warning("null TOTP");
                e.printStackTrace();
                RESTUtil.showMissingParams(req, resp, "Missing TOTP");
                return;
            }
            if (!TwoFactorUtils.isValid(user.getSecret_key(), totp)) {
                RESTUtil.showForbiddenError(req, resp, "Invalid TOTP");
                return;
            }
            APICredentical apiCredentical = new APICredentical(credential.getUserEmail());
            if (apiCredentical == null) {
                resp.setStatus(500);
                RespMsg msg = new RespMsg(500, "Server Error", "Contact admin for support");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
                return;
            }
            if (ofy().save().entity(apiCredentical).now() == null) {
                RESTUtil.showInternalError(req, resp);
                return;
            }
            resp.setStatus(201);
            resp.getWriter().println(RESTUtil.gson.toJson(apiCredentical));
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserCredential credential = ofy().load().type(UserCredential.class).filter("tokenKey", req.getHeader("Authentication")).first().now();
        if (credential == null) {
            resp.setStatus(404);
            RESTUtil.showEmptyResult(req, resp);
            return;
        }
        logger.info(RESTUtil.gson.toJson(credential));
        List<APICredentical> list = ofy().load().type(APICredentical.class).filter("userEmail", credential.getUserEmail()).list();
        if (list == null) {
            logger.warning("List null");
            return;
        }
        resp.setStatus(200);
        resp.getWriter().println(RESTUtil.gson.toJson(list));
    }
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String apikey = req.getParameter("id");
        APICredentical apiCredentical = ofy().load().type(APICredentical.class).id(apikey).now();
        if (apiCredentical == null) {
            logger.warning("API NOT FOUND");
            resp.setStatus(404);
            RESTUtil.showEmptyResult(req, resp);
            return;
        }
        logger.info("CREDENTIAL:" + RESTUtil.gson.toJson(apiCredentical));
        ofy().delete().entity(apiCredentical).now();
        if (ofy().delete().entity(apiCredentical).now() == null) {
            logger.warning("DELETE FAIL");
            resp.setStatus(500);
            RESTUtil.showInternalError(req, resp);
            return;
        }
        resp.setStatus(200);
        RespMsg msg = new RespMsg(200, "Successfully", "Delete successfully");
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }
}

package endpointSupport.marketAPI;

import RESTULTI.RESTUtil;
import com.entities.MarketPair;
import com.entities.Order;
import com.entities.Wallet;
import com.google.gson.JsonSyntaxException;
//import com.pusher.rest.Pusher;
//import com.pusher.rest.data.Result;
import com.sun.org.apache.xpath.internal.operations.Or;
import com.util.RespMsg;
import endpointSupport.publicAPI.OrderBookEndpoint;
import entitiesSupport.APICredentical;
import entitiesSupport.ResponseAPI;
import entitiesSupport.marketAPI.OpenOrder;
import entitiesSupport.marketAPI.OrderCreated;
import entitiesSupport.publicAPI.Market;
import org.json.JSONException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import static com.googlecode.objectify.ObjectifyService.ofy;

//@javax.servlet.annotation.WebServlet(name = "MarketEndpoint")
public class MarketEndpoint extends HttpServlet {

    static Logger logger = Logger.getLogger(MarketEndpoint.class.getSimpleName());
    String matching_URL = "http://54.39.22.4:8080/matching/match";
    String rollback_URL = "https://1-dot-bittrexcln.appspot.com/api/v1/rollback?id=";
    static String rollback_key = "9f4ed17aeac77e7cfdf6";
//    static Pusher pusher;
//    Result result;

//    static {
//        if (pusher == null) {
//            logger.info("Initiate new Pusher");
//            pusher = new Pusher("504021", "f4c14cf0e0b456a14b95", "2fb04d90967e47761f96");
//            pusher.setCluster("mt1");
//            logger.info("new pusher: " + pusher.toString());
//        }
//    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = null;
        String market = null;
        long uuid = 0;
        double quantity = 0;
        double rate = 0;
        APICredentical apiCredentical = (APICredentical) request.getAttribute("APIKey");
        String[] uriSplit = request.getRequestURI().split("/");
        if (uriSplit.length == 5) {
            action = uriSplit[uriSplit.length - 1];
        }else {
            logger.info("Page not found! - limited URI");
            response.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Page not found! - limited URI", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        MarketPair marketPair = null;
        Order order = null;
        switch (action) {
            case "buylimit":
                market = request.getParameter("market");
                if (market == null | market == ""){
                    logger.info("Invalid parameter!");
                    response.setStatus(403);
                    ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
                    response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                    return;
                }
                marketPair = ofy().load().type(MarketPair.class).id(market).now();
                try {
                    quantity = Double.parseDouble(request.getParameter("quantity"));
                    rate = Double.parseDouble(request.getParameter("rate"));
                }catch (NumberFormatException e){
                    logger.info(e.getMessage());
                    response.setStatus(403);
                    ResponseAPI responseAPI = new ResponseAPI(false, e.getMessage(), null);
                    response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                    return;
                }
                if(String.valueOf(quantity) == null | String.valueOf(rate) == null | marketPair == null){
                    logger.info("Invalid parameter!");
                    response.setStatus(404);
                    ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
                    response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                    return;
                }
                BuyLimit(request, response, marketPair, quantity, rate, apiCredentical);
                break;
            case "selllimit":
                market = request.getParameter("market");
                if (market == null | market == ""){
                    logger.info("Invalid parameter!");
                    response.setStatus(403);
                    ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
                    response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                    return;
                }
                marketPair = ofy().load().type(MarketPair.class).id(market).now();
                try {
                    quantity = Double.parseDouble(request.getParameter("quantity"));
                    rate = Double.parseDouble(request.getParameter("rate"));
                }catch (NumberFormatException e){
                    logger.info(e.getMessage());
                    response.setStatus(403);
                    ResponseAPI responseAPI = new ResponseAPI(false, e.getMessage(), null);
                    response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                    return;
                }
                if(String.valueOf(quantity) == null | String.valueOf(rate) == null | marketPair == null){
                    logger.info("Invalid parameter!");
                    response.setStatus(404);
                    ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
                    response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                    return;
                }
                SellLimit(request, response, marketPair, quantity, rate, apiCredentical);
                break;
            case "cancel":
                try {
                    uuid = Long.parseLong(request.getParameter("uuid"));
                }catch (NumberFormatException e){
                    logger.info(e.getMessage());
                    response.setStatus(403);
                    ResponseAPI responseAPI = new ResponseAPI(false, e.getMessage(), null);
                    response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                    return;
                }
                order = ofy().load().type(Order.class).id(uuid).now();
                if(String.valueOf(uuid) == null | order == null){
                    logger.info("Invalid parameter!");
                    response.setStatus(404);
                    ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
                    response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                    return;
                }
                Cancel(request, response, order, apiCredentical);
                break;
            case "getopenorders":
                market = request.getParameter("market");
                if (market == null | market == ""){
                    logger.info("Invalid parameter!");
                    response.setStatus(403);
                    ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
                    response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                    return;
                }
                marketPair = ofy().load().type(MarketPair.class).id(market).now();
                if(marketPair == null){
                    logger.info("Invalid parameter!");
                    response.setStatus(404);
                    ResponseAPI responseAPI = new ResponseAPI(false, "Invalid parameter!", null);
                    response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                    return;
                }
                GetOpenOrder(request, response, marketPair);
                break;
            default:
                logger.info("Page not found!");
                response.setStatus(404);
                ResponseAPI responseAPI = new ResponseAPI(false, "Page not found!", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
        }
    }
    protected void BuyLimit(HttpServletRequest request, HttpServletResponse response, MarketPair marketPair, double quantity, double rate, APICredentical credential) throws ServletException, IOException {
        try {
            Order order = new Order();
            order.setPair(marketPair.getId());
            order.setAmount(quantity);
            order.setPrice(rate);
            order.setType("buy");

            String pair[] = marketPair.getId().split("-");

            double totalAmount = BigDecimal.valueOf(quantity).multiply(BigDecimal.valueOf(rate)).doubleValue();
            double fee = BigDecimal.valueOf(totalAmount).multiply(BigDecimal.valueOf(0.0025)).doubleValue();
            double bidAmount = com.util.RESTUtil.addDouble(totalAmount, fee);

            //            GET THE WALLET.
            Wallet wallet = ofy().load().type(Wallet.class).filter("type", pair[0].trim()).filter("userId", credential.getUserId()).first().now();
            if (wallet == null) {
                logger.warning("CANNOT FIND WALLET");
                response.setStatus(500);
                ResponseAPI responseAPI = new ResponseAPI(false, "User wallet cannot be found", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            double currentBal = wallet.getAmount();
            if (currentBal < bidAmount) {
                logger.warning("NOT ENOUGH FUND TO PLACE ORDER");
                response.setStatus(403);
                ResponseAPI responseAPI = new ResponseAPI(false, "User wallet does not have enough fund to place bid order", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            order.setUserId(credential.getUserId());
            order.setFee(fee);
            order.setBaseFee(fee);
            order.setValid("processing");
            logger.info("Saving order to database!");
            if (order.validate().size() != 0) {
                logger.warning("server error:" + com.util.RESTUtil.gson.toJson(order.validate()));
                response.setStatus(500);
                ResponseAPI responseAPI = new ResponseAPI(false, "server error:" + com.util.RESTUtil.gson.toJson(order.validate()), null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
//            SAVE ORDER.
            if (ofy().save().entity(order).now() == null) {
                logger.warning("CANNOT SAVE ORDER: " + order.getId());
                response.setStatus(500);
                ResponseAPI responseAPI = new ResponseAPI(false, "Cannot place order now, please try again.", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            Order orderTest = ofy().load().type(Order.class).id(order.getId()).now();
            logger.info("basefee: " + orderTest.getBaseFee());
            logger.info("order saved: " + com.util.RESTUtil.gson.toJson(order));

//            SAVE WALLET.
            wallet.setAmount(com.util.RESTUtil.minusDouble(wallet.getAmount(), bidAmount));
            wallet.estimateWallet();

            logger.info("WALLET-AMOUNT: " + (wallet.getAmount()));
            if (ofy().save().entity(wallet).now() == null) {
                logger.severe("CANNOT UPDATE THE WALLET: " + com.util.RESTUtil.gson.toJson(wallet) + " TRYING TO DELETE ORDER");
                ofy().delete().entity(order).now();
                response.setStatus(500);
                ResponseAPI responseAPI = new ResponseAPI(false, "Some errors occured, please try again later!", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
//            CONNECT TO MATCHING ENGINE.
            HashMap<String, Object> orderTobeMatched = new HashMap<>();
            Order loadedOrder = ofy().load().type(Order.class).id(order.getId()).now();
            if (loadedOrder == null) {
                response.setStatus(500);
                ResponseAPI responseAPI = new ResponseAPI(false, "Internal Error when creating Order!", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            long long_price = BigDecimal.valueOf(loadedOrder.getPrice()).multiply(BigDecimal.valueOf(1000_000_000_000L)).longValue();
            long long_amount = BigDecimal.valueOf(loadedOrder.getAmount()).multiply(BigDecimal.valueOf(1000_000_000_000L)).longValue();
            logger.info("long_amount: " + long_amount);
            orderTobeMatched.put("orderId", loadedOrder.getId());
            orderTobeMatched.put("currencyPair", loadedOrder.getPair());
            orderTobeMatched.put("price", long_price);
            orderTobeMatched.put("quantity", long_amount);
            String postingContent = com.util.RESTUtil.gson.toJson(orderTobeMatched);
            logger.info("posting content: " + postingContent);

            ExecutorService executors = Executors.newCachedThreadPool();
            executors.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Jsoup.connect(matching_URL + "?command=buy").timeout(30*1000).ignoreContentType(true).method(Connection.Method.POST).requestBody(postingContent).execute();
                    } catch (IOException a) {
                        logger.severe("CANNOT POST TO MATCHING VPS: " + com.util.RESTUtil.gson.toJson(orderTobeMatched) + " Order Deleted " + com.util.RESTUtil.gson.toJson(order));
//                  ROLLBACK ORDER API.
                        try {
                            Jsoup.connect(rollback_URL + order.getId()).ignoreContentType(true).method(Connection.Method.POST).header("key", rollback_key).execute();
                        } catch (IOException c) {
                        }
                    }
                }
            });
            executors.shutdown();


//            PUSHER CHANGE WALLET BALANCE, ORDER OPENED
//            result = pusher.trigger("walletbalance-" + wallet.getUserId(), wallet.getType(), Collections.singletonMap("data", com.util.RESTUtil.gson.toJson(wallet)));
//            logger.info("result + Change wallet Balance: " + result.getMessage() + " status: " + result.getStatus());
            logger.info("Update wallet to database successfully: " + com.util.RESTUtil.gson.toJson(wallet));
//            result = pusher.trigger("openedOrder", order.getUserId(), Collections.singletonMap("message", "new data updated"));
//            logger.info("result Opened Order: " + result.getMessage() + " status: " + result.getStatus());
            OrderCreated orderCreated = new OrderCreated();
            orderCreated.setUuid(order.getId());
            ResponseAPI responseAPI = new ResponseAPI(true, "", orderCreated);
            response.setStatus(201);
            response.getWriter().println(com.util.RESTUtil.gson.toJson(responseAPI));
        }catch (JsonSyntaxException e) {
            logger.warning("Syntax Json: " + e.fillInStackTrace().getLocalizedMessage() + "==== " + com.util.RESTUtil.gson.toJson(e.getStackTrace()));
            response.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "Invalid order format", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        } catch (NumberFormatException e) {
            logger.warning("NumberFormat exception: " + e.fillInStackTrace().getLocalizedMessage());
            response.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "Invalid number format", null);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }


    }
    protected void SellLimit(HttpServletRequest request, HttpServletResponse response, MarketPair marketPair, double quantity, double rate, APICredentical credential) throws ServletException, IOException {
        try {

            Order order = new Order();
            order.setPair(marketPair.getId());
            order.setAmount(quantity);
            order.setPrice(rate);
            order.setType("sell");

            String pair[] = order.getPair().split("-");
            //CHECK BALANCE OF THE SECOND CURRENCY INSIDE THE PAI DUE TO SELL ORDER.
            Wallet wallet = ofy().load().type(Wallet.class).filter("type", pair[1].trim()).filter("userId", credential.getUserId()).first().now();
            if (wallet == null) {
                logger.warning("CANNOT FIND WALLET");
                response.setStatus(500);
                ResponseAPI responseAPI = new ResponseAPI(false, "User wallet cannot be found", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            double currentBal = wallet.getAmount();
            double askAmount = order.getAmount();
            double fee = BigDecimal.valueOf(order.getPrice()).multiply(BigDecimal.valueOf(order.getAmount())).multiply(BigDecimal.valueOf(0.0025)).doubleValue();
            logger.info("ask amount: " + askAmount);
            if (currentBal < askAmount) {
                logger.warning("Not enough fund to make sell order");
                response.setStatus(403);
                ResponseAPI responseAPI = new ResponseAPI(false, "User not have enough " + pair[1] + " to place order", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            order.setUserId(credential.getUserId());
            order.setFee(fee);
            order.setBaseFee(fee);
            order.setValid("processing");
            logger.info("order: " + com.util.RESTUtil.gson.toJson(order));
            if (order.validate().size() != 0) {
                logger.warning("server error: validate order");
                ResponseAPI responseAPI = new ResponseAPI(false, "server error: validate order", null);
                response.setStatus(500);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            if (ofy().save().entity(order).now() == null) {
                logger.severe("CANNOT SAVE ORDER TO DATASTORE!");
                ResponseAPI responseAPI = new ResponseAPI(false, "ERROR WHEN CREATING ORDER, PLEASE TRY AGAIN", null);
                response.setStatus(500);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            wallet.setAmount(com.util.RESTUtil.minusDouble(wallet.getAmount(), askAmount));
            wallet.estimateWallet();

            if (ofy().save().entity(wallet).now() == null) {
                logger.warning("UPDATE WALLET FAIL WHEN MAKE SELL ORDER! TRYING DELETE ORDER");
                ofy().delete().entity(order).now();
                ResponseAPI responseAPI = new ResponseAPI(false, "Some Errors occured! Please try again later!", null);
                response.setStatus(500);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
//            PUSHER CHANGE WALLET BALANCE.
//            result = pusher.trigger("walletbalance-" + wallet.getUserId(), wallet.getType(), Collections.singletonMap("data", com.util.RESTUtil.gson.toJson(wallet)));
//            logger.info("result + Change wallet Balance: " + result.getMessage() + " status: " + result.getStatus());

//            CONNECT TO MATCHING ENGINE.
            HashMap<String, Object> orderTobeMatched = new HashMap<>();
            Order loadedOrder = ofy().load().type(Order.class).id(order.getId()).now();
            if (loadedOrder == null) {
                ResponseAPI responseAPI = new ResponseAPI(false, "Internal Error when creating Order!", null);
                response.setStatus(500);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            long long_price = BigDecimal.valueOf(loadedOrder.getPrice()).multiply(BigDecimal.valueOf(1000_000_000_000L)).longValue();
            long long_amount = BigDecimal.valueOf(loadedOrder.getAmount()).multiply(BigDecimal.valueOf(1000_000_000_000L)).longValue();
            logger.info("long_amount: " + long_amount);
            orderTobeMatched.put("orderId", loadedOrder.getId());
            orderTobeMatched.put("currencyPair", loadedOrder.getPair());
            orderTobeMatched.put("price", long_price);
            orderTobeMatched.put("quantity", long_amount);
            final String postingContent = com.util.RESTUtil.gson.toJson(orderTobeMatched);
            logger.info("posting content: " + postingContent);
            logger.info("Starting to executor");
            ExecutorService executors = Executors.newCachedThreadPool();

            executors.execute(new Runnable() {
                @Override
                public void run() {
                    try {
//                        throw new IOException();
                        Jsoup.connect(matching_URL + "?command=sell").timeout(30*1000).ignoreContentType(true).method(Connection.Method.POST).requestBody(postingContent).execute();
                    } catch (Exception a) {
                        //REQUEST TO CANCELLING ORDER ENDPOINT.
                        try {
                            Jsoup.connect(rollback_URL + order.getId() + "&content=" + a.getMessage()).method(Connection.Method.POST).ignoreContentType(true).header("key", rollback_key).execute();
                        } catch (IOException e) {
                        }
                    }
                }
            });
            executors.shutdown();
            logger.info("END");
            OrderCreated orderCreated = new OrderCreated();
            orderCreated.setUuid(order.getId());
            ResponseAPI responseAPI = new ResponseAPI(true, "", orderCreated);
            response.setStatus(201);
            response.getWriter().println(com.util.RESTUtil.gson.toJson(responseAPI));
        } catch (JsonSyntaxException e) {
            logger.warning("Syntax Json: " + e.fillInStackTrace().getLocalizedMessage() + "==== " + com.util.RESTUtil.gson.toJson(e.getStackTrace()));
            ResponseAPI responseAPI = new ResponseAPI(false, "Invalid order format", null);
            response.setStatus(403);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        } catch (NumberFormatException e) {
            logger.warning("NumberFormat: " + e.fillInStackTrace().getLocalizedMessage() + "==== " + com.util.RESTUtil.gson.toJson(e.getStackTrace()));
            ResponseAPI responseAPI = new ResponseAPI(false, "Invalid number format", null);
            response.setStatus(403);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
    }
    protected void Cancel(HttpServletRequest request, HttpServletResponse response, Order orderExist, APICredentical credentical) throws ServletException, IOException {
        try {
            if (orderExist.getStatus() == 0 || orderExist.getStatus() == 1) {
                response.setStatus(204);
                ResponseAPI responseAPI = new ResponseAPI(false, "Data is empty", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            if (!orderExist.getUserId().equalsIgnoreCase(credentical.getUserId())) {
                logger.severe("Missing tokenKey");
                response.setStatus(403);
                ResponseAPI responseAPI = new ResponseAPI(false, "Missing Token Key", null);
                response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            HashMap<String, Object> data = new HashMap<>();
            data.put("orderId", orderExist.getId());
            data.put("currencyPair", orderExist.getPair());
            String rsp = Jsoup.connect(matching_URL + "?command=cancelall").timeout(60 * 1000).ignoreContentType(true).method(Connection.Method.POST).requestBody(com.util.RESTUtil.gson.toJson(data)).execute().body();
            logger.info("cancelAll rsp: " + rsp);
            ResponseAPI responseAPI = new ResponseAPI(true, "", null);
            response.setStatus(200);
            response.getWriter().println(com.util.RESTUtil.gson.toJson(responseAPI));
        } catch (JSONException e) {
            logger.warning("Json syntax");
            ResponseAPI responseAPI = new ResponseAPI(false, "Json syntax", null);
            response.setStatus(403);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        } catch (IOException e) {
            logger.warning("Error on connect VPS: " + e.getMessage());
            ResponseAPI responseAPI = new ResponseAPI(false, "Error on connect VPS: " + e.getMessage(), null);
            response.setStatus(403);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
    }
    protected void GetOpenOrder(HttpServletRequest request, HttpServletResponse response, MarketPair marketPair) throws ServletException, IOException {
        List<Order> list = ofy().load().type(Order.class).filter("pair", marketPair.getId()).list();
        if(list == null){
            ResponseAPI responseAPI = new ResponseAPI(false, "No open order!", null);
            response.setStatus(404);
            response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
        }
        OpenOrder openOrder = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
        List<OpenOrder> openOrderList = new ArrayList<>();
        for (Order order : list){
            openOrder = new OpenOrder();
            openOrder.setUuid(null);
            openOrder.setOrderUuid(order.getId());
            openOrder.setExchange(marketPair.getId());
            openOrder.setOrderType("LIMIT_" + order.getType().toUpperCase());
            openOrder.setQuantity(order.getAmount());
            openOrder.setQuantityRemaining(order.getAmount());
            openOrder.setLimit("unlimited");
            openOrder.setCommissionPaid(0.00000000);
            openOrder.setPrice(order.getPrice());
            openOrder.setPricePerUnit(0.00000000);
            openOrder.setOpened(format.format(order.getCreated_at()));
            openOrder.setClosed(null);
            openOrder.setCancelInitiated(false);
            openOrder.setImmediateOrCancel(false);
            openOrder.setConditional(false);
            openOrder.setCondition(null);
            openOrder.setConditionTarget(null);
            openOrderList.add(openOrder);
        }

        ResponseAPI responseAPI = new ResponseAPI(true, "", openOrderList);
        response.setStatus(200);
        response.getWriter().print(RESTUtil.gson.toJson(responseAPI));
    }
}

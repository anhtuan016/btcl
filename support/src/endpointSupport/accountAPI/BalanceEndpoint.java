package endpointSupport.accountAPI;

import RESTULTI.RESTUtil;
import RESTULTI.RespMsg;
import com.entities.*;
import entitiesSupport.APICredentical;
import entitiesSupport.ResponseAPI;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;


import static com.googlecode.objectify.ObjectifyService.ofy;

public class BalanceEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(BalanceEndpoint.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 5) {
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Parameters are missing", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        APICredentical apiCredentical = (APICredentical) req.getAttribute("APIKey");
        if (apiCredentical == null) {
            logger.warning("APIKey null");
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "APIKey null", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        logger.info("API KEY: " + apiCredentical);
        User user = ofy().load().type(User.class).id(apiCredentical.getUserId()).now();
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Cannot find user!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        logger.info(RESTUtil.gson.toJson(user));
        String method = uriSplit[uriSplit.length - 1];
        if (method.equalsIgnoreCase("getbalances")) {
            getBalances(req, resp, user);
        }
        if (method.equalsIgnoreCase("getbalance")) {
            String param = req.getParameter("currency");
            if (param == null) {
                resp.setStatus(404);
                ResponseAPI responseAPI = new ResponseAPI(false, "Parameters are missing", null);
                resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
                return;
            }
            getBalance(req, resp, param, user);
        }
    }

    private void getBalances(HttpServletRequest req, HttpServletResponse resp, User user) throws IOException {
        logger.info("GET-BALANCES");
        List<Wallet> list = ofy().load().type(Wallet.class).filter("userId", user.getEmail()).list();
        logger.info("List wallet: " + list.size() + " data: " + RESTUtil.gson.toJson(list));
        List<Object> listWalletId = new ArrayList<>();
        for (Wallet wallet : list) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("Uuid", null);
            map.put("Requested", false);
            map.put("Balance", String.valueOf(wallet.getAmount()));
            map.put("Available", String.valueOf(wallet.getAmount() + wallet.getAmount_pending()));
            map.put("Pending", String.valueOf(wallet.getAmount_pending()));
            map.put("Currency", wallet.getType());
            listWalletId.add(map);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("success", true);
        map.put("message", "");
        map.put("result", listWalletId);
        logger.info(RESTUtil.gson.toJson(map));
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(map));
    }

    private void getBalance(HttpServletRequest req, HttpServletResponse resp, String param, User user) throws IOException {
        logger.info("GET BALANCE");
        Wallet wallet = ofy().load().type(Wallet.class).filter("userId", user.getEmail()).filter("type", param).first().now();
        if (wallet == null) {
            resp.setStatus(404);
            logger.warning("wallet null");
            ResponseAPI responseAPI = new ResponseAPI(false, "Wallet has been delete or not found or check currency and try again", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        logger.info("WALLET: " + RESTUtil.gson.toJson(wallet));
        HashMap<String, Object> map = new HashMap<>();
        map.put("Uuid", null);
        map.put("Requested", false);
        map.put("Balance", String.valueOf(wallet.getAmount()));
        map.put("Available", String.valueOf(wallet.getAmount() + wallet.getAmount_pending()));
        map.put("Pending", String.valueOf(wallet.getAmount_pending()));
        map.put("Currency", wallet.getType());
        HashMap<String, Object> result = new HashMap<>();
        result.put("success", true);
        result.put("message", "");
        result.put("result", map);
        logger.info("MAP: " + RESTUtil.gson.toJson(result));
        resp.setStatus(200);
        resp.getWriter().println(RESTUtil.gson.toJson(result));
    }
}

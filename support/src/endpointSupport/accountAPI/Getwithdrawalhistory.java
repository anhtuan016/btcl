package endpointSupport.accountAPI;

import RESTULTI.RESTUtil;
import RESTULTI.RespMsg;
import entitiesSupport.*;
import com.entities.*;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class Getwithdrawalhistory extends HttpServlet {
    Logger logger = Logger.getLogger(Getwithdrawalhistory.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        APICredentical apiKey = (APICredentical) req.getAttribute("APIKey");
        if (apiKey == null) {
            logger.warning("APIKEY null!");
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "APIKey not found or has been deleted!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }

        User user = ofy().load().type(User.class).id(apiKey.getUserId()).now();
        if (user == null) {
            logger.warning("User null");
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "User not found or has been deleted!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        String strCase = uriSplit[uriSplit.length - 1];
        switch (strCase) {
            case "getwithdrawalhistory":
                getwithdrawalhistory(req, resp, user);
                break;
            default:
                resp.setStatus(400);
                ResponseAPI responseAPI = new ResponseAPI(false, "Can not find request", null);
                resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
        }
    }

    private void getwithdrawalhistory(HttpServletRequest req, HttpServletResponse resp, User user) throws ServletException, IOException {
        String curentcyType = req.getParameter("currency");
        int i = CurrenryType.getint(curentcyType);
        logger.info("GET HISTORY WITHDRAWAL");
        List<Wallet> list = ofy().load().type(Wallet.class).filter("userId", user.getEmail()).list();
        logger.info("List wallet: " + list.size() + " data: " + RESTUtil.gson.toJson(list));
        List<Object> listResp = new ArrayList<>();
        List<Object> listWalletId = new ArrayList<>();
        for (Wallet wallet : list) {
            listWalletId.add(wallet.getId());
        }
        List<WithdrawalTx> withdrawalTxList = ofy().load().type(WithdrawalTx.class).filter("type", i).filter("walletId in", listWalletId).list();
        if (withdrawalTxList.size() == 0) {
            resp.setStatus(403);
            ResponseAPI responseAPI = new ResponseAPI(false, "LIST WITHDRAWAL HISTORY SIZE = 0", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        logger.warning("List: " + RESTUtil.gson.toJson(withdrawalTxList));
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
        for (WithdrawalTx withdrawalTx : withdrawalTxList) {
            HashMap<String, Object> withdrawal = new HashMap<>();
            withdrawal.put("PaymentUuid", withdrawalTx.getId());
            withdrawal.put("Currency", CurrenryType.getCurrery(i));
            withdrawal.put("Amount", withdrawalTx.getAmount());
            withdrawal.put("Address", withdrawalTx.getReceiving_address());
            withdrawal.put("Opened", withdrawalTx.getCreated_at());
            withdrawal.put("Authorized", true);
            if (withdrawalTx.getStatus().equalsIgnoreCase("pending")) {
                withdrawal.put("PendingPayment", true);
            }
            if (withdrawalTx.getStatus().equalsIgnoreCase("complete")) {
                withdrawal.put("PendingPayment", false);
            }
            withdrawal.put("TxCost", null);
            withdrawal.put("TxId", withdrawalTx.getHash());
            withdrawal.put("Canceled", false);
            withdrawal.put("InvalidAddress", false);
            listResp.add(withdrawal);
        }
        ResponseAPI responseAPI = new ResponseAPI();
        responseAPI.setSuccess(true);
        responseAPI.setMessage("");
        responseAPI.setResult(listResp);
        logger.info(RESTUtil.gson.toJson(responseAPI));
        resp.getWriter().println(RESTUtil.gson.toJson(responseAPI));

    }
}

package endpointSupport.accountAPI;

import RESTULTI.RESTUtil;
import RESTULTI.RespMsg;
import com.sun.org.apache.xpath.internal.operations.Or;
import entitiesSupport.APICredentical;
import com.entities.*;
import entitiesSupport.ResponseAPI;

import javax.management.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class OrderhistoryEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(OrderhistoryEndpoint.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 5) {
            RESTUtil.showMissingParams(req, resp);
            ResponseAPI responseAPI = new ResponseAPI(false, "Parameters are missing", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        APICredentical apiKey = (APICredentical) req.getAttribute("APIKey");
        if (apiKey == null) {
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "APIKey not found or has been deleted!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        logger.info("API KEY: " + RESTUtil.gson.toJson(apiKey));
        User user = ofy().load().type(User.class).id(apiKey.getUserId()).now();
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Cannot found user!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(1);
        listStatus.add(0);
        List<Object> listRes = new ArrayList<>();
        List<Order> orderHistory = ofy().load().type(Order.class).filter("userId", user.getEmail()).filter("status in", listStatus).order("-updated_at").list();
        logger.info("orderHistory is : " + RESTUtil.gson.toJson(orderHistory));
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
        for (Order order : orderHistory) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("AccountId", null);
            map.put("OrderUuid", order.getId());
            map.put("Exchange", order.getPair());
            map.put("Type", order.getType());
            map.put("Quantity", order.getAmount());
            map.put("QuantityRemaining", order.getAmount());
            map.put("Limit", "");
            map.put("Reserved", null);
            map.put("ReserveRemaining", null);
            map.put("CommissionReserved", null);
            map.put("CommissionReserveRemaining", null);
            map.put("CommissionPaid", null);
            map.put("Price", order.getPrice());
            map.put("PricePerUnit", null);
            map.put("Opened", format.format(order.getCreated_at()));
            map.put("Closed", null);
            map.put("IsOpen", true);
            map.put("Sentinel", null);
            map.put("CancelInitiated", false);
            map.put("ImmediateOrCancel", false);
            map.put("IsConditional", false);
            map.put("Condition", "NONE");
            map.put("ConditionTarget", null);
            listRes.add(map);
        }
        resp.setStatus(200);
        ResponseAPI responseAPI = new ResponseAPI();
        responseAPI.setSuccess(true);
        responseAPI.setMessage("");
        responseAPI.setResult(listRes);
        resp.getWriter().println(RESTUtil.gson.toJson(responseAPI));
    }
}

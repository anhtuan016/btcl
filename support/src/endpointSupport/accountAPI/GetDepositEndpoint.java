package endpointSupport.accountAPI;


import RESTULTI.RESTUtil;
import com.entities.Enum.CurrenryType;
import com.util.Generate;
import entitiesSupport.APICredentical;
import com.entities.*;
import entitiesSupport.ResponseAPI;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Currency;
import java.util.HashMap;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;
import static com.googlecode.objectify.ObjectifyService.pop;

public class GetDepositEndpoint extends HttpServlet {
    public static String HOST_URL = "https://1-dot-digitalwalletservice.appspot.com";
    public static String API_KEY = "d0179161-6cec-4691-aaa1-181ca13143c3";
    public static String API_SECRET_KEY = "4b133b7e-9dda-4d7c-a54a-f6f6c1770f670.8091180048930605";
    private static String ACCOUNT_BTC = "6451085a-f549-42dc-90f7-ba8d783bc4a8";
    private static String ACCOUNT_BCH = "4c31feb9-1c5b-49a5-a83a-e44142622461";
    private static String ACCOUNT_ETH = "0x746ad0f8082dd4585d532adc6601e4e94971e556";
    private static String ACCOUNT_USDT = "61e9ad59-71fe-43a6-95a1-6f757cdf9fe1";
    private static String Address = "rwpnQ3c7LWzHD8c4fTXPKZU5NakF3KmnSa";


    Logger logger = Logger.getLogger(GetDepositEndpoint.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 5) {
            logger.warning("parameter missing");
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Parameters are missing", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        APICredentical apiKey = (APICredentical) req.getAttribute("APIKey");
        String currency = req.getParameter("currency");
        if (apiKey == null || currency == null) {
            logger.warning("APIKey null or currency null");
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Parameters are missing", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        getDeposit(req, resp, currency, apiKey);
    }

    private void getDeposit(HttpServletRequest req, HttpServletResponse resp, String currency, APICredentical apiCredentical) throws IOException {
        User user = ofy().load().type(User.class).id(apiCredentical.getUserId()).now();
        if (user == null) {
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "User not found or has been deleted!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", currency).filter("userId", user.getEmail()).first().now();
        if (wallet == null) {
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Wallet not found or has been deleted!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        logger.info("USER: " + RESTUtil.gson.toJson(user));

        String currencytoLowerCase = currency.toLowerCase();
        logger.info("Currency lower case: " + currencytoLowerCase);
        switch (currencytoLowerCase) {
            case "btc":
                getAddress(req, resp, "btc", ACCOUNT_BTC, wallet);
                break;
            case "bch":
                getAddress(req, resp, "bch", ACCOUNT_BCH, wallet);
                break;
            case "eth":
                getAddress(req, resp, "eth", ACCOUNT_ETH, wallet);
                break;
            case "usdt":
                getAddress(req, resp, "usdt", ACCOUNT_USDT, wallet);
                break;
            case "xrp":
                creatAddressXRP(req, resp, wallet);
                break;
            default:
                logger.warning("LOI DEFAULT");
                break;
        }
    }

    private void getAddress(HttpServletRequest req, HttpServletResponse resp, String currency, String accountId, Wallet wallet) throws IOException {
        HashMap<String, String> headers = new HashMap<>();
        String timeStamp = String.valueOf(Generate.currentTime() / 1000);
        String method = "POST";
        String url = "/v2/" + currency + "/accounts/" + accountId + "/addresses";
        String rawSignature = timeStamp + method + url;
        logger.info("Raw Signature Before Coverting: " + rawSignature);
        String HMACSignature = com.util.RESTUtil.toHMAC256(rawSignature, API_SECRET_KEY);
        logger.info("HMAC: " + HMACSignature);
        headers.put("CB-ACCESS-SIGN", HMACSignature);
        headers.put("CB-ACCESS-KEY", API_KEY);
        headers.put("CB-ACCESS-TIMESTAMP", timeStamp);
        Connection.Response rsp = Jsoup.connect(HOST_URL + url)
                .headers(headers)
                .ignoreContentType(true)
                .method(Connection.Method.POST)
                .ignoreHttpErrors(true)
                .execute();
        logger.info("response: " + rsp);
        if (rsp.statusCode() != 201) {
            logger.info("response create address vps null!");
            resp.setStatus(201);
            ResponseAPI responseAPI = new ResponseAPI(false, "Response create address from VPS null! Please try again!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        String content = rsp.body();
        logger.info("content: " + content);
        JSONObject obj = new JSONObject(content);
        String address = obj.getString("address");
        Address addressObj = new Address();
        addressObj.setAddress(address);
        addressObj.setType(CurrenryType.getint(currency.toUpperCase()));
        addressObj.setWalletId(wallet.getId());
        ofy().save().entity(addressObj).now();
        HashMap<String, String> map = new HashMap<>();
        map.put("Currency", currency.toUpperCase());
        map.put("Address", address);
        logger.info(RESTUtil.gson.toJson(map));
        ResponseAPI responseAPI = new ResponseAPI();
        responseAPI.setSuccess(true);
        responseAPI.setMessage("");
        responseAPI.setResult(map);
        resp.getWriter().println(RESTUtil.gson.toJson(responseAPI));
    }

    private void creatAddressXRP(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws IOException {
        Address address = ofy().load().type(Address.class).id(Address).now();
        if (address == null) {
            logger.warning("Address null");
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Address is null, check again!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        DestinationTag tag = new DestinationTag(wallet.getId(), address.getAddress());
        if (ofy().save().entity(tag).now() == null) {
            logger.warning("save tag");
            resp.setStatus(500);
            ResponseAPI responseAPI = new ResponseAPI(false, "Server error. Please try again!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        logger.info("resp address");
        HashMap<String, String> map = new HashMap<>();
        map.put("Currency", "XRP");
        map.put("Address", address.getAddress());
        logger.info(RESTUtil.gson.toJson(map));
        ResponseAPI responseAPI = new ResponseAPI();
        responseAPI.setSuccess(true);
        responseAPI.setMessage("");
        responseAPI.setResult(map);
        resp.getWriter().println(RESTUtil.gson.toJson(responseAPI));
    }
}

package endpointSupport.accountAPI;

import RESTULTI.RESTUtil;
import RESTULTI.RespMsg;
import com.entities.Enum.CurrenryType;
import com.entities.User;
import com.entities.Wallet;
import com.entities.WithdrawalTx;
import com.util.Generate;
import entitiesSupport.APICredentical;
import entitiesSupport.ResponseAPI;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import sun.text.resources.ja.JavaTimeSupplementary_ja;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class WithdrawalEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(WithdrawalEndpoint.class.getSimpleName());

    public static String HOST_URL = "https://1-dot-digitalwalletservice.appspot.com";
    public static String API_KEY = "d0179161-6cec-4691-aaa1-181ca13143c3";
    public static String API_SECRET_KEY = "4b133b7e-9dda-4d7c-a54a-f6f6c1770f670.8091180048930605";
    private static String ACCOUNT_BTC = "6451085a-f549-42dc-90f7-ba8d783bc4a8";
    private static String ACCOUNT_BCH = "4c31feb9-1c5b-49a5-a83a-e44142622461";
    private static String ACCOUNT_ETH = "0x746ad0f8082dd4585d532adc6601e4e94971e556";
    private static String ACCOUNT_USDT = "61e9ad59-71fe-43a6-95a1-6f757cdf9fe1";
    private static String ACCOUNT_XRP = "c4de2390-34da-4f8a-98ef-49c133248023";
    private static String Address = "rwpnQ3c7LWzHD8c4fTXPKZU5NakF3KmnSa";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 5) {
            logger.warning("parameter missing");
            RESTUtil.showMissingParams(req, resp);
            return;
        }
        APICredentical apiKey = (APICredentical) req.getAttribute("APIKey");
        String currency = req.getParameter("currency");
        if (apiKey == null || currency == null) {
            logger.warning("APIKey null or currency null");
            RESTUtil.showMissingParams(req, resp);
            return;
        }
        getWidthrawal(req, resp, currency, apiKey);
    }

    private void getWidthrawal(HttpServletRequest req, HttpServletResponse resp, String currency, APICredentical apiCredentical) throws IOException {
        User user = ofy().load().type(User.class).id(apiCredentical.getUserId()).now();
        if (user == null) {
            resp.setStatus(404);
            RESTUtil.showEmptyResult(req, resp, "User not found or has been deleted!");
            return;
        }
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", currency).filter("userId", user.getEmail()).first().now();
        if (wallet == null) {
            resp.setStatus(404);
            RESTUtil.showEmptyResult(req, resp, "Wallet not found or has been deleted!");
            return;
        }
        logger.info("USER: " + RESTUtil.gson.toJson(user));

        String currencytoLowerCase = currency.toLowerCase();
        logger.info("Currency lower case: " + currencytoLowerCase);
        switch (currencytoLowerCase) {
            case "btc":
                createWidthrawal(req,resp, "btc", ACCOUNT_BTC, wallet);
            case "bch":
                createWidthrawal(req,resp, "bch", ACCOUNT_BCH, wallet);

            case "eth":
                createWidthrawal(req,resp, "eth", ACCOUNT_ETH, wallet);

            case "usdt":
                createWidthrawal(req,resp, "usdt", ACCOUNT_USDT, wallet);

            case "xrp":
                createWidthrawal(req,resp, "xrp", ACCOUNT_XRP, wallet);

            default:
                logger.warning("LOI DEFAULT");
                break;
        }
    }

    private void createWidthrawal(HttpServletRequest req, HttpServletResponse resp, String currency, String accountId, Wallet wallet) throws IOException {
        HashMap<String, String> headers = new HashMap<>();
        String addressReceive = req.getParameter("address");
        Double amount = Double.parseDouble(req.getParameter("quantity"));
        //put request body
        HashMap<String, Object> data = new HashMap<>();
        data.put("to", addressReceive);
        data.put("amount", String.valueOf(amount));
        HashMap<String, Object> map = new HashMap<>();
        map.put("multip", false);
        map.put("data", data);
        //put header
        String timeStamp = String.valueOf(Generate.currentTime() / 1000);
        String method = "POST";
        String url = "/v2/" + currency + "/transfer/" + accountId + "/transaction";
        String rawSignature = timeStamp + method + url;
        logger.info("Raw Signature Before Coverting: " + rawSignature);
        String HMACSignature = com.util.RESTUtil.toHMAC256(rawSignature, API_SECRET_KEY);
        logger.info("HMAC: " + HMACSignature);
        headers.put("CB-ACCESS-SIGN", HMACSignature);
        headers.put("CB-ACCESS-KEY", API_KEY);
        headers.put("CB-ACCESS-TIMESTAMP", timeStamp);
        Connection.Response rsp = Jsoup.connect(HOST_URL + url)
                .headers(headers)
                .ignoreContentType(true)
                .requestBody(RESTUtil.gson.toJson(map))
                .method(Connection.Method.POST)
                .ignoreHttpErrors(true)
                .execute();
        logger.info("response: " + rsp);
        if (rsp.statusCode() != 201) {
            logger.info("response create address vps null!");
            com.util.RESTUtil.showInternalError(req, resp, "Response create address from VPS null! Please try again!");
            return;
        }
        String content = rsp.body();
        logger.info("content: " + content);
        JSONObject obj = new JSONObject(content);
        WithdrawalTx withdrawalTx = new WithdrawalTx();
        withdrawalTx.setId(UUID.randomUUID().toString());
        withdrawalTx.setType(CurrenryType.getint(currency.toUpperCase()));
        withdrawalTx.setAmount(String.valueOf(amount));
        withdrawalTx.setCreated_at(Generate.getTimeUTC());
        withdrawalTx.setReceiving_address(addressReceive);
        withdrawalTx.setStatus("pending");
        try {
            withdrawalTx.setHash(obj.getJSONObject("network").getString("hash"));
        } catch (JSONException e) {
            logger.warning("fail 141");
            RESTUtil.showJsonError(req, resp);
            return;
        }
        withdrawalTx.setWalletId(wallet.getId());
        ofy().save().entity(withdrawalTx).now();
        HashMap<String, String> map1 = new HashMap<>();
        map1.put("uuid", withdrawalTx.getId());
        ResponseAPI responseAPI = new ResponseAPI();
        responseAPI.setResult(map1);
        resp.getWriter().println(RESTUtil.gson.toJson(responseAPI));
    }

}

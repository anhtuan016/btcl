package endpointSupport.accountAPI;

import RESTULTI.RESTUtil;
import RESTULTI.RespMsg;
import entitiesSupport.*;
import com.entities.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class Getdeposithistory extends HttpServlet {
    Logger logger = Logger.getLogger(Getdeposithistory.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        APICredentical apiKey = (APICredentical) req.getAttribute("APIKey");
        if (apiKey == null) {
            logger.warning("APIKEY null!");
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "APIKEy not found or has been deleted", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }

        User user = ofy().load().type(User.class).id(apiKey.getUserId()).now();
        if (user == null) {
            logger.warning("User null");
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "User not found or has been deleted", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        String strCase = uriSplit[uriSplit.length - 1];
        switch (strCase) {
            case "getdeposithistory":
                getdeposithistory(req, resp, user);
                break;
            default:
                resp.setStatus(400);
                ResponseAPI responseAPI = new ResponseAPI(false, "Can not find request", null);
                resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
        }
    }

    private void getdeposithistory(HttpServletRequest req, HttpServletResponse resp, User user) throws IOException {
        String curentcyType = req.getParameter("currency");
        int i = CurrenryType.getint(curentcyType);
        logger.info("GET HISTORY DEPOSIT");
        List<Wallet> list = ofy().load().type(Wallet.class).filter("userId", user.getEmail()).list();
        logger.info("List wallet: " + list.size() + " data: " + RESTUtil.gson.toJson(list));
        List<Object> listResp = new ArrayList<>();
        List<Object> listWalletId = new ArrayList<>();
        for (Wallet wallet : list) {
            listWalletId.add(wallet.getId());
        }
        List<DepositTx> depositTxList = ofy().load().type(DepositTx.class).filter("type", i).filter("walletId in", listWalletId).list();
        if (depositTxList.size() == 0) {
            resp.setStatus(400);
            ResponseAPI responseAPI = new ResponseAPI(false, "LIST DEPOSIT SIZE = 0", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        logger.warning("List: " + RESTUtil.gson.toJson(depositTxList));
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
        for (DepositTx depositTx : depositTxList) {
            HashMap<String, Object> depositmap = new HashMap<>();
            depositmap.put("PaymentUuid", depositTx.getId());
            depositmap.put("Currency", CurrenryType.getCurrery(i));
            depositmap.put("Amount", depositTx.getAmount());
            depositmap.put("Address", depositTx.getReceiving_address());
            depositmap.put("Opened", depositTx.getCreated_at());
            depositmap.put("Authorized", true);
            if (depositTx.getStatus().equalsIgnoreCase("pending")) {
                depositmap.put("PendingPayment", true);
            }
            if (depositTx.getStatus().equalsIgnoreCase("complete")) {
                depositmap.put("PendingPayment", false);
            }
            depositmap.put("TxCost", null);
            depositmap.put("TxId", depositTx.getHash());
            depositmap.put("Canceled", false);
            depositmap.put("InvalidAddress", false);
            listResp.add(depositmap);
        }
        ResponseAPI responseAPI = new ResponseAPI();
        responseAPI.setSuccess(true);
        responseAPI.setMessage("");
        responseAPI.setResult(listResp);
        logger.info(RESTUtil.gson.toJson(responseAPI));
        resp.getWriter().println(RESTUtil.gson.toJson(responseAPI));

    }

}


package endpointSupport.accountAPI;

import RESTULTI.RESTUtil;
import RESTULTI.RespMsg;
import com.util.Generate;
import entitiesSupport.APICredentical;
import com.entities.*;
import entitiesSupport.ResponseAPI;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class OrderEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(Order.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 5) {
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Parameters are missing", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        APICredentical apiKey = (APICredentical) req.getAttribute("APIKey");
        if (apiKey == null) {
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "APIKey not found or has been deleted!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        logger.info("API OBJECT: " + RESTUtil.gson.toJson(apiKey));
        long uuid = Long.parseLong(req.getParameter("uuid"));
        if (uuid == 0) {
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Parameters are missing", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        Order order = ofy().load().type(Order.class).id(uuid).now();
        if (order == null) {
            resp.setStatus(404);
            ResponseAPI responseAPI = new ResponseAPI(false, "Order not found or has been deleted!", null);
            resp.getWriter().print(RESTUtil.gson.toJson(responseAPI));
            return;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
        HashMap<String, Object> map = new HashMap<>();
        map.put("AccountId", null);
        map.put("OrderUuid", uuid);
        map.put("Exchange", order.getPair());
        map.put("Type", order.getType());
        map.put("Quantity", order.getAmount());
        map.put("QuantityRemaining", order.getAmount());
        map.put("Limit", "");
        map.put("Reserved", null);
        map.put("ReserveRemaining", null);
        map.put("CommissionReserved", null);
        map.put("CommissionReserveRemaining", null);
        map.put("CommissionPaid", null);
        map.put("Price", order.getPrice());
        map.put("PricePerUnit", null);
        map.put("Opened", format.format(order.getCreated_at()));
        map.put("Closed", null);
        map.put("IsOpen", true);
        map.put("Sentinel", null);
        map.put("CancelInitiated", false);
        map.put("ImmediateOrCancel", false);
        map.put("IsConditional", false);
        map.put("Condition", "NONE");
        map.put("ConditionTarget", null);
        ResponseAPI responseAPI = new ResponseAPI();
        responseAPI.setSuccess(true);
        responseAPI.setMessage("");
        responseAPI.setResult(map);
        logger.info(RESTUtil.gson.toJson(responseAPI));

        resp.getWriter().println(RESTUtil.gson.toJson(responseAPI));
    }
}

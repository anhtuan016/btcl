package com.endpoint;

import com.entities.Order;
import com.entities.Wallet;
import com.pusher.rest.data.Result;
import com.util.RESTUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.logging.Logger;

import static com.endpoint.OrderEndpoint.rollback_key;
import static com.googlecode.objectify.ObjectifyService.ofy;

public class RollbackEndpoint extends HttpServlet {
    Wallet wallet = null;
    Logger logger = Logger.getLogger(RollbackEndpoint.class.getSimpleName());

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] uriSplit = req.getRequestURI().split("/");
        String strCase = uriSplit[uriSplit.length - 1];
        if (strCase.equals("rollback")) {
            if (req.getHeaders("key")==null){
                logger.warning("header null");
                return;
            }
            if (!req.getHeader("key").equals(rollback_key)) {

                return;
            }
            rollback(req, resp);
        }
    }

    private void rollback(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("roll back order");
        if (req.getParameter("id") == null) {
            logger.warning("parameter null");
            return;
        }
        Order order = ofy().load().type(Order.class).id(Long.parseLong(req.getParameter("id"))).now();
        if (order == null) {
            logger.warning("Order null:" + req.getParameter("id"));
            return;
        }
        if (!order.getValid().equals("processing")) {
            logger.warning("order is not processing");
            return;
        }
        order.setValid("failed");
        if (ofy().save().entity(order).now() == null) {
            logger.warning("save order error. order id:" + order.getId());
            return;
        }
        Result result = OrderEndpoint.pusher.trigger("orderCreated-" + order.getUserId(), "failed", Collections.singletonMap("data", RESTUtil.gson.toJson(order)));
        logger.info("result + orderCreated: " + result.getMessage() + " status: " + result.getStatus());
        if (order.getType().equals("buy")) {
            String[] splipType = order.getPair().split("-");
            wallet = ofy().load().type(Wallet.class).filter("userId", order.getUserId()).filter("type", splipType[0]).first().now();
            if (wallet == null) {
                logger.warning("wallet null: " + order.getUserId());
                return;
            }
            BigDecimal returnMoney = BigDecimal.valueOf(order.getPrice()).multiply(BigDecimal.valueOf(order.getAmount())).multiply(BigDecimal.valueOf(1.0025));
            wallet.setAmount(RESTUtil.addDouble(wallet.getAmount(), returnMoney.doubleValue()));
            logger.info("amount:" + wallet.getAmount());
            if (ofy().save().entity(wallet).now() == null) {
                logger.warning("roll back buy order error: " + wallet.getId());
                return;
            }
            logger.info("okey");
        }
        if (order.getType().equals("sell")) {
            String[] splipType = order.getPair().split("-");
            wallet = ofy().load().type(Wallet.class).filter("userId", order.getUserId()).filter("type", splipType[1]).first().now();
            if (wallet == null) {
                logger.warning("wallet null: " + order.getUserId());
                return;
            }
            BigDecimal returnMoney = BigDecimal.valueOf(order.getAmount());
            wallet.setAmount(RESTUtil.addDouble(wallet.getAmount(), returnMoney.doubleValue()));
            logger.info("amount:" + wallet.getAmount());
            if (ofy().save().entity(wallet).now() == null) {
                logger.warning("roll back sell order error: " + wallet.getId());
                return;
            }
            logger.info("okey");
        }
        //            PUSHER CHANGE WALLET BALANCE.
        com.pusher.rest.data.Result result1 = OrderEndpoint.pusher.trigger("walletbalance-" + wallet.getUserId(), wallet.getType(), Collections.singletonMap("data", RESTUtil.gson.toJson(wallet)));
        logger.info("result + Change wallet Balance: " + result.getMessage() + " status: " + result.getStatus());


    }
}

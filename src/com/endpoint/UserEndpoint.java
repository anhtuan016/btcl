package com.endpoint;

import com.entities.*;
import com.util.*;

import com.entities.User;
import com.entities.UserActivity;
import com.entities.UserCredential;
import com.entities.Wallet;


import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class UserEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(UserEndpoint.class.getSimpleName());

//    static String strActi = "https://1-dot-bittrexcln.appspot.com/api/v1/user/activation?code=";
     String strActi = "https://bitcheck-208302.firebaseapp.com/activation/";
     String strIpAddress = "https://1-dot-bittrexcln.appspot.com/api/v1/user/ipverification?id=";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] str = req.getRequestURI().split("/");
        if (str.length != 5) {
            RESTUtil.showWrongURLError(req, resp);
            return;
        }

        if ("allwallet".equalsIgnoreCase(str[str.length - 1])) {
            UserCredential credential = (UserCredential) req.getAttribute("credential");
            String userId = credential.getUserEmail();
            List<Wallet> walletList = ofy().load().type(Wallet.class).filter("userId", userId).list();
            if (walletList.size() == 0) {
                resp.setStatus(404);
                RespMsg msg = new RespMsg(404, "Not found", "Wallets are not exist");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
                return;
            }
            resp.setStatus(200);
            resp.getWriter().println(RESTUtil.gson.toJson(walletList));
            return;
        }
        if ("wallet".equalsIgnoreCase(str[str.length - 1])) {
            UserCredential credential = (UserCredential) req.getAttribute("credential");
            showWallet(req, resp, credential);
            return;
        }
        if ("activation".equalsIgnoreCase(str[str.length - 1])) {
            ActiveCode(req, resp);
            return;
        }
        if ("ipverification".equalsIgnoreCase(str[str.length - 1])) {
            IpVerification(req, resp);
            return;
        }
        if ("myprofile".equalsIgnoreCase(str[str.length - 1])) {
            getCurrentUser(req, resp);
            return;
        }

        if ("getopenorders".equalsIgnoreCase(str[str.length - 1])) {
            UserCredential credential = (UserCredential) req.getAttribute("credential");
            getAllOpenOrder(req, resp, credential);
            return;
        }
        if ("getoldorders".equalsIgnoreCase(str[str.length - 1])) {
            UserCredential credential = (UserCredential) req.getAttribute("credential");
            getOldOrders(req, resp, credential);
            return;
        }
    }

    private void getOldOrders(HttpServletRequest req, HttpServletResponse resp, UserCredential credential) throws IOException {
        String userId = credential.getUserEmail();
        int page = 1;
        int limit = 10;
        String pageStr = req.getParameter("page");
        String limitStr = req.getParameter("limit");
        try {
            page = Integer.parseInt(pageStr);
            limit = Integer.parseInt(limitStr);
        } catch (NumberFormatException e) {
            page = 1;
            limit = 10;
        }
        int totalItem = ofy().load().type(Order.class).filter("userId", userId).filter("status <", 2).count();
        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(1);
        listStatus.add(0);
        List<Order> orderHistory = ofy().load().type(Order.class).filter("status in", listStatus).filter("userId", userId).order("-updated_at").offset((page - 1) * limit).limit(limit).list();
        logger.info("orderHistory is : " + RESTUtil.gson.toJson(orderHistory));

//        SORT 1 MORE TIME.
//        Generate.MyComparator_Order_UpdatedTime comparator = new Generate.MyComparator_Order_UpdatedTime();
//        Collections.sort(orderHistory, comparator);
        logger.info("orderHistory after sort 2nd times is : " + RESTUtil.gson.toJson(orderHistory));
        HashMap<String, Object> data = new HashMap<>();
        data.put("totalItem", totalItem);
        data.put("data", orderHistory);
        resp.getWriter().println(RESTUtil.gson.toJson(data));
        resp.setStatus(200);
        return;
    }

    private void getAllOpenOrder(HttpServletRequest req, HttpServletResponse resp, UserCredential credential) throws IOException {
        String userId = credential.getUserEmail();
        int page = 1;
        int limit = 10;
        String pageStr = req.getParameter("page");
        String limitStr = req.getParameter("limit");
        try {
            page = Integer.parseInt(pageStr);
            limit = Integer.parseInt(limitStr);
        } catch (NumberFormatException e) {
            page = 1;
            limit = 10;
        }
        int totalItem = ofy().load().type(Order.class).filter("userId", userId).filter("status", 2).count();
        List<Order> openOrderList = ofy().load().type(Order.class).filter("userId", userId).filter("status", 2).order("-created_at").offset((page - 1) * limit).limit(limit).list();
        logger.info("OpenOrder list is : " + RESTUtil.gson.toJson(openOrderList));
        HashMap<String, Object> data = new HashMap<>();
        data.put("totalItem", totalItem);
        data.put("data", openOrderList);
        logger.info("Data: " + RESTUtil.gson.toJson(data));
        resp.getWriter().println(RESTUtil.gson.toJson(data));
        resp.setStatus(200);
        return;
    }


    private void getCurrentUser(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        User user = ofy().load().type(User.class).id(credential.getUserEmail()).now();
        if (user==null){
            RESTUtil.showEmptyResult(req,resp,"User is not exist or is deactivated");
            return;
        }
        resp.setCharacterEncoding("UTF-8");

        resp.getWriter().println(RESTUtil.gson_noEncode.toJson(user));

    }

    private void showWallet(HttpServletRequest req, HttpServletResponse resp, UserCredential credential) throws IOException {
        String userId = credential.getUserEmail();
        String type = req.getParameter("type");
        if (type == null) {
            RESTUtil.showMissingParams(req, resp);
            return;
        }
        Wallet wallet = ofy().load().type(Wallet.class).filter("userId", userId).filter("type", type.toUpperCase()).first().now();
        logger.info("GET Wallet: " + RESTUtil.gson.toJson(wallet));
        if (wallet == null) {
            RESTUtil.showEmptyResult(req, resp);
            return;
        }
        resp.getWriter().println(RESTUtil.gson.toJson(wallet));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] str = req.getRequestURI().split("/");
        if (!str[str.length - 2].equals("user") && str.length == 5) {
            RESTUtil.showWrongURLError(req, resp);
            return;
        }
        String strCase = str[str.length - 1];
        switch (strCase) {
            case "register":
                register(req, resp);
                break;
            case "login":
                try {
                    login(req, resp);

                } catch (JSONException e) {
                    RESTUtil.showJsonError(req, resp, "Missing information");
                    return;
                }
                break;
            default:
                resp.setStatus(400);
                RespMsg msg = new RespMsg(400, "Bad Request", "URL is not well-form");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
                break;
        }
    }

    private void register(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = RESTUtil.readStringInput(req.getInputStream());
        logger.info("data: " + data);

        if (data.length() == 0) {
            logger.warning("data is empty");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Lack of content for register");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        HashMap<String, String> map = RESTUtil.gson.fromJson(data, HashMap.class);
        if (map.get("password") == null ) {
            logger.warning("password is missing");
            RESTUtil.showMissingParams(req, resp, "Password is missing:");
            return;
        }
        if ( map.get("email") == null ) {
            logger.warning("email is missing");
            RESTUtil.showMissingParams(req, resp, "Email is missing:");
            return;
        }
        if (ofy().load().type(User.class).id(map.get("email")).now()!=null){
            logger.warning("email has been registered");
            RESTUtil.showMissingParams(req, resp, "Email has been registered");
            return;
        }
        String key_id = map.get("email");
        User user = new User("", map.get("password"), map.get("email"));
        GoogleAuthenticator gAuth = new GoogleAuthenticator();
        GoogleAuthenticatorKey key = gAuth.createCredentials();
        String secret_key = key.getKey();
        String qrUrl = TwoFactorUtils.generateQrUrl(key_id, secret_key);
        logger.info("key_id: " + key_id);
        logger.info("secret_key: " + secret_key);
        logger.info("qrUrl: " + qrUrl);
        user.setTwofactorauth(0);
        user.setSecret_key(secret_key);
        user.setQr_url(qrUrl);
        if (map.get("country") != null && map.get("sponsorId") != null) {
            user.setCountry(map.get("country"));
            user.setSponsorId(map.get("sponsorId"));
        }
        if (user.validateRegister().size()!=0) {
            logger.warning("server error:" + RESTUtil.gson.toJson(user.validateRegister()));
            resp.setStatus(400);
            resp.getWriter().print(RESTUtil.gson.toJson(user.validateRegister()));
            return;
        }
        if (ofy().save().entity(user).now() == null) {
            RESTUtil.showInternalError(req, resp, "Internal Error. Please try again!");
            return;
        }
        try {
            SendMail.Activation(user.getEmail(), strActi + user.getActiveCode(),"BittrexClone Account Verification");
            logger.info("Link acti: "+strActi + user.getActiveCode());
        } catch (Exception ex) {
            logger.warning("Cannot send activation mail");
            RESTUtil.showForbiddenError(req, resp, "Cannot send activation mail");
            return;
        }
        UserActivity userActivity = new UserActivity(req,"VERIFY_NEW_IP",user.getEmail());
        userActivity.setStatus(1);
        if (ofy().save().entity(userActivity).now()==null){
            logger.warning("cannot save new ip");
            return;
        }
//        user.initiatedWallet();// cmt khi hoan thanh
        resp.setStatus(201);
        resp.getWriter().print(RESTUtil.gson_noEncode.toJson(user));
    }


    private void login(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, JSONException {
        String email = (String) req.getAttribute("email");
        User user = ofy().load().type(User.class).id(email).now();
        if (user.getStatus() == 2) {
            logger.warning("User have not been activated yet");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "User have not been activated yet", "PLease active your account by mail.");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (user.getStatus() == 0) {
            logger.warning("User havent been deleted or not exist");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "User not found", "User not found or has been deleted");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }

        UserActivity userActivity = new UserActivity(req,"LOGIN",user.getEmail());
        if (ofy().save().entity(userActivity).now() == null) {
            logger.warning("Cannot save user Activity");
            RESTUtil.showForbiddenError(req, resp, "Cannot save user Activity");
            return;
        }
        createCredential(req, resp, user);

    }

    private void createCredential(HttpServletRequest req, HttpServletResponse resp, User user) throws IOException {
        UserCredential credential = new UserCredential(user.getEmail());
        if (ofy().save().entity(credential).now() == null) {
            RESTUtil.showInternalError(req, resp, "Cannot create credential");
            return;
        }
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(credential));
    }


    /**
     * GET ACTIVATION CODE
     */
//    @Override
//    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String uriSplit[] = req.getRequestURI().split("/");
//        if (uriSplit[uriSplit.length - 1].equalsIgnoreCase("2fa")) {
//            modify2fa(req, resp);
//            return;
//        }
//
//    }


    private void ActiveCode(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String active_code = req.getParameter("code");
        User user = ofy().load().type(User.class).filter("activeCode", active_code).first().now();
        if (user == null) {
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "User not found", "User not found!");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        user.setStatus(1);
        user.initiatedWallet(); // bo cmt khi hoan thanh
        if (ofy().save().entity(user).now() == null) {
            resp.setStatus(500);
            RespMsg msg = new RespMsg(500, "Error Occured", "Cannot activate account, please try again.");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        resp.getWriter().print("Success! "+user.getEmail());
    }

    private void IpVerification(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("id")==null || req.getParameter("id").length()==0){
            RESTUtil.showWrongURLError(req,resp);
            return;
        }
        String strId = req.getParameter("id");
        UserActivity userActivity = ofy().load().type(UserActivity.class).id(Long.parseLong(strId)).now();
        if (userActivity == null) {
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "ip not found!");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        logger.info("load ip: " + userActivity.getUserName() + "-ip:" + userActivity.getIpAddress());
        if (ofy().load().type(UserActivity.class).filter("userName",userActivity.getUserName()).filter("ipAddress",userActivity.getIpAddress()).filter("activity","VERIFY_NEW_IP").first().now()!=null){
            resp.sendRedirect("https://bitcheck-208302.firebaseapp.com/login");
            return;
        }
        UserActivity userVerify = new UserActivity(req,"VERIFY_NEW_IP",userActivity.getUserName());
        userVerify.setIpAddress(userActivity.getIpAddress());
        userVerify.setStatus(1);
        if (ofy().save().entity(userVerify).now() == null) {
            resp.setStatus(500);
            RespMsg msg = new RespMsg(500, "Error Occured", "Cannot activate ip, please try again.");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        logger.info("verify");
        resp.sendRedirect("https://bitcheck-208302.firebaseapp.com/login");
    }

//    private void modify2fa(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        String enable = req.getParameter("enable");
//        if ("true".equalsIgnoreCase(enable)) {
//            enable2fa(req, resp);
//            return;
//        }
//        if ("false".equalsIgnoreCase(enable)) {
//            disable2fa(req, resp);
//        }
//    }

    private void disable2fa(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        JSONObject obj = new JSONObject(RESTUtil.readStringInput(req.getInputStream()));
        int totp;
        try {
            totp = obj.getInt("totp");
        } catch (JSONException a) {
            logger.warning("Missing TOTP");
            RESTUtil.showMissingParams(req, resp, "Missing TOTP");
            return;
        }
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        User user = ofy().load().type(User.class).id(credential.getUserEmail()).now();
        String key_id = user.getEmail();
        if (!TwoFactorUtils.isValid(user.getSecret_key(), totp)) {
            RESTUtil.showForbiddenError(req, resp, "Invalid TOTP");
            return;
        }
        user.setTwofactorauth(0);
        if (ofy().save().entity(user).now() == null) {
            RESTUtil.showInternalError(req, resp, "Cannot update the user information! Please try again!");
            return;
        }
        resp.getWriter().println(RESTUtil.gson.toJson(user));
    }

    private void enable2fa(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        JSONObject obj = new JSONObject(RESTUtil.readStringInput(req.getInputStream()));
        int totp;
        try {
            totp = obj.getInt("totp");
        } catch (JSONException a) {
            logger.warning("Missing TOTP");
            RESTUtil.showMissingParams(req, resp, "missing TOTP");
            return;
        }
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        User user = ofy().load().type(User.class).id(credential.getUserEmail()).now();
        String key_id = user.getEmail();
        if (!TwoFactorUtils.isValid(user.getSecret_key(), totp)) {
            RESTUtil.showForbiddenError(req, resp, "Invalid TOTP");
            return;
        }
        user.setTwofactorauth(1);
        if (ofy().save().entity(user).now() == null) {
            RESTUtil.showInternalError(req, resp, "Internal Error. Please try again");
            return;
        }
        resp.getWriter().println(RESTUtil.gson.toJson(user));
    }
}

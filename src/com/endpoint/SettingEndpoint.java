package com.endpoint;

import com.entities.*;
import com.util.*;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class SettingEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(UserEndpoint.class.getSimpleName());
    static String url = "https://1-dot-bittrexcln.appspot.com/api/v1/settings/resetpassword?id=";
    static String strActi = "https://bitcheck-208302.firebaseapp.com/confirm2fa/";
//    static String strActi = "https://2-dot-bittrexcln.appspot.com/api/v1/settings/authentication/activate?id=";

    //    <--------------------------------------do get----------------------------------->
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] uriSplit = req.getRequestURI().split("/");
        String strCase = uriSplit[uriSplit.length - 1];
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        if (credential == null) {
            logger.warning("credential null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find credential");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (strCase.equalsIgnoreCase("myactivity")) {
            int page, limit;
            try {
                page = Integer.parseInt(req.getParameter("page"));
                limit = Integer.parseInt(req.getParameter("limit"));
            } catch (Exception e) {
                logger.warning("page limit");
                resp.setStatus(400);
                RespMsg msg = new RespMsg(400, "bad resquest", "integer parse");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
                return;
            }
            if (page < 1 || limit < 1) {
                logger.warning("page or limit <1");
                resp.setStatus(400);
                RespMsg msg = new RespMsg(400, "bad resquest", "page or limit <1");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
                return;
            }
            myActivity(req, resp, credential, page, limit);
            return;
        }

        if (strCase.equalsIgnoreCase("getaddress")) {
            getAddress(req, resp, credential);
            return;
        }
        if (strCase.equalsIgnoreCase("getip")) {
            getip(req, resp, credential);
            return;
        }
        if (strCase.equalsIgnoreCase("getIdentity")) {
            getIdentity(req, resp, credential);
            return;
        }
        RESTUtil.showWrongURLError(req, resp);

    }

    private void getip(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential) throws ServletException, IOException {
        logger.info("get ip address list");
        List<UserActivity> list = ofy().load().type(UserActivity.class).filter("userName", userCredential.getUserEmail()).filter("status", 2).list();
        HashMap<String, Object> hashMap = new HashMap<>();
        List<Object> objectList = new ArrayList<>();
        for (UserActivity userActivity : list) {
            HashMap<String, Object> mapAddress = new HashMap<>();
            mapAddress.put("ip", userActivity.getIpAddress());
            objectList.add(mapAddress);
        }
        logger.info("totalitem: " + objectList.size());
        hashMap.put("totalItem", objectList.size());
        hashMap.put("data", objectList);
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(hashMap));
    }

    private void getAddress(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential) throws ServletException, IOException {
        List<Wallet> walletList = ofy().load().type(Wallet.class).filter("userId", userCredential.getUserEmail()).list();
        List<Long> listWalletId = new ArrayList<>();
        for (Wallet wallet : walletList) {
            listWalletId.add(wallet.getId());
        }
        List<WithdrawalWhitelist> list = ofy().load().type(WithdrawalWhitelist.class).filter("walletId in", listWalletId).filter("status", 1).list();
        HashMap<String, Object> hashMap = new HashMap<>();

        List<Object> objectList = new ArrayList<>();

        for (WithdrawalWhitelist address : list) {
            HashMap<String, Object> mapAddress = new HashMap<>();
            mapAddress.put("address", address.getAddress());
            mapAddress.put("type", AddressType.getSymbol(address.getType()));
//            mapAddress.put("walletId", address.getWalletId());
            objectList.add(mapAddress);
        }
        hashMap.put("totalItem", objectList.size());
        hashMap.put("data", objectList);
        resp.setStatus(200);
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().print(RESTUtil.gson.toJson(hashMap));
    }

    private void getIdentity(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential) throws ServletException, IOException {
        User user = ofy().load().type(User.class).id(userCredential.getUserEmail()).now();
        if (user == null) {
            RESTUtil.showMissingParams(req, resp, "Credential not found!");
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("identityCard", user.getIdentifyCard_url());
        map.put("passport", user.getPassport_url());
        logger.info(RESTUtil.gson.toJson(map));
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(map));
    }


    private void myActivity(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential, int page, int limit) throws ServletException, IOException {
        User user = ofy().load().type(User.class).id(userCredential.getUserEmail()).now();
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        int count = ofy().load().type(UserActivity.class).filter("userName", user.getEmail()).orderKey(true).count();

        PageLimit data = new PageLimit(page, limit, count);
        if (data.validate().size() != 0) {
            resp.setStatus(400);
            logger.info("validate page limit");
            return;
        }
        List<UserActivity> actiList = ofy().load().type(UserActivity.class).filter("userName", user.getEmail()).orderKey(true).limit(limit).offset(limit * (page - 1)).list();

        HashMap<String, Object> activity = new HashMap<>();
        activity.put("page", page);
        activity.put("limit", limit);
        activity.put("totalPage", data.getTotalPage());
        activity.put("totalItem", data.getTotalItem());
        activity.put("data", actiList);

        String strStatus = null;
        try {
            if (user.getIdentifyCard_url() != null || user.getPassport_url() != null){
                strStatus = "verified";
            }else { strStatus = "unverified";}

        } catch (NullPointerException e) {
            strStatus = "unverified";
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("userName", user.getEmail());
        map.put("status", strStatus);
        map.put("withdrawal", "0 BTC");
        map.put("activity", activity);

        logger.info(RESTUtil.gson.toJson(map));
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(map));

    }


    //<------------------------------do post------------------------------------>
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] uriSplit = req.getRequestURI().split("/");
        String strCase = uriSplit[uriSplit.length - 1];
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        if (credential == null) {
            logger.warning("credential null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find credential");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (strCase.equalsIgnoreCase("resetpassword")) {
            resetPass(req, resp, credential);
            return;
        }

        if (strCase.equalsIgnoreCase("information")) {
            Information(req, resp, credential);
            return;
        }
        if (strCase.equalsIgnoreCase("addaddress")) {
            addAddress(req, resp, credential);
            return;
        }
        if (strCase.equalsIgnoreCase("deleteaddress")) {
            DeleteAddress(req, resp, credential);
            return;
        }
        if (strCase.equalsIgnoreCase("addip")) {
            addIpAddress(req, resp, credential, "addip");
            return;
        }
        if (strCase.equalsIgnoreCase("deleteip")) {
            addIpAddress(req, resp, credential, "delete");
            return;
        }
        if (strCase.equalsIgnoreCase("enable")) {
            twoFactor(req, resp, credential, "enable");
            return;
        }
        if (strCase.equalsIgnoreCase("disable")) {
            twoFactor(req, resp, credential, "disable");
            return;
        }
        if (strCase.equalsIgnoreCase("identity")) {
            identity(req, resp, credential);
            return;
        }

        RESTUtil.showWrongURLError(req, resp);
    }


    private void twoFactor(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential, String strCase) throws ServletException, IOException {
        User user = ofy().load().type(User.class).id(userCredential.getUserEmail()).now();
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        String data = RESTUtil.readStringInput(req.getInputStream());
        if (data.length() == 0) {
            logger.warning("data null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "bad request", "enter address");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        JSONObject object = new JSONObject(data);
        String strAuthen;
        try {
            strAuthen = object.getString("authentication");
        } catch (JSONException e) {
            logger.warning("authentication null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "enter two-factor authentication code");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        } catch (NumberFormatException ex) {
            RESTUtil.showBadRequest(req, resp, "TOTP is invalid");
            return;
        }

        if (strCase.equals("enable")) {
            if (user.getTwofactorauth() != 0) {
                if (user.getTwofactorauth() == 1)
                    RESTUtil.showForbiddenError(req, resp, "Two-factor authentication enable");
                if (user.getTwofactorauth() == 2)
                    RESTUtil.showForbiddenError(req, resp, "Two-factor authentication pending");
                logger.warning("Two-factor authentication enable error");
                return;
            }
            if (!TwoFactorUtils.isValid(user.getSecret_key(), Integer.parseInt(strAuthen))) {
                logger.warning("totp is invalid");
                RESTUtil.showForbiddenError(req, resp, "TOTP is invalid");
                return;
            }
            user.setActiveCode(Generate.strCode());
            user.setTwofactorauth(2);// pending !!
            if (ofy().save().entity(user).now() == null) {
                RESTUtil.showInternalError(req, resp, "server error: not save");
                return;
            }
            try {
                SendMail.Activation(user.getEmail(), strActi + user.getActiveCode(), "Bittrex Two-Factor Authentication");
                logger.info("Link acti: " + strActi + user.getActiveCode());
            } catch (Exception ex) {
                logger.warning("Cannot send activation mail");
                RESTUtil.showForbiddenError(req, resp, "Cannot send activation mail");
                return;
            }
            UserActivity userActivity = new UserActivity(req, user.getEmail());
            userActivity.setActivity("PENDING_2FA");
            if (ofy().save().entity(userActivity).now() == null) {
                RESTUtil.showInternalError(req, resp, "server error: not save");
                return;
            }
            HashMap<String, String> mapresp = new HashMap<>();
            mapresp.put("enable", "Check mail activate");
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(mapresp));
            return;
        }
        if (strCase.equals("disable")) {
            if (user.getTwofactorauth() == 0) {
                logger.warning("Two-factor authentication disable");
                RESTUtil.showForbiddenError(req, resp, "Two-factor authentication disable");
                return;
            }
            if (!TwoFactorUtils.isValid(user.getSecret_key(), Integer.parseInt(strAuthen))) {
                logger.warning("totp is invalid");
                RESTUtil.showForbiddenError(req, resp, "TOTP is invalid");
                return;
            }
            user.setTwofactorauth(0);
            user.setActiveCode(Generate.strCode());
            if (ofy().save().entity(user).now() == null) {
                RESTUtil.showInternalError(req, resp, "server error: not save");
                return;
            }
            UserActivity userActivity = new UserActivity(req, user.getEmail());
            userActivity.setActivity("DISABLE_2FA");
            if (ofy().save().entity(userActivity).now() == null) {
                RESTUtil.showInternalError(req, resp, "server error: not save");
                return;
            }
            HashMap<String, String> mapresp = new HashMap<>();
            mapresp.put("disable", "Two Factor Authenticator is disabled successfully!");
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(mapresp));
            return;

        }
    }


    private void addIpAddress(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential, String strCase) throws ServletException, IOException {
        User user = ofy().load().type(User.class).id(userCredential.getUserEmail()).now();
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        String data = RESTUtil.readStringInput(req.getInputStream());
        if (data.length() == 0) {
            logger.warning("data null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "bad request", "enter address");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        JSONObject object = new JSONObject(data);
        String strAuth;
        String strIp;
        int TOTP;
        try {
            strAuth = object.getString("authentication");
            TOTP = Integer.parseInt(strAuth);
        } catch (JSONException e) {
            logger.warning("JSONException");
            RESTUtil.showBadRequest(req, resp, "TOTP is invalid");
            return;
        } catch (NumberFormatException ex) {
            logger.warning("NumberFormatException");
            RESTUtil.showBadRequest(req, resp, "TOTP is invalid");
            return;
        }
        try {
            strIp = object.getString("ip");
        } catch (JSONException e) {
            logger.warning("ip null");
            RESTUtil.showBadRequest(req, resp, "Ip address is invalid");
            return;
        }
        if (!TwoFactorUtils.isValid(user.getSecret_key(), TOTP)) {
            logger.warning("totp is invalid");
            RESTUtil.showForbiddenError(req, resp, "TOTP is invalid");
            return;
        }
        String[] ipSplit = strIp.split(".");
        try {
            for (String code : ipSplit) {
                if (Integer.parseInt(code) < 0) {
                    RESTUtil.showBadRequest(req, resp, "Ip address is invalid");
                    return;
                }
            }
        } catch (NumberFormatException e) {
            RESTUtil.showBadRequest(req, resp, "Ip address is invalid");
            return;
        }
//
        if (ofy().load().type(UserActivity.class).filter("userName", userCredential.getUserEmail()).filter("status", 2).count() >= 8) {
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Your account only limited 8 ip addresses!");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        UserActivity userAtvt = ofy().load().type(UserActivity.class).filter("ipAddress", strIp).filter("activity", "VERIFY_NEW_IP").filter("userName", user.getEmail()).first().now();
        if (strCase.equalsIgnoreCase("addip")) {
            logger.info(RESTUtil.gson.toJson(userAtvt));
            if (userAtvt != null && userAtvt.getStatus() == 2) {
                resp.setStatus(400);
                RespMsg msg = new RespMsg(400, "Bad request", "IP address already exists!");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            if (userAtvt != null && userAtvt.getStatus() != 2) {
                userAtvt.setStatus(2);
                logger.info("add ip: " + userAtvt.getIpAddress());
                if (ofy().save().entity(userAtvt).now() == null) {
                    logger.warning("error save useractivity");
                    resp.setStatus(500);
                    RespMsg msg = new RespMsg(500, "server error", "request again");
                    resp.getWriter().print(RESTUtil.gson.toJson(msg));
                    return;
                }
                HashMap<String, String> mapresp = new HashMap<>();
                mapresp.put("ip", userAtvt.getIpAddress());
                resp.setStatus(200);
                resp.getWriter().print(RESTUtil.gson.toJson(mapresp));
                return;
            }

            UserActivity userVerify = new UserActivity(req, "VERIFY_NEW_IP", user.getEmail());
            userVerify.setIpAddress(strIp);
            userVerify.setStatus(2);
            logger.info("add ip:" + userVerify.getIpAddress());
            if (ofy().save().entity(userVerify).now() == null) {
                logger.warning("error save useractivity");
                resp.setStatus(500);
                RespMsg msg = new RespMsg(500, "server error", "request again");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
                return;
            }
            HashMap<String, String> mapresp = new HashMap<>();
            mapresp.put("ip", userVerify.getIpAddress());
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(mapresp));
            return;


        }
        if (strCase.equalsIgnoreCase("delete")) {
            if (userAtvt == null) {
                logger.warning("Ip address does not exist");
                resp.setStatus(400);
                RespMsg msg = new RespMsg(400, "Bad request", " Ip address does not exist");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
                return;
            }
            logger.info("delete ip:" + userAtvt.getIpAddress());
            userAtvt.setStatus(0);
            if (ofy().save().entity(userAtvt).now() == null) {
                RESTUtil.showInternalError(req, resp, "Server error");
            }
            HashMap<String, String> mapresp = new HashMap<>();
            mapresp.put("delete", userAtvt.getIpAddress());
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(mapresp));
            return;

        }
    }


    private void addAddress(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential) throws ServletException, IOException {
        User user = ofy().load().type(User.class).id(userCredential.getUserEmail()).now();
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        String data = RESTUtil.readStringInput(req.getInputStream());
        if (data.length() == 0) {
            logger.warning("data null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Enter the full information");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        JSONObject object = new JSONObject(data);
        String strAddress = null;
        String strType = null;
        try {
            strAddress = object.getString("address");
            strType = object.getString("type");
        } catch (JSONException e) {
            RESTUtil.showJsonError(req, resp, "Enter address, type");
            return;
        }
        if (AddressType.getType(strType) == 0) {
            logger.warning("type null");
            RESTUtil.showBadRequest(req, resp, "Address type is invalid");
            return;
        }
        Wallet wallet = ofy().load().type(Wallet.class).filter("userId", user.getEmail()).filter("type", AddressType.getStringType(strType)).first().now();
        if (wallet == null) {
            logger.warning("wallet null");
            RespMsg msg = new RespMsg(500, "Server error", "Wallet is not found");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        int intTOTP;
        try {
            String strAuthen = object.getString("authentication");
            intTOTP = Integer.parseInt(strAuthen);
        } catch (JSONException e) {
            logger.warning("authentication:");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "TOTP is invalid");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        } catch (NumberFormatException ex) {
            RESTUtil.showBadRequest(req, resp, "TOTP is invalid");
            return;
        }
        if (!TwoFactorUtils.isValid(user.getSecret_key(), intTOTP)) {
            logger.warning("totp is invalid");
            RESTUtil.showForbiddenError(req, resp, "TOTP is invalid");
            return;
        }
        List<Wallet> walletList = ofy().load().type(Wallet.class).filter("userId", userCredential.getUserEmail()).list();
        List<Long> listWalletId = new ArrayList<>();
        for (Wallet walletL : walletList) {
            listWalletId.add(walletL.getId());
        }
        if (ofy().load().type(WithdrawalWhitelist.class).filter("walletId in", listWalletId).filter("status", 1).count() >= 8) {
            logger.warning("account only limited 8 addresses");
            RESTUtil.showBadRequest(req, resp, "Your account only limited 8 addresses!");
            return;
        }
        if (ofy().load().type(WithdrawalWhitelist.class).filter("address", strAddress).filter("walletId", wallet.getId()).first().now() != null) {
            logger.warning("Address already exists");
            RESTUtil.showBadRequest(req, resp, "Address already exists");
            return;
        }
        WithdrawalWhitelist address = new WithdrawalWhitelist();
        address.setAddress(strAddress);
        address.setWalletId(wallet.getId());
        address.setStatus(Generate.intRandom());
        address.setType(AddressType.getType(strType));
        address.setStatus(1);
        if (ofy().save().entity(address).now() == null) {
            logger.warning("save address error");

            return;
        }
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("Success", "Activate address success!");
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(hashMap));
    }


    private void Information(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential) throws ServletException, IOException {
        User user = ofy().load().type(User.class).id(userCredential.getUserEmail()).now();
        String data = RESTUtil.readStringInput(req.getInputStream());
        if (data.length() == 0) {
            logger.warning("data null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Enter your information");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        HashMap<String, String> map = RESTUtil.gson.fromJson(data, HashMap.class);
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (map.get("fullName") == null || map.get("fullName").length() == 0 || map.get("birthday") == null || map.get("birthday").length() == 0 || map.get("address") == null || map.get("address").length() == 0) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("detail", "IInformation is invalid");
            if (map.get("fullName") == null || map.get("fullName").length() == 0) {
                logger.warning("full name : " + data);
                hashMap.put("fullName", "null");
            }
            if (map.get("birthday") == null || map.get("birthday").length() == 0) {
                logger.warning(" birthday : " + data);
                hashMap.put("birthday", "null");
            }
            if (map.get("address") == null || map.get("address").length() == 0) {
                logger.warning("address: " + data);
                hashMap.put("address", "null");
            }
            resp.setStatus(400);
            resp.getWriter().print(RESTUtil.gson.toJson(hashMap));
            return;
        }
        user.setAddress(map.get("address"));
        user.setName(map.get("fullName"));
        user.setBirthday(map.get("birthday"));
        if (user.validateFull().size() != 0) {
            logger.warning("server error: validate user");
            resp.setStatus(400);
            HashMap<String, String> hashMap = user.validateFull();
            hashMap.put("detail", "Error valid");
            resp.getWriter().print(RESTUtil.gson.toJson(hashMap));
            return;
        }
        if (ofy().save().entity(user).now() == null) {
            logger.warning("user null");
            RespMsg msg = new RespMsg(500, "Server error", "Not update user. Please try again!");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        resp.setStatus(200);
        RESTUtil.showSuccess(req, resp, "Update information success!");
    }


    private void resetPass(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential) throws ServletException, IOException {
        User user = ofy().load().type(User.class).id(userCredential.getUserEmail()).now();
        String data = RESTUtil.readStringInput(req.getInputStream());
        JSONObject object = new JSONObject(data);
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (user.getTwofactorauth() == 1) {
            int intTOTP;
            try {
                intTOTP = Integer.parseInt(object.getString("authentication"));
            } catch (JSONException e) {
                RESTUtil.showJsonError(req, resp, "TOTP is invalid");
                return;
            } catch (NumberFormatException ex) {
                RESTUtil.showBadRequest(req, resp, "TOTP is invalid");
                return;
            }
            if (!TwoFactorUtils.isValid(user.getSecret_key(), intTOTP)) {
                logger.warning("totp is invalid");
                RESTUtil.showForbiddenError(req, resp, "TOTP is invalid");
                return;
            }
        }
        String strCurrentPassword;
        String strNewPassword;
        try {
            strCurrentPassword = object.getString("currentPassword");
        } catch (JSONException e) {
            resp.setStatus(400);
            logger.warning("currentPassword null");
            RESTUtil.showInternalError(req, resp, "Enter current password");
            return;
        }
        if (!Generate.md5(strCurrentPassword).equals(user.getPassword())) {
            RESTUtil.showBadRequest(req, resp, "Password is incorrect.");
            return;
        }
        try {
            strNewPassword = object.getString("newPassword");
        } catch (JSONException e) {
            logger.warning("newPassword null");
            RESTUtil.showBadRequest(req, resp, "Password incorrect");
            return;
        }
        if (Generate.md5(strNewPassword).equals(user.getPassword())) {
            logger.warning("");
            RESTUtil.showBadRequest(req, resp, "New password is identical to current password!");
            return;
        }
        user.setPassword(strNewPassword);
        if (user.validateRegister().size() != 0) {
            RESTUtil.showBadRequest(req, resp, "Password has to from 8-16 characters. There are at least 1 uppercase and 1 special character!");
            logger.warning("server error:" + RESTUtil.gson.toJson(user.validateRegister()));
            return;
        }
        if (ofy().save().entity(user).now() == null) {
            logger.warning("user null");
            RESTUtil.showInternalError(req, resp);
            return;
        }
        resp.setStatus(200);
        RESTUtil.showSuccess(req, resp, "Successfully");
    }

    private void identity(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential) throws ServletException, IOException {
        User user = ofy().load().type(User.class).id(userCredential.getUserEmail()).now();
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        String data = RESTUtil.readStringInput(req.getInputStream());
        JSONObject object;
        try {
            object = new JSONObject(data);
        } catch (JSONException e) {
            logger.warning("JSONException");
            return;
        }
        String strPassport = null;
        String strIdentityCard = null;
        try {
            strPassport = object.getString("passport");
            if (strPassport.length() != 0) user.setPassport_url(strPassport);
        } catch (JSONException e) {
            logger.info("passport null");
        }
        try {
            strIdentityCard = object.getString("identityCard");
            if (strIdentityCard.length() != 0) user.setIdentifyCard_url(strIdentityCard);
        } catch (JSONException e) {
            logger.info("identityCard null");
        }
        if (strPassport == null && strIdentityCard == null) {
            RESTUtil.showMissingParams(req, resp, "Missing information, identity or passport information is missing");
            logger.severe("Missing information, identity or passport information is missing");
            return;
        }
        if (ofy().save().entity(user).now() == null) {
            logger.warning("user null");
            RESTUtil.showInternalError(req, resp);
        }
        resp.setStatus(200);
        RespMsg msg = new RespMsg(200, "Success", "Process request successfully");
        resp.getWriter().print(RESTUtil.gson.toJson(msg));
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }

    private void DeleteAddress(HttpServletRequest req, HttpServletResponse resp, UserCredential userCredential) throws ServletException, IOException {
        User user = ofy().load().type(User.class).id(userCredential.getUserEmail()).now();
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        String data = RESTUtil.readStringInput(req.getInputStream());
        if (data.length() == 0) {
            logger.warning("data null");
            RESTUtil.showEmptyResult(req, resp);
            return;
        }
        JSONObject object = new JSONObject(data);
        String strAddress;
        String strAuthen;
        try {
            strAddress = object.getString("address");
        } catch (JSONException e) {
            logger.warning("address null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Address is invalid");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        try {
            strAuthen = object.getString("authentication");
        } catch (JSONException e) {
            logger.warning("authentication null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "TOTP is invalid");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        } catch (NumberFormatException ex) {
            RESTUtil.showBadRequest(req, resp, "TOTP is invalid");
            return;
        }

        if (!TwoFactorUtils.isValid(user.getSecret_key(), Integer.parseInt(strAuthen))) {
            logger.warning("totp is invalid");
            RESTUtil.showForbiddenError(req, resp, "TOTP is invalid");
            return;
        }

        List<Wallet> listWallet = ofy().load().type(Wallet.class).filter("userId", user.getEmail()).list();
        List<Long> list = new ArrayList<>();
        for (Wallet wallet : listWallet) {
            list.add(wallet.getId());
        }

        WithdrawalWhitelist address = ofy().load().type(WithdrawalWhitelist.class).filter("address", strAddress).filter("walletId in", list).first().now();
        if (address == null) {
            logger.warning("address null");
            RESTUtil.showEmptyResult(req, resp);
            return;
        }
        ofy().delete().entities(address).now();
        if (ofy().load().type(WithdrawalWhitelist.class).id(address.getId()).now() != null) {
            logger.warning("error delete");
            RESTUtil.showInternalError(req, resp);
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put("delete", address.getAddress());
        resp.setStatus(200);
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().print(RESTUtil.gson.toJson(map));
    }
}

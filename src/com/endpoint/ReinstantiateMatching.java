package com.endpoint;

import com.entities.Order;
import com.util.RESTUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ReinstantiateMatching extends HttpServlet {
    Logger logger = Logger.getLogger(ReinstantiateMatching.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 5) {
            logger.severe("invalid URL");
            RESTUtil.showWrongURLError(req, resp);
            return;
        }
        HashMap<String, Object> map = new HashMap<>();
        List<Order> buyList = ofy().load().type(Order.class).filter("type", "buy").filter("status", 2).list();
        List<Order> sellList = ofy().load().type(Order.class).filter("type", "sell").filter("status", 2).list();
        map.put("sell", sellList);
        map.put("buy", buyList);
        logger.info("HASMAP: "+RESTUtil.gson.toJson(map));

        resp.getWriter().println(RESTUtil.gson.toJson(map));

    }
}

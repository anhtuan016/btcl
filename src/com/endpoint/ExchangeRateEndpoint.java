package com.endpoint;

import com.entities.ExchangeRateUSD;
import com.util.RESTUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ExchangeRateEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(ExchangeRateUSD.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String param = req.getParameter("type");
        if (param == null) {
            getAll(req, resp);
            return;
        }
        if (param != null) {
            getSingle(req, resp, param);
            return;
        }
    }

    private void getSingle(HttpServletRequest req, HttpServletResponse resp, String param) throws IOException {
        ExchangeRateUSD rate = ofy().load().type(ExchangeRateUSD.class).filter("name", param).first().now();
        if (rate == null) {
            logger.severe("Cannot find rate of this coin: " + param);
            RESTUtil.showEmptyResult(req, resp, "Coin not found");
            return;
        }
        resp.setStatus(200);
        resp.getWriter().println(RESTUtil.gson.toJson(rate));
        return;

    }

    private void getAll(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int totalItem = ofy().load().type(ExchangeRateUSD.class).count();
        List<ExchangeRateUSD> list = ofy().load().type(ExchangeRateUSD.class).list();
        logger.info("LIST: "+RESTUtil.gson.toJson(list));
        HashMap<String, Object> data = new HashMap<>();
        data.put("totalItem", totalItem);
        data.put("data", list);
        resp.setStatus(200);
        resp.getWriter().println(RESTUtil.gson.toJson(data));

    }
}

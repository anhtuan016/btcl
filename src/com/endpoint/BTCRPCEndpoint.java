package com.endpoint;

import com.entities.*;
import com.util.ConvertCurrency;
import com.util.Generate;
import com.util.RESTUtil;
import com.util.RespMsg;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class BTCRPCEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(BTCRPCEndpoint.class.getSimpleName());
    public String HOST_URL = "https://1-dot-digitalwalletservice.appspot.com";
    public String BTC_account_ID = "6451085a-f549-42dc-90f7-ba8d783bc4a8";
    public String CREATE_ADDRESS_URI = "/v2/btc/accounts/" + BTC_account_ID + "/addresses";
    public String TRANSFER_BTC_URI = "/v2/btc/transfer/" + BTC_account_ID + "/transaction";

    public String FEE_CALCULATOR_URL = "http://35.231.225.42:8080/btcrpc/raw";
    public String API_KEY = "d0179161-6cec-4691-aaa1-181ca13143c3";
    public String API_SECRET_KEY = "4b133b7e-9dda-4d7c-a54a-f6f6c1770f670.8091180048930605";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        User user = (User) req.getAttribute("user");
        logger.info(RESTUtil.gson.toJson(user));
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", "BTC").filter("userId", credential.getUserEmail()).first().now();
        if (wallet == null) {
            logger.warning("wallet null");
            RESTUtil.showMissingParams(req, resp, "Can not find wallet!");
            return;
        }
        if ("withdrawal".equals(uriSplit[uriSplit.length - 2]))
            withdrawalBTC(req, resp, wallet);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 6) {
            RESTUtil.showMissingParams(req, resp);
            return;
        }
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        logger.info(RESTUtil.gson.toJson(credential));
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", "BTC").filter("userId", credential.getUserEmail()).first().now();
        if (wallet == null) {
            logger.warning("wallet null!");
            RESTUtil.showMissingParams(req, resp, "Can not find wallet");
            return;
        }
        String key = uriSplit[uriSplit.length - 1];
        switch (key) {
//            case "withdrawal":
//                withdrawalBTC(req, resp, wallet);
//                break;
            case "deposit":
                depositBTC(req, resp, wallet);
                break;
            default:
                RESTUtil.showMissingParams(req, resp);
        }
    }

    private void depositBTC(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws IOException {
        HashMap<String, String> headers = new HashMap<>();
        String timeStamp = String.valueOf(Generate.currentTime() / 1000);
        String method = "POST";

        String rawSignature = timeStamp + method + CREATE_ADDRESS_URI;
        logger.info("Raw Signature Before Coverting: " + rawSignature);
        String HMACSignature = RESTUtil.toHMAC256(rawSignature, API_SECRET_KEY);
        logger.info("HMAC: " + HMACSignature);
        headers.put("CB-ACCESS-SIGN", HMACSignature);
        headers.put("CB-ACCESS-KEY", API_KEY);
        headers.put("CB-ACCESS-TIMESTAMP", timeStamp);

        Connection.Response rsp = Jsoup.connect(HOST_URL + CREATE_ADDRESS_URI)
                .headers(headers)
                .ignoreContentType(true)
                .method(Connection.Method.POST)
                .ignoreHttpErrors(true)
                .execute();
        logger.info("response: " + rsp);
        if (rsp.statusCode() != 201) {
            logger.info("response create address vps null!");
            RESTUtil.showInternalError(req, resp, "Response create address from VPS null! Please try again!");
            return;
        }
        String content = rsp.body();
        logger.info("content: " + content);
        JSONObject obj = new JSONObject(content);
        String btcAddr = obj.getString("address");
        Address address = new Address(btcAddr, AddressType.BTC.toInt(), wallet.getId());
        if (ofy().save().entity(address).now() == null) {
            logger.severe("Cannot save address to database");
            RESTUtil.showInternalError(req, resp, "Server error! Please try again!");
            return;
        }
        resp.getWriter().println(RESTUtil.gson.toJson(address));
    }

    private void withdrawalBTC(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws IOException {

        String content = RESTUtil.readStringInput(req.getInputStream());

//        long min_fee = 50000;
        logger.info("Content: " + content);
        JSONObject obj = new JSONObject(content);
        String receiverAddress = obj.getString("receiverAddress");
        String amountStr = obj.getString("amount");
        double totalSentAmount;
        double oldBal;
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("account", "testnet");
        hashMap.put("receiverAddress", receiverAddress);
        hashMap.put("amount", amountStr);

        String min_fee = Jsoup.connect(FEE_CALCULATOR_URL)
                .ignoreContentType(true)
                .method(Connection.Method.POST)
                .ignoreHttpErrors(true)
                .requestBody(RESTUtil.gson.toJson(hashMap))
                .execute()
                .body();
        if (min_fee == null) {
            logger.warning("Min_fee null");
            RESTUtil.showInternalError(req, resp, "Fee null! Try again!");
            return;
        }
        logger.info("fee: " + min_fee);
        //        CHECK THE RECEIVING ADDRESS.
        if (receiverAddress == null || amountStr == null) {
            logger.warning("Missing receive data");
            RESTUtil.showForbiddenError(req, resp, "Missing receive data");
            return;
        }
        try {
            oldBal = wallet.getAmount();
            totalSentAmount = ConvertCurrency.satoshiToBTC(Long.parseLong(min_fee)) + Double.parseDouble(amountStr);
        } catch (NumberFormatException e) {
            logger.severe("NumberformatException, Check database and content received!");
            RESTUtil.showForbiddenError(req, resp, "Number format is not well-form");
            return;
        }
        if (oldBal < totalSentAmount) {
            logger.warning("Wallet of user does not have enough money (minimum fee calculated)");
            RESTUtil.showForbiddenError(req, resp, "Your Wallet does not have BTC to make withdrawal (fee included)!");
            return;
        }

//        IN CASE OF ENOUGH FOR MINIMUM FEE LEVEL, PREPARE A PROCESSING REQUEST.
        WithdrawalTx tx = new WithdrawalTx(AddressType.BTC.toInt(), amountStr, Generate.getTimeUTC(), receiverAddress, "processing", "processing");
        tx.setWalletId(wallet.getId());
        if (ofy().save().entity(tx).now() == null) {
            logger.severe("SAVING TRANSACTION FAIL");
            RESTUtil.showForbiddenError(req, resp, "Server error, please try to withdrawal again! ");
            return;
        }
        HashMap<String, String> headers = new HashMap<>();
        String timeStamp = String.valueOf(Generate.currentTime() / 1000);
        String method = "POST";
        logger.info("Content of receiver: " + content);
        String rawSignature = timeStamp + method + TRANSFER_BTC_URI + content;
        logger.info("Raw Signature Before Coverting: " + rawSignature);
        String HMACSignature = RESTUtil.toHMAC256(rawSignature, API_SECRET_KEY);
        logger.info("HMAC: " + HMACSignature);
        headers.put("CB-ACCESS-SIGN", HMACSignature);
        headers.put("CB-ACCESS-KEY", API_KEY);
        headers.put("CB-ACCESS-TIMESTAMP", timeStamp);
        Connection.Response rsp = Jsoup.connect(HOST_URL + TRANSFER_BTC_URI)
                .headers(headers).timeout(50 * 1000)
                .ignoreContentType(true)
                .method(Connection.Method.POST)
                .ignoreHttpErrors(true)
                .requestBody(content)
                .execute();
        logger.info("response from GAE creating eth address: " + rsp.statusMessage());
        logger.info("Link: " + HOST_URL + TRANSFER_BTC_URI);
        if (rsp.statusCode() != 200) {
            resp.setStatus(rsp.statusCode());
            resp.getWriter().println(rsp.statusMessage());
            return;
        }
        String rspContent = rsp.body();
        logger.info("rsp content: " + rspContent);
        JSONObject jsonObj = new JSONObject(rspContent);
        WithdrawalTx withdrawalTx = new WithdrawalTx();
        withdrawalTx.setAmount(jsonObj.getJSONObject("amount").getString("amount"));
        withdrawalTx.setCreated_at(jsonObj.getString("created_at"));
        withdrawalTx.setHash(jsonObj.getJSONObject("network").getString("hash"));
        withdrawalTx.setId(jsonObj.getString("id"));
        withdrawalTx.setStatus("pending");
        withdrawalTx.setType(AddressType.BTC.toInt());
        withdrawalTx.setReceiving_address(jsonObj.getJSONObject("to").getString("address"));
        if (withdrawalTx.getHash() == null) {
            logger.warning("Hash is null, tx does not exist!");
            RESTUtil.showInternalError(req, resp, "Withdrawal fail, contact admin for support!");
            return;
        }
        if (ofy().save().entity(wallet).now() == null) {
            logger.severe("Cannot update new Wallet to database!");
            return;
        }
        if (ofy().save().entity(withdrawalTx).now() == null)
            logger.warning("fail to save withdrawalTx Id of Tx is : " + withdrawalTx.getId());

        //update balance wallet
        double newBalance = wallet.getAmount() - totalSentAmount;
        wallet.setAmount(newBalance);
        wallet.estimateWallet();
        if (ofy().save().entity(wallet).now() == null) {
            logger.severe("Cannot update new Wallet to database");
            RESTUtil.showInternalError(req, resp,"Update wallet fail. Please try again!");
        }
        resp.setStatus(200);
        resp.getWriter().println(RESTUtil.gson.toJson(withdrawalTx));
    }
}

package com.endpoint;

import com.entities.Order;
import com.entities.User;
import com.entities.UserActivity;
import com.entities.UserCredential;
import com.util.*;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class SupportEndpoint extends HttpServlet{
    Logger logger = Logger.getLogger(UserEndpoint.class.getSimpleName());
    private static final String regexEmail = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";


    static String url = "https://bitcheck-208302.firebaseapp.com/resetPassword/";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] uriSplit = req.getRequestURI().split("/");
        String strCase = uriSplit[uriSplit.length - 1];
        if (strCase.equalsIgnoreCase("forgotpassword")) {
            forgotPassword(req, resp);
            return;
        }
        if (strCase.equalsIgnoreCase("resetpassword")) {
            resetPass(req, resp);
            return;
        }
        if (strCase.equalsIgnoreCase("activate")) {
            twoFactorActivate(req, resp);
            return;
        }
        RESTUtil.showWrongURLError(req,resp);
    }


    private void twoFactorActivate(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("activate enable two-factor");
        String data = RESTUtil.readStringInput(req.getInputStream());
        if (data.length() == 0) {
            logger.warning("data null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Enter address");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        JSONObject object = new JSONObject(data);
        String strAuthen;
        try {
            strAuthen = object.getString("authentication");
        }catch (JSONException e){
            logger.warning("authentication null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Enter two-factor authentication code");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (req.getParameter("id") == null) {
            logger.warning("null actiCode");
            RESTUtil.showMissingParams(req, resp, "id null");
            return;
        }
        if (req.getParameter("id").length() != 20) {
            logger.warning("id wrong:" + req.getParameter("id"));
            RESTUtil.showMissingParams(req, resp, "ID not found");
            return;
        }
        User user = ofy().load().type(User.class).filter("activeCode", req.getParameter("id")).first().now();
        if (user == null) {
            logger.warning("user null");
            RESTUtil.showMissingParams(req, resp, "ID is incorrect");
            return;
        }
        if (!TwoFactorUtils.isValid(user.getSecret_key(), Integer.parseInt(strAuthen))) {
            logger.warning("totp is invalid");
            RESTUtil.showForbiddenError(req, resp, "TOTP is invalid");
            return;
        }
        if (user.getTwofactorauth()!=2){
            HashMap<String, String> mapresp = new HashMap<>();
            mapresp.put("Pending", "Two Factor authenticator not pending!");
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(mapresp));
            return;
        }
        user.setTwofactorauth(1);
        user.setActiveCode(Generate.strCode());
        if (ofy().save().entity(user).now() == null) {
            RESTUtil.showInternalError(req, resp, "server error: not save");
            return;
        }
        UserActivity userActivity = new UserActivity(req,user.getEmail());
        userActivity.setActivity("ENABLE_2FA");
        if (ofy().save().entity(userActivity).now() == null) {
            RESTUtil.showInternalError(req, resp, "Server error: not save");
            return;
        }
        HashMap<String, String> mapresp = new HashMap<>();
        mapresp.put("enable", "Two Factor authenticator is enable successfully!");
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(mapresp));
        return;

    }
    private void resetPass(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("id")==null){
            logger.warning("id null");
            RESTUtil.showMissingParams(req, resp);
            return;
        }
        String strID = req.getParameter("id");
        logger.info("Activate code:"+strID);
        String data = RESTUtil.readStringInput(req.getInputStream());
        if (data.length()==0){
            logger.warning("data email null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Enter header name: email");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }

        User user = ofy().load().type(User.class).filter("activeCode",strID).first().now();
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (!strID.equals(user.getActiveCode())){
            logger.warning("id not found");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Please check your mail!");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        JSONObject object = new JSONObject(data);
        String strNewpass;
        try {
            strNewpass = object.getString("newPassword");
        }catch (JSONException e ){
            RESTUtil.showJsonError(req, resp,"Password is empty or wrong!");

            return;
        }
        user.setPassword(strNewpass);
        user.setActiveCode(Generate.strCode());
        if (user.validateRegister().size() != 0) {
            resp.setStatus(400);
            RESTUtil.showInternalError(req, resp, "Password has to from 8-16 characters. There are at least 1 uppercase and 1 special character!");
            logger.warning("server error:" + RESTUtil.gson.toJson(user.validateRegister()));
            return;
        }
        if (ofy().save().entity(user).now() == null) {
            logger.warning("user null");
            RESTUtil.showInternalError(req, resp,"Server error. Please try again!");
            return;
        }
        RESTUtil.showSuccess(req,resp,"Reset password successfully!");
    }



    private void forgotPassword(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = RESTUtil.readStringInput(req.getInputStream());
        if (data.length()==0){
            logger.warning("data email null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Enter header name: email");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        JSONObject object = new JSONObject(data);
        String strEmail;
        try{
            strEmail = object.getString("email");
        }catch (JSONException e){
            logger.warning("email null");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Email is empty, please try again");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (!regexEmail(strEmail)){
            logger.warning("regex email");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Email is incorrect, please try again");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        User user = ofy().load().type(User.class).id(object.getString("email")).now();
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        try {
            SendMail.Activation(user.getEmail(), url+user.getActiveCode(),"Bittrex password");
            logger.info("send mail url:" + url+user.getActiveCode());
        } catch (Exception ex) {
            logger.warning("send mail error");
            return;
        }
        RESTUtil.showSuccess(req,resp,"Please check your mail!");
    }
    private boolean regexEmail(String email) {
        Pattern pattern = Pattern.compile(regexEmail);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}


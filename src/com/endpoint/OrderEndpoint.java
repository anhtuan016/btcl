package com.endpoint;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.datatransfer.FlavorEvent;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.*;
import java.util.logging.Logger;

import com.entities.*;
import com.google.gson.JsonSyntaxException;
import com.pusher.rest.Pusher;
import com.pusher.rest.data.Result;
import com.sun.corba.se.spi.orb.ORBData;
import com.util.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class OrderEndpoint extends HttpServlet {
    static Logger logger = Logger.getLogger(OrderEndpoint.class.getSimpleName());
    String matching_URL = "http://54.39.22.4:8080/matching/match";
    String rollback_URL = "https://1-dot-bittrexcln.appspot.com/api/v1/rollback?id=";
    static String rollback_key = "9f4ed17aeac77e7cfdf6";
    static Pusher pusher;
    Result result;

    static {
        if (pusher == null) {
            logger.info("Initiate new Pusher");
            pusher = new Pusher("504021", "f4c14cf0e0b456a14b95", "2fb04d90967e47761f96");
            pusher.setCluster("mt1");
            logger.info("new pusher: " + pusher.toString());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        String uriSplit[] = req.getRequestURI().split("/");
        logger.info("URI: " + req.getRequestURI());
        if ("pricelevel".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            getpricelevel(req, resp);
            return;
        }
        if ("history".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            getOrderHistory(req, resp, credential);
        }
        if ("getopen".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            getOpenOrder(req, resp, credential);
        }
        resp.setStatus(200);
        return;

    }

    private void getOpenOrder(HttpServletRequest req, HttpServletResponse resp, UserCredential credential) throws IOException {
        String pair = req.getParameter("pair");
        String userId = credential.getUserEmail();
        int page = 1;
        int limit = 10;
        String pageStr = req.getParameter("page");
        String limitStr = req.getParameter("limit");
        if (pair == null) {
            RESTUtil.showMissingParams(req, resp);
            return;
        }
        try {
            page = Integer.parseInt(pageStr);
            limit = Integer.parseInt(limitStr);
        } catch (NumberFormatException e) {
            page = 1;
            limit = 10;
        }
        int totalItem = ofy().load().type(Order.class).filter("userId", userId).filter("pair", pair.toUpperCase()).filter("status", 2).count();
        List<Order> openOrderList = ofy().load().type(Order.class).filter("userId", userId).filter("pair", pair.toUpperCase()).filter("status", 2).orderKey(true).offset((page - 1) * limit).limit(limit).list();
        logger.info("OpenOrder list is : " + RESTUtil.gson.toJson(openOrderList));
//        if (openOrderList.isEmpty()) {
//            RESTUtil.showEmptyResult(req, resp);
//            return;
//        }
        HashMap<String, Object> data = new HashMap<>();
        data.put("totalItem", totalItem);
        data.put("data", openOrderList);
        logger.info("Data: " + RESTUtil.gson.toJson(data));
        resp.getWriter().println(RESTUtil.gson.toJson(data));
        resp.setStatus(200);
        return;
    }

    private void getOrderHistory(HttpServletRequest req, HttpServletResponse resp, UserCredential credential) throws IOException {
        String pair = req.getParameter("pair");
        String userId = credential.getUserEmail();
        int page = 1;
        int limit = 10;
        String pageStr = req.getParameter("page");
        String limitStr = req.getParameter("limit");
        if (pair == null) {
            RESTUtil.showMissingParams(req, resp);
            return;
        }
        try {
            page = Integer.parseInt(pageStr);
            limit = Integer.parseInt(limitStr);
        } catch (NumberFormatException e) {
            page = 1;
            limit = 10;
        }
        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(1);
        listStatus.add(0);
        int totalItem = ofy().load().type(Order.class).filter("userId", userId).filter("pair", pair.toUpperCase()).filter("status <", 2).count();
        List<Order> orderHistory = ofy().load().type(Order.class).filter("userId", userId).filter("pair", pair.toUpperCase()).filter("status in", listStatus).order("-updated_at").offset((page - 1) * limit).limit(limit).list();
        logger.info("orderHistory is : " + RESTUtil.gson.toJson(orderHistory));

//        if (orderHistory.isEmpty()) {
//            RESTUtil.showEmptyResult(req, resp);
//            return;
//        }
//        SORT 1 MORE TIME.
//        Generate.MyComparator_Order_UpdatedTime comparator = new Generate.MyComparator_Order_UpdatedTime();
//        Collections.sort(orderHistory, comparator);
//        logger.info("orderHistory after sort 2nd times is : " + RESTUtil.gson.toJson(orderHistory));
        HashMap<String, Object> data = new HashMap<>();
        data.put("totalItem", totalItem);
        data.put("data", orderHistory);
        resp.getWriter().println(RESTUtil.gson.toJson(data));
        resp.setStatus(200);
        return;
    }

    private void getpricelevel(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int page = 1;
        int limit = 10;
        String pair = req.getParameter("pair");
        String type = req.getParameter("type");
        String pageStr = req.getParameter("page");
        String limitStr = req.getParameter("limit");
        if (pair == null || type == null) {
            RESTUtil.showWrongURLError(req, resp);
            return;
        }
        if (pageStr == null || limitStr == null) {
            page = 1;
            limit = 10;
        } else {
            try {
                page = Integer.parseInt(pageStr);
                limit = Integer.parseInt(limitStr);
            } catch (NumberFormatException e) {
                page = 1;
                limit = 10;
            }
        }
        if (page * limit > 500) {
            page = 50;
            limit = 10;
        }

//        SORT DESCENDING FOR BUY ORDER.

        if ("buy".equalsIgnoreCase(type)) {
            int totalItem;
            totalItem = ofy().load().type(PriceLevelBid.class).filter("pair", pair).count();
            List<PriceLevelBid> list = ofy().load().type(PriceLevelBid.class).filter("pair", pair).order("-price").limit(limit * page).list();
            HashMap<String, Object> map = new HashMap<>();
            map.put("totalItem", totalItem);
            map.put("data", RESTUtil.gson.toJson(list));
            resp.setStatus(200);
            resp.getWriter().println(RESTUtil.gson.toJson(map));
            return;
        }
//        SORT ASCENDING FOR SELL ORDER.
        if ("sell".equalsIgnoreCase(type)) {
            int totalItem;
            totalItem = ofy().load().type(PriceLevelAsk.class).filter("pair", pair).count();
            List<PriceLevelAsk> listAsk = ofy().load().type(PriceLevelAsk.class).filter("pair", pair).filter("price !=", null).limit(limit * page).list();
            HashMap<String, Object> map = new HashMap<>();
            map.put("totalItem", totalItem);
            map.put("data", RESTUtil.gson.toJson(listAsk));
            resp.setStatus(200);
            resp.getWriter().println(RESTUtil.gson.toJson(map));
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        logger.info("URI: " + req.getRequestURI());
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        if (uriSplit.length != 5) {
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "URL is invalid");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        if ("buy".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            logger.info("Get in createBuyOrder!");
            createBuyOrder(req, resp, credential);
            return;
        }
        if ("sell".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            createSellOrder(req, resp, credential);
            return;
        }
        if ("cancelall".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            cancelAll(req, resp, credential);
        }
    }

    private void cancelAll(HttpServletRequest req, HttpServletResponse resp, UserCredential credential) throws IOException {
        JSONObject obj = new JSONObject(RESTUtil.readStringInput(req.getInputStream()));
        try {
            long orderId = obj.getLong("orderId");
            String pair = obj.getString("pair");
            Order order = ofy().load().type(Order.class).id(orderId).now();
            if (order == null) {
                RESTUtil.showEmptyResult(req, resp);
                return;
            }
            if (order.getStatus() == 0 || order.getStatus() == 1) {
                RESTUtil.showEmptyResult(req, resp);
                return;
            }
            if (!order.getUserId().equalsIgnoreCase(credential.getUserEmail())) {
                logger.severe("Missing tokenKey");
                RESTUtil.showForbiddenError(req, resp, "Missing Token Key");
                return;
            }
            HashMap<String, Object> data = new HashMap<>();
            data.put("orderId", orderId);
            data.put("currencyPair", pair);
            String rsp = Jsoup.connect(matching_URL + "?command=cancelall").timeout(60 * 1000).ignoreContentType(true).method(Connection.Method.POST).requestBody(RESTUtil.gson.toJson(data)).execute().body();
            logger.info("cancelAll rsp: " + rsp);
            resp.getWriter().println(RESTUtil.gson.toJson(order));
        } catch (JSONException e) {
            logger.warning("Json syntax");
            RESTUtil.showJsonError(req, resp);
            return;
        } catch (IOException e) {
            logger.warning("Error on connect VPS: " + e.getMessage());
            RESTUtil.showInternalError(req, resp);
            return;
        }
    }

    private void createSellOrder(HttpServletRequest req, HttpServletResponse resp, UserCredential credential) throws IOException {
        try {
            String content = RESTUtil.readStringInput(req.getInputStream());
            Order order = RESTUtil.gson.fromJson(content, Order.class);
            if (order == null) {
                logger.warning("Order place parse : NULL");
                resp.setStatus(403);
                RespMsg msg = new RespMsg(403, "Forbidden", "Invalid order format");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            if (!"sell".equalsIgnoreCase(order.getType())) {
                resp.setStatus(403);
                RespMsg msg = new RespMsg(403, "Forbidden", "Invalid order type");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            String pair[] = order.getPair().split("-");
            //CHECK BALANCE OF THE SECOND CURRENCY INSIDE THE PAI DUE TO SELL ORDER.
            Wallet wallet = ofy().load().type(Wallet.class).filter("type", pair[1].trim()).filter("userId", credential.getUserEmail()).first().now();
            if (wallet == null) {
                logger.warning("CANNOT FIND WALLET");
                resp.setStatus(500);
                RespMsg msg = new RespMsg(500, "Internal Server Error", "User wallet cannot be found");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            double currentBal = wallet.getAmount();
            double askAmount = order.getAmount();
            double fee = BigDecimal.valueOf(order.getPrice()).multiply(BigDecimal.valueOf(order.getAmount())).multiply(BigDecimal.valueOf(0.0025)).doubleValue();
            logger.info("ask amount: " + askAmount);
            if (currentBal < askAmount) {
                logger.warning("Not enough fund to make sell order");
                resp.setStatus(403);
                RespMsg msg = new RespMsg(403, "Forbidden", "User not have enough " + pair[1] + " to place order");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            order.setUserId(credential.getUserEmail());
            order.setFee(fee);
            order.setBaseFee(fee);
            order.setValid("processing");
            logger.info("order: " + RESTUtil.gson.toJson(order));
            if (order.validate().size() != 0) {
                logger.warning("server error: validate order");
                return;
            }
            if (ofy().save().entity(order).now() == null) {
                logger.severe("CANNOT SAVE ORDER TO DATASTORE!");
                resp.setStatus(500);
                RespMsg msg = new RespMsg(500, "Internal Server Error", "ERROR WHEN CREATING ORDER, PLEASE TRY AGAIN");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            wallet.setAmount(RESTUtil.minusDouble(wallet.getAmount(), askAmount));
            wallet.estimateWallet();

            if (ofy().save().entity(wallet).now() == null) {
                logger.warning("UPDATE WALLET FAIL WHEN MAKE SELL ORDER! TRYING DELETE ORDER");
                ofy().delete().entity(order).now();
                RESTUtil.showInternalError(req, resp, "Some Errors occured! Please try again later!");
                return;
            }
//            PUSHER CHANGE WALLET BALANCE.
            result = pusher.trigger("walletbalance-" + wallet.getUserId(), wallet.getType(), Collections.singletonMap("data", RESTUtil.gson.toJson(wallet)));
            logger.info("result + Change wallet Balance: " + result.getMessage() + " status: " + result.getStatus());

//            CONNECT TO MATCHING ENGINE.
            HashMap<String, Object> orderTobeMatched = new HashMap<>();
            Order loadedOrder = ofy().load().type(Order.class).id(order.getId()).now();
            if (loadedOrder == null) {
                RESTUtil.showInternalError(req, resp, "Internal Error when creating Order!");
                return;
            }
            long long_price = BigDecimal.valueOf(loadedOrder.getPrice()).multiply(BigDecimal.valueOf(1000_000_000_000L)).longValue();
            long long_amount = BigDecimal.valueOf(loadedOrder.getAmount()).multiply(BigDecimal.valueOf(1000_000_000_000L)).longValue();
            logger.info("long_amount: " + long_amount);
            orderTobeMatched.put("orderId", loadedOrder.getId());
            orderTobeMatched.put("currencyPair", loadedOrder.getPair());
            orderTobeMatched.put("price", long_price);
            orderTobeMatched.put("quantity", long_amount);
            final String postingContent = RESTUtil.gson.toJson(orderTobeMatched);
            logger.info("posting content: " + postingContent);
            logger.info("Starting to executor");
            ExecutorService executors = Executors.newCachedThreadPool();

            executors.execute(new Runnable() {
                @Override
                public void run() {
                    try {
//                        throw new IOException();
                        Jsoup.connect(matching_URL + "?command=sell").timeout(30*1000).ignoreContentType(true).method(Connection.Method.POST).requestBody(postingContent).execute();
                    } catch (Exception a) {
                        //REQUEST TO CANCELLING ORDER ENDPOINT.
                        try {
                            Jsoup.connect(rollback_URL + order.getId() + "&content=" + a.getMessage()).method(Connection.Method.POST).ignoreContentType(true).header("key", rollback_key).execute();
                        } catch (IOException e) {
                        }
                    }
                }
            });
            executors.shutdown();
            logger.info("END");
            resp.setStatus(201);
            resp.getWriter().println(RESTUtil.gson.toJson(order));
            return;
        } catch (JsonSyntaxException e) {
            logger.warning("Syntax Json: " + e.fillInStackTrace().getLocalizedMessage() + "==== " + RESTUtil.gson.toJson(e.getStackTrace()));
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "Invalid order format");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        } catch (NumberFormatException e) {
            logger.warning("NumberFormat: " + e.fillInStackTrace().getLocalizedMessage() + "==== " + RESTUtil.gson.toJson(e.getStackTrace()));
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "Invalid number format");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
    }

    private void createBuyOrder(HttpServletRequest req, HttpServletResponse resp, UserCredential credential) throws IOException {
        try {
            String content = RESTUtil.readStringInput(req.getInputStream());
            Order order = RESTUtil.gson.fromJson(content, Order.class);
            if (order == null) {
                logger.warning("Order place parse : NULL");
                resp.setStatus(403);
                RespMsg msg = new RespMsg(403, "Forbidden", "Invalid order format");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            logger.info("order: " + RESTUtil.gson.toJson(order));
            if (!"buy".equalsIgnoreCase(order.getType())) {
                resp.setStatus(403);
                RespMsg msg = new RespMsg(403, "Forbidden", "Invalid order type");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            //cap pair order
            String pair[] = order.getPair().split("-");
//            CHECK BALANCE OF FIRST CURRENCY INSIDE PAIR DUE TO THE BUY ORDER


            double totalAmount = BigDecimal.valueOf(order.getAmount()).multiply(BigDecimal.valueOf(order.getPrice())).doubleValue();
            double fee = BigDecimal.valueOf(totalAmount).multiply(BigDecimal.valueOf(0.0025)).doubleValue();
            double bidAmount = RESTUtil.addDouble(totalAmount, fee);
//            GET THE WALLET.
            Wallet wallet = ofy().load().type(Wallet.class).filter("type", pair[0].trim()).filter("userId", credential.getUserEmail()).first().now();
            if (wallet == null) {
                logger.warning("CANNOT FIND WALLET");
                resp.setStatus(500);
                RespMsg msg = new RespMsg(500, "Internal Server Error", "User wallet cannot be found");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            double currentBal = wallet.getAmount();
            if (currentBal < bidAmount) {
                logger.warning("NOT ENOUGH FUND TO PLACE ORDER");
                resp.setStatus(403);
                RespMsg msg = new RespMsg(403, "Forbidden", "User wallet does not have enough fund to place bid order");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            order.setUserId(credential.getUserEmail());
            order.setFee(fee);
            order.setBaseFee(fee);
            order.setValid("processing");
            logger.info("Saving order to database!");
            if (order.validate().size() != 0) {
                logger.warning("server error:" + RESTUtil.gson.toJson(order.validate()));
                return;
            }
//            SAVE ORDER.
            if (ofy().save().entity(order).now() == null) {
                logger.warning("CANNOT SAVE ORDER: " + order.getId());
                resp.setStatus(500);
                RespMsg msg = new RespMsg(500, "Error Encountered", "Cannot place order now, please try again.");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
            Order orderTest = ofy().load().type(Order.class).id(order.getId()).now();
            logger.info("basefee: " + orderTest.getBaseFee());
            logger.info("order saved: " + RESTUtil.gson.toJson(order));

//            SAVE WALLET.
            wallet.setAmount(RESTUtil.minusDouble(wallet.getAmount(), bidAmount));
            wallet.estimateWallet();

            logger.info("WALLET-AMOUNT: " + (wallet.getAmount()));
            if (ofy().save().entity(wallet).now() == null) {
                logger.severe("CANNOT UPDATE THE WALLET: " + RESTUtil.gson.toJson(wallet) + " TRYING TO DELETE ORDER");
                ofy().delete().entity(order).now();
                resp.setStatus(500);
                RespMsg msg = new RespMsg(500, "Error Occured", "Some errors occured, please try again later!");
                resp.getWriter().println(RESTUtil.gson.toJson(msg));
                return;
            }
//            CONNECT TO MATCHING ENGINE.
            HashMap<String, Object> orderTobeMatched = new HashMap<>();
            Order loadedOrder = ofy().load().type(Order.class).id(order.getId()).now();
            if (loadedOrder == null) {
                RESTUtil.showInternalError(req, resp, "Internal Error when creating Order!");
                return;
            }
            long long_price = BigDecimal.valueOf(loadedOrder.getPrice()).multiply(BigDecimal.valueOf(1000_000_000_000L)).longValue();
            long long_amount = BigDecimal.valueOf(loadedOrder.getAmount()).multiply(BigDecimal.valueOf(1000_000_000_000L)).longValue();
            logger.info("long_amount: " + long_amount);
            orderTobeMatched.put("orderId", loadedOrder.getId());
            orderTobeMatched.put("currencyPair", loadedOrder.getPair());
            orderTobeMatched.put("price", long_price);
            orderTobeMatched.put("quantity", long_amount);
            String postingContent = RESTUtil.gson.toJson(orderTobeMatched);
            logger.info("posting content: " + postingContent);

            ExecutorService executors = Executors.newCachedThreadPool();
            executors.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Jsoup.connect(matching_URL + "?command=buy").timeout(30*1000).ignoreContentType(true).method(Connection.Method.POST).requestBody(postingContent).execute();
                    } catch (IOException a) {
                        logger.severe("CANNOT POST TO MATCHING VPS: " + RESTUtil.gson.toJson(orderTobeMatched) + " Order Deleted " + RESTUtil.gson.toJson(order));
//                  ROLLBACK ORDER API.
                        try {
                            Jsoup.connect(rollback_URL + order.getId()).ignoreContentType(true).method(Connection.Method.POST).header("key", rollback_key).execute();
                        } catch (IOException c) {
                        }
                    }
                }
            });
            executors.shutdown();


//            PUSHER CHANGE WALLET BALANCE, ORDER OPENED
            result = pusher.trigger("walletbalance-" + wallet.getUserId(), wallet.getType(), Collections.singletonMap("data", RESTUtil.gson.toJson(wallet)));
            logger.info("result + Change wallet Balance: " + result.getMessage() + " status: " + result.getStatus());
            logger.info("Update wallet to database successfully: " + RESTUtil.gson.toJson(wallet));
            result = pusher.trigger("openedOrder", order.getUserId(), Collections.singletonMap("message", "new data updated"));
            logger.info("result Opened Order: " + result.getMessage() + " status: " + result.getStatus());
            resp.setStatus(201);
            resp.getWriter().println(RESTUtil.gson.toJson(order));
            return;
        } catch (JsonSyntaxException e) {
            logger.warning("Syntax Json: " + e.fillInStackTrace().getLocalizedMessage() + "==== " + RESTUtil.gson.toJson(e.getStackTrace()));
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "Invalid order format");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        } catch (NumberFormatException e) {
            logger.warning("NumberFormat exception: " + e.fillInStackTrace().getLocalizedMessage());
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "Invalid number format");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
    }
}

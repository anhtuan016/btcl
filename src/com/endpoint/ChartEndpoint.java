package com.endpoint;

import com.entities.CandleStickItem30m;
import com.entities.Transaction;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.cmd.Query;
import com.pusher.rest.Pusher;
import com.pusher.rest.data.Result;
import com.util.Generate;
import com.util.RESTUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ChartEndpoint extends HttpServlet {
    static Logger logger = Logger.getLogger(ChartEndpoint.class.getSimpleName());
    long interval = 30 * 60 * 1000;
    static Pusher pusher= null;
    static {

        if (pusher == null) {
            logger.info("Initiate new Pusher");
            pusher = new Pusher("504021", "f4c14cf0e0b456a14b95", "2fb04d90967e47761f96");
            pusher.setCluster("mt1");
            logger.info("new pusher: " + pusher.toString());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pair = req.getParameter("pair");
        if (pair == null) {
            RESTUtil.showMissingParams(req, resp, "Parameter of pair is missing");
            return;
        }
        List<CandleStickItem30m> list = ofy().load().type(CandleStickItem30m.class).filter("pair", pair).list();

        if (list.isEmpty()) {
            HashMap<String, Object> chartBeacon = getBeacon(interval, pair);
            long startingPoint = (long) chartBeacon.get("start");
            long stoppingPoint = (long) chartBeacon.get("stop");
            if (startingPoint == stoppingPoint) {
                logger.warning("Empty Chart Data (Due to no transaction)");
                resp.getWriter().println(list);
                return;
            }
            if (generateData(interval, startingPoint, stoppingPoint, pair) == null) {
                resp.getWriter().println("Init failed");
                logger.severe("Initialize Chart data failed!");
                return;
            }
            list = ofy().load().type(CandleStickItem30m.class).filter("pair", pair).list();
        }
        resp.getWriter().println(RESTUtil.gsonExpose.toJson(list));
    }


    public static List<CandleStickItem30m> generateData(long interval, long startingPoint, long stoppingPoint, String pair) {
        List<CandleStickItem30m> list = new ArrayList<>();
        for (long i = startingPoint; i < stoppingPoint; i += interval) {
            long end_interval = i + interval;
            String dateLabel = Generate.formatfromLong(i, "HH:mm");
            if ("00:00".equalsIgnoreCase(dateLabel)) dateLabel = Generate.formatfromLong(i, "MM-dd");
            CandleStickItem30m item = new CandleStickItem30m(i, dateLabel, pair);
            logger.info("end_interval: " + end_interval);

            Query<Transaction> query = ofy().load().type(Transaction.class).filter("pair", pair).filter("timelong >=", i).filter("timelong <=", end_interval);
            int count = query.count();
            logger.info("query count : " + count);
            if (count == 0) {
                logger.warning("Query empty");
                if (ofy().save().entity(item).now() == null) {
                    logger.warning("save chart item failed: " + item.getDate());
                }
                logger.info("SAVE ITEM : " + RESTUtil.gson.toJson(item));
                list.add(item);
                continue;
            }

//            UNORDERED-LIST IS SORT BY TIMELONG DESC.
            List<Transaction> unorderedTxList = query.chunkAll().list();
            logger.info("list before: " + RESTUtil.gson.toJson(unorderedTxList));
            item.setClose(String.valueOf(unorderedTxList.get(0).getPrice()));
            item.setOpen(String.valueOf(unorderedTxList.get(unorderedTxList.size() - 1).getPrice()));

//            SORT THE UNORDERED-LIST BY PRICE.
            Generate.MyComparator comparator = new Generate.MyComparator();
            Collections.sort(unorderedTxList, comparator);
            logger.info("list after: " + RESTUtil.gson.toJson(unorderedTxList));
            item.setHigh(String.valueOf(unorderedTxList.get(0).getPrice()));
            item.setLow(String.valueOf(unorderedTxList.get(unorderedTxList.size() - 1).getPrice()));

            if (ofy().save().entity(item).now() == null) {
                logger.warning("Save chart item failed: " + item.getDate());
            }
            logger.info("SAVE ITEM : " + RESTUtil.gson.toJson(item));
            list.add(item);
        }
        return list;
    }

    public static HashMap<String, Object> getBeacon(long interval, String pair) {
        HashMap<String, Object> chartBeacon = new HashMap<>();
        int count = ofy().load().type(Transaction.class).filter("pair", pair).count();
        if (ofy().load().type(Transaction.class).filter("pair", pair).filter("timelong >", 0).count() == 0) {
            long Point = System.currentTimeMillis();
            chartBeacon.put("start", Point);
            chartBeacon.put("stop", Point);
            return chartBeacon;
        }

        long lastTransactionTime = ofy().load().type(Transaction.class).filter("pair", pair).filter("timelong >", 0).first().now().getTimelong();
        logger.warning("last Transaction time: " + lastTransactionTime);
        long beginningTransactionTime = ofy().load().type(Transaction.class).filter("pair", pair).filter("timelong >", 0).offset(count - 1).first().now().getTimelong();
        logger.warning("Beginning Transaction time: " + beginningTransactionTime);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(beginningTransactionTime);
        long startingPoint = Generate.getBeginningOfDay(calendar.getTime());
//        calendar.setTimeInMillis(lastTransactionTime+interval);
        long stoppingPoint = lastTransactionTime;
        chartBeacon.put("start", startingPoint);
        chartBeacon.put("stop", stoppingPoint);
        logger.info("starting point: " + startingPoint);
        logger.info("stopping point: " + stoppingPoint);
        logger.info("interval: " + interval);
        return chartBeacon;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String body = RESTUtil.readStringInput(req.getInputStream());
        Transaction transaction = RESTUtil.gson.fromJson(body,Transaction.class);
        String pairStr = transaction.getPair();
        CandleStickItem30m item = ofy().load().type(CandleStickItem30m.class).filter("pair", pairStr).order("-timelong").first().now();
        if (item == null) {
            HashMap<String, Object> chartBeacon = ChartEndpoint.getBeacon(interval, pairStr);
            long startingPoint = (long) chartBeacon.get("start");
            long stoppingPoint = (long) chartBeacon.get("stop");
            logger.info("stoppingPoint in matching:  " + stoppingPoint);
            if (ChartEndpoint.generateData(interval, startingPoint, stoppingPoint, pairStr) == null) {
                logger.severe("Initialize Chart data failed!");
                return;
            }
            item = ofy().load().type(CandleStickItem30m.class).filter("pair", pairStr).order("-timelong").first().now();
            logger.info("Initiate chart successfully!");
        }
        long startingPoint = item.getTimelong();
        long stoppingPoint = transaction.getTimelong();
        List<CandleStickItem30m> data = ChartEndpoint.generateData(interval, startingPoint, stoppingPoint, pairStr);
        if (data == null) {
            logger.severe("fetch new chart data errors");
            return;
        }
        logger.info("List new item of chart: " + RESTUtil.gsonExpose.toJson(data));

        Result resultChart = pusher.trigger("candleStickChart", "30m", Collections.singletonMap("data", RESTUtil.gsonExpose.toJson(data)));
        logger.info("resultChart: " + resultChart.getMessage() + " status: " + resultChart.getStatus());
    }
}

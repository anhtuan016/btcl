package com.endpoint;

import com.entities.*;
import com.googlecode.objectify.Key;
import com.pusher.rest.Pusher;
import com.pusher.rest.data.Result;
import com.util.Generate;
import com.util.RESTUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class MatchingNoti extends HttpServlet {
    Result result;
    static Logger logger_noti = Logger.getLogger(MatchingNoti.class.getSimpleName());
    static Pusher pusher;
    static ExecutorService executor = null;

    //    Result result = pusher.trigger("1", "event", Collections.singletonMap("message", "new message"));
    static {
        executor = Executors.newFixedThreadPool(8);
        if (pusher == null) {
            logger_noti.info("Initiate new Pusher");
            pusher = new Pusher("504021", "f4c14cf0e0b456a14b95", "2fb04d90967e47761f96");
            pusher.setCluster("mt1");
            logger_noti.info("new pusher: " + pusher.toString());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String content = RESTUtil.readStringInput(req.getInputStream());
        String uriSplit[] = req.getRequestURI().split("/");
        String endpoint = uriSplit[uriSplit.length - 1];
        if ("pricelevel".equalsIgnoreCase(endpoint)) {
            updatePriceLevel(req, resp, content);
            return;
        }

        try {
            JSONObject jsonObj = new JSONObject(content);
            String command = jsonObj.getString("command");
            switch (command) {
                case "match":
                    logger_noti.info("enter match");
                    match(req, resp, jsonObj);
                    break;
                case "add":
                    logger_noti.info("enter add");
                    add(req, resp, jsonObj);
                    break;
                case "cancelAll":
                    logger_noti.info("enter cancelall");
                    cancelAll(req, resp, jsonObj);
                    break;
                default:
                    logger_noti.severe("Command not found");
                    return;
            }

        } catch (JSONException e) {

        }
    }


    private void updatePriceLevel(HttpServletRequest req, HttpServletResponse resp, String content) throws IOException {
        boolean flag = true; //false: add new or delete/ true: modify.
        logger_noti.info("PRICELEVEL RESPONSE: " + content);
        JSONObject obj = new JSONObject(content);
        String side = obj.getString("side");
        long price = obj.getLong("price");
        long totalQuantity = obj.getLong("totalQuantity");
        String pair = obj.getString("pair");
        //CONVERT TOTALQUANTITY AND PRICE TO BIGDECIMAL.
        BigDecimal newPriceLevelAmount = BigDecimal.valueOf(totalQuantity).divide(BigDecimal.valueOf(1000_000_000_000D));
        logger_noti.info("newPriceLevelAmount: " + newPriceLevelAmount);
        BigDecimal priceLevelPrice = BigDecimal.valueOf(price).divide(BigDecimal.valueOf(1000_000_000_000D));
        logger_noti.info("priceLevelPrice: " + priceLevelPrice);

        if ("BUY".equals(side)) {
            PriceLevelBid bid = ofy().load().type(PriceLevelBid.class).id(price + pair).now();
            if (bid == null) {
                bid = new PriceLevelBid(price, pair);
                flag = false;
            }
//            CALCULATE MARKETPAIR TOTALCOIN0.
            BigDecimal newPriceLevelCoin0 = newPriceLevelAmount.multiply(priceLevelPrice);
            logger_noti.info("newPriceLevelCoin0: " + newPriceLevelCoin0);
            BigDecimal oldPriceLevelCoin0 = BigDecimal.valueOf(bid.getAmount()).multiply(BigDecimal.valueOf(bid.getPrice()));
            logger_noti.info("oldPriceLevelCoin0: " + oldPriceLevelCoin0);
            BigDecimal changeInCoin0 = newPriceLevelCoin0.subtract(oldPriceLevelCoin0);

//            CALCULATE MARKETPAIR TOTALCOIN1.
            BigDecimal changeInCoin1 = newPriceLevelAmount.subtract(BigDecimal.valueOf(bid.getAmount()));
            logger_noti.info("changeInCoin0: " + changeInCoin0);
            logger_noti.info("change in coin1: " + changeInCoin1);

            bid.setAmount(newPriceLevelAmount.doubleValue());
            bid.setPrice(priceLevelPrice.doubleValue());
            if (ofy().save().entity(bid).now() == null) logger_noti.warning("Save PriceLevel Bid fail!");

            if (bid.getAmount() == 0) {
                ofy().delete().key(Key.create(PriceLevelBid.class, price + pair)).now();
                logger_noti.info("DELETED PRICELEVEL BID: " + price);
                flag = false;
            }
            if (flag) {
                result = pusher.trigger("pricelevelmodify-" + pair, bid.getId() + "-bid", Collections.singletonMap("message", "new data updated"));
                logger_noti.info("result pricelevelmodify- " + pair + " " + result.getMessage() + " status: " + result.getStatus());
            }
            if (!flag) {
                result = pusher.trigger("pricelevel-" + pair, "bidchange", Collections.singletonMap("message", "new data updated"));
                logger_noti.info("result pricelevel- " + pair + " " + result.getMessage() + " status: " + result.getStatus());
            }

        }
        if ("SELL".equals(side)) {
            PriceLevelAsk ask = ofy().load().type(PriceLevelAsk.class).id(price + pair).now();

            if (ask == null) {
                ask = new PriceLevelAsk(price, pair);
                flag = false;
            }
//            CALCULATE MARKETPAIR TOTALCOIN0.
            BigDecimal newPriceLevelCoin0 = newPriceLevelAmount.multiply(priceLevelPrice);
            BigDecimal oldPriceLevelCoin0 = BigDecimal.valueOf(ask.getAmount()).multiply(BigDecimal.valueOf(ask.getPrice()));
            BigDecimal changeInCoin0 = newPriceLevelCoin0.subtract(oldPriceLevelCoin0);

//            CALCULATE MARKETPAIR TOTALCOIN1.
            BigDecimal changeInCoin1 = newPriceLevelAmount.subtract(BigDecimal.valueOf(ask.getAmount()));
            logger_noti.info("changeInCoin0: " + changeInCoin0);
            logger_noti.info("change in coin1: " + changeInCoin1);
            ask.setAmount(newPriceLevelAmount.doubleValue());
            ask.setPrice(priceLevelPrice.doubleValue());

            if (ofy().save().entity(ask).now() == null) logger_noti.warning("Save PriceLevel Ask fail!");
            if (ask.getAmount() == 0) {
                flag = false;
                ofy().delete().key(Key.create(PriceLevelAsk.class, price + pair)).now();
                logger_noti.info("DELETED PRICELEVEL ASK: " + price);
            }
            if (flag) {
                result = pusher.trigger("pricelevelmodify-" + pair, ask.getId() + "-ask", Collections.singletonMap("message", "new data updated"));
                logger_noti.info("result pricelevelmodify: " + result.getMessage() + " status: " + result.getStatus());
            }
            if (!flag) {
                result = pusher.trigger("pricelevel-" + pair, "askchange", Collections.singletonMap("message", "new data updated"));
                logger_noti.info("result pricelevelAsk: " + result.getMessage() + " status: " + result.getStatus());
            }
        }
        /**
         * HANDLE TOTALBID/TOTALASK OF MARKET_PAIR.
         * **/
        double totalAsk_coin0 = obj.getDouble("totalAsk_coin0");
        double totalAsk_coin1 = obj.getDouble("totalAsk_coin1");
        double totalBid_coin0 = obj.getDouble("totalBid_coin0");
        double totalBid_coin1 = obj.getDouble("totalBid_coin1");
        MarketPair marketPair = ofy().load().type(MarketPair.class).id(pair).now();
        marketPair.setCoin0_totalask(totalAsk_coin0);
        marketPair.setCoin1_totalask(totalAsk_coin1);
        marketPair.setCoin0_totalbid(totalBid_coin0);
        marketPair.setCoin1_totalbid(totalBid_coin1);
        if (ofy().save().entity(marketPair).now() == null)
            logger_noti.severe("Update totalBid/totalAsk of Market Pair fail");

        /**PUSHER FOR TOTAL-BID AND TOTAL-ASK*/
        if ("BUY".equals(side)) {
//              TRIGGER PUSHER TOTALBID OF COINS.
            HashMap<String, Object> data = new HashMap<>();
            data.put("coin0", marketPair.getCoin0_totalbid());
            data.put("coin1", marketPair.getCoin1_totalbid());
            result = pusher.trigger(marketPair.getId(), "totalBid", Collections.singletonMap("data", RESTUtil.gson.toJson(data)));
            logger_noti.info("event totalbid: " + result.getMessage() + "  " + result.getStatus());
        }
        if ("SELL".equals(side)) {
//            TRIGGER PUSHER TOTALASK/BID OF COINS.
            HashMap<String, Object> data = new HashMap<>();
            data.put("coin0", marketPair.getCoin0_totalask());
            data.put("coin1", marketPair.getCoin1_totalask());
            result = pusher.trigger(marketPair.getId(), "totalAsk", Collections.singletonMap("data", RESTUtil.gson.toJson(data)));
            logger_noti.info("event totalask: " + result.getMessage() + "  " + result.getStatus());
        }

    }

    private void cancelAll(HttpServletRequest req, HttpServletResponse resp, JSONObject jsonObj) throws IOException {

        long orderId = jsonObj.getLong("orderId");
        Order order = ofy().load().type(Order.class).id(orderId).now();
        Result result = null;
        if (order == null) {
            logger_noti.warning("Order cancelall Not found");
            return;
        }
        String pair[] = order.getPair().split("-");
        order.setStatus(0);
        logger_noti.info("Order to be canceled: " + RESTUtil.gson.toJson(order));
        if (ofy().save().entity(order).now() == null) {
            logger_noti.warning("Update Database fail: " + RESTUtil.gson.toJson(order));
            return;
        }
        result = pusher.trigger("cancelOrder", order.getUserId(), Collections.singletonMap("message", "new data updated"));
        logger_noti.info("result: " + result.getMessage() + " status: " + result.getStatus());
        if ("buy".equalsIgnoreCase(order.getType())) {

//            ADD AMOUNT BACK TO BALANCE OF WALLET.

            Wallet wallet = ofy().load().type(Wallet.class).filter("userId", order.getUserId()).filter("type", pair[0]).first().now();
            wallet.setAmount(RESTUtil.addDouble(wallet.getAmount(), BigDecimal.valueOf(order.getAmount()).multiply(BigDecimal.valueOf(order.getPrice())).doubleValue(), order.getFee()));
            wallet.estimateWallet();
            if (ofy().save().entity(wallet).now() == null) {
                logger_noti.severe("Cannot add back balance to wallet: " + RESTUtil.gson.toJson(wallet));
            }

//            PUSHER CHANGE WALLET BALANCE.
            result = pusher.trigger("walletbalance-" + wallet.getUserId(), wallet.getType(), Collections.singletonMap("data", RESTUtil.gson.toJson(wallet)));
            logger_noti.info("result + Change wallet Balance on cancelAll event: " + result.getMessage() + " status: " + result.getStatus());

        }
        if ("sell".equalsIgnoreCase(order.getType())) {
//            ADD AMOUNT BACK TO BALANCE OF WALLET.
            Wallet wallet = ofy().load().type(Wallet.class).filter("userId", order.getUserId()).filter("type", pair[1]).first().now();
            wallet.setAmount(RESTUtil.addDouble(wallet.getAmount(), order.getAmount()));
            wallet.estimateWallet();
            if (ofy().save().entity(wallet).now() == null) {
                logger_noti.severe("Cannot add back balance to wallet: " + RESTUtil.gson.toJson(wallet));
            }

//            PUSHER CHANGE WALLET BALANCE.
            result = pusher.trigger("walletbalance-" + wallet.getUserId(), wallet.getType(), Collections.singletonMap("data", RESTUtil.gson.toJson(wallet)));
            logger_noti.info("result + Change wallet Balance on cancelAll: " + result.getMessage() + " status: " + result.getStatus());


        }
    }

    private void add(HttpServletRequest req, HttpServletResponse resp, JSONObject jsonObj) throws IOException {
        try {
            long orderId = jsonObj.getLong("orderId");
            String side = jsonObj.getString("side");
            double valueFromMatchEng = jsonObj.getDouble("quantity");
            logger_noti.info("order Id: " + orderId + " quantity: " + valueFromMatchEng + " side: " + side);
            double quantity = BigDecimal.valueOf(valueFromMatchEng).divide(BigDecimal.valueOf(1_000_000_000_000L)).doubleValue();
            logger_noti.info("quantity after divided: " + quantity);
            Order order = ofy().load().type(Order.class).id(orderId).now();
            if (order == null) {
                logger_noti.warning("Cannot find the existence of this order in datastore");
                return;
            }

            order.setAmount(quantity);
            order.setUpdated_at(Generate.currentTime());
            if (order.validate().size() != 0) {
                logger_noti.severe("validate order:" + RESTUtil.gson.toJson(order.validate()));
                RESTUtil.showInternalError(req, resp, "server error, validate order!");
                return;
            }
            if (order.getValid().equals("processing")) order.setValid("created");

            if (ofy().save().entity(order).now() == null) {
                logger_noti.warning("Cannot Update Order: " + RESTUtil.gson.toJson(order));
                return;
            }
            /**TRIGGER EVENT OPENINGORDER*/
            Result result = pusher.trigger("openedOrder", order.getUserId(), Collections.singletonMap("message", "new data updated"));
            logger_noti.info("result: " + result.getMessage() + " status: " + result.getStatus());
            result = pusher.trigger("orderCreated-" + order.getUserId(), "success", Collections.singletonMap("data", RESTUtil.gson.toJson(order)));
            logger_noti.info("result + orderCreated: " + result.getMessage() + " status: " + result.getStatus());

            resp.setStatus(200);
            logger_noti.info("Update done!: " + RESTUtil.gson.toJson(order));
            return;
        } catch (JSONException e) {
            logger_noti.severe("JSon syntax exception!!");
        }

    }


    private void match(HttpServletRequest req, HttpServletResponse resp, JSONObject jsonObj) throws IOException {
        boolean flag_incomingOrder = false;
        boolean flag_waitingOrder = false;
        try {
            long waitingOrderId = jsonObj.getLong("waitingOrderId");
            long incomingOrderId = jsonObj.getLong("incomingOrderId");

            double executedQuantity = BigDecimal.valueOf(jsonObj.getLong("executedQuantity")).divide(BigDecimal.valueOf(1000_000_000_000L)).doubleValue();//số lượng tiền thực thi thực tế.
            double remainingQuantity = BigDecimal.valueOf(jsonObj.getLong("remainingQuantity")).divide(BigDecimal.valueOf(1000_000_000_000L)).doubleValue();//số lượng tiền còn dư cua incoming order.

            logger_noti.info("executedQuantity :" + executedQuantity);
            logger_noti.info("remainingQuantity :" + remainingQuantity);
            String side = jsonObj.getString("incomingSide");

            //waiting order là order đã đứng ở bảng, incoming order là order vừa được đặt
            Order waitingOrder = ofy().load().type(Order.class).filterKey(Key.create(Order.class, waitingOrderId)).filter("status", 2).first().now(); //0-Cancel,1-Complete,2-Waiting.
            Order incomingOrder = ofy().load().type(Order.class).filterKey(Key.create(Order.class, incomingOrderId)).filter("status", 2).first().now();
            if (waitingOrder == null || incomingOrder == null) {
                if (waitingOrder == null) logger_noti.severe("Missing waitingOrder.");
                if (incomingOrder == null) logger_noti.severe("Missing incomingOrder!!.");
                return;
            }

            String incomingUserId = incomingOrder.getUserId();
            String waitingUserId = waitingOrder.getUserId();
            String pairStr = waitingOrder.getPair();
            double matchedPrice = waitingOrder.getPrice();
            double fee = BigDecimal.valueOf(executedQuantity).multiply(BigDecimal.valueOf(matchedPrice)).multiply(BigDecimal.valueOf(0.0025)).doubleValue();//btc
            double executedAmount = BigDecimal.valueOf(executedQuantity).multiply(BigDecimal.valueOf(matchedPrice)).doubleValue();
            String pair[] = pairStr.split("-");
            if ("SELL".equalsIgnoreCase(side)) {
                Wallet incomingWallet = ofy().load().type(Wallet.class).filter("userId", incomingUserId).filter("type", pair[0]).first().now();
                Wallet waitingWallet = ofy().load().type(Wallet.class).filter("userId", waitingUserId).filter("type", pair[1]).first().now();
                /**
                 * INCOMINGORDER IS SELL => INCOMINGWALLET WILL BE ADDED THE SUM OF EXECUTED AMOUNT IN FIRST CURRENCY (EXECUTED AMOUNT * WAITING ORDER'S PRICE).*/
                // caculate fee and update wallet incoming wallet
                incomingWallet.setAmount(RESTUtil.minusDouble(RESTUtil.addDouble(incomingWallet.getAmount(), executedAmount), fee));
//                WAITINGORDER IS BUY => WAITING WALLET WILL BE ADDED THE EXECUTED AMOUNT BY THE SECOND CURRENCY.
                waitingWallet.setAmount(RESTUtil.addDouble(waitingWallet.getAmount(), executedQuantity)); //executedQuantity is ETH.(Khac executedAmount)
                incomingWallet.estimateWallet();
                waitingWallet.estimateWallet();
                if (ofy().save().entity(incomingWallet).now() == null || ofy().save().entity(waitingWallet).now() == null) {
                    logger_noti.severe("Update wallet failed: incomingwallet: " + RESTUtil.gson.toJson(incomingWallet) + " waitingWallet: " + RESTUtil.gson.toJson(waitingWallet));
                }
//                    UPDATE LAI FIELD FEE CUA INCOMING ORDER VA WAITING ORDER.
                incomingOrder.setFee(RESTUtil.minusDouble(incomingOrder.getFee(), fee));
                waitingOrder.setFee(RESTUtil.minusDouble(waitingOrder.getFee(), fee));

                /**
                 * CHECK MATCHEDPRICE VÀ GIÁ INCOMING ORDER, NẾU KHỚP VỚI MỨC GIÁ CAO HƠN- TRỪ THÊM KHOẢN PHI NÀY executedQuantity*(matchedPrice-INCONMING ORDER PRICE)*0.0025 => UPDATE LAI ORDER */
                if (matchedPrice > incomingOrder.getPrice()) {
                    double feeAdded = executedQuantity * (RESTUtil.minusDouble(matchedPrice, incomingOrder.getPrice())) * 0.0025;
                    if (ofy().save().entity(incomingWallet).now() == null) {
                        logger_noti.severe("save wallet incomming fail");
                    } else {
                        logger_noti.info("Save wallet incomming success!");
                    }
//                    UPDATE LAI FIELD FEE CUA INCOMING ORDER VA WAITING ORDER.
                    incomingOrder.setBaseFee(RESTUtil.addDouble(incomingOrder.getBaseFee(), feeAdded));
                    incomingOrder.setFee(RESTUtil.addDouble(incomingOrder.getFee(), feeAdded));
                }
                //.............................................
                logger_noti.info("INCOMING WALLET: " + RESTUtil.gson.toJson(incomingWallet));

//            PUSHER CHANGE WALLET BALANCE.
                result = pusher.trigger("walletbalance-" + incomingWallet.getUserId(), incomingWallet.getType(), Collections.singletonMap("data", RESTUtil.gson.toJson(incomingWallet)));
                logger_noti.info("result + Change wallet Balance on matchEvent: incomingWallet: " + result.getMessage() + " status: " + result.getStatus());
                result = pusher.trigger("walletbalance-" + waitingWallet.getUserId(), waitingWallet.getType(), Collections.singletonMap("data", RESTUtil.gson.toJson(waitingWallet)));
                logger_noti.info("result + Change wallet Balance on matchEvent: waitingWallet: " + result.getMessage() + " status: " + result.getStatus());

                /**
                 * CREATE TRANSACTION FOR MATCHED ORDERS.*/
                saveTransaction(pairStr, side, executedQuantity, matchedPrice, resp);
            }

            if ("BUY".equalsIgnoreCase(side)) {
//                MarketPair marketPair = ofy().load().type(MarketPair.class).id(incomingOrder.getPair()).now();
//                marketPair.setCoin1_totalask(RESTUtil.minusDouble(marketPair.getCoin1_totalask(), executedQuantity));
//                double changeInCoin0_totalask = executedAmount;
//                marketPair.setCoin0_totalask(RESTUtil.minusDouble(marketPair.getCoin0_totalask(), changeInCoin0_totalask));
//                if (ofy().save().entity(marketPair).now() == null)
//                    logger_noti.severe("update marketPair totalAsk failed");
//                HashMap<String, Object> data = new HashMap<>();
//                data.put("coin0", marketPair.getCoin0_totalask());
//                data.put("coin1", marketPair.getCoin1_totalask());
//                result = pusher.trigger(marketPair.getId(), "totalAsk", Collections.singletonMap("data", RESTUtil.gson.toJson(data)));
//                logger_noti.info("event totalAsk: " + result.getMessage() + "  " + result.getStatus());


                Wallet incomingWallet = ofy().load().type(Wallet.class).filter("userId", incomingUserId).filter("type", pair[1]).first().now();
                Wallet waitingWallet = ofy().load().type(Wallet.class).filter("userId", waitingUserId).filter("type", pair[0]).first().now();
                /**
                 * INCOMINGORDER IS BUY => INCOMING WALLET WILL BE ADDED THE EXECUTED AMOUNT IN SECOND CURRENCY.
                 * CHECK THE EXECUTED PRICE,IF BUYER BOUGHT WITH FAVORABLE PRICE (CHEAPER) THEN WE RE-ADD THE SURPLUS AMOUNT FOR THEM*/
//                UPDATE INCOMINGWALLET.
                incomingWallet.setAmount(RESTUtil.addDouble(incomingWallet.getAmount(), executedQuantity));
//                UPDATE WAITING WALLET WITH FEE DEDUCTED.
                waitingWallet.setAmount(RESTUtil.minusDouble(RESTUtil.addDouble(waitingWallet.getAmount(), executedAmount), fee));
//                UPDATE FEE CUA CAC ORDER.
                incomingOrder.setFee(RESTUtil.minusDouble(incomingOrder.getFee(), fee));
                waitingOrder.setFee(RESTUtil.minusDouble(waitingOrder.getFee(), fee));

                if (waitingOrder.getPrice() < incomingOrder.getPrice()) {
                    Wallet incomingWallet2 = ofy().load().type(Wallet.class).filter("userId", incomingUserId).filter("type", pair[0]).first().now();
                    double paybackAmount = executedQuantity * RESTUtil.minusDouble(incomingOrder.getPrice(), matchedPrice) * (1 + 0.0025);
                    double feeSubstract = executedQuantity * RESTUtil.minusDouble(incomingOrder.getPrice(), matchedPrice) * 0.0025;
                    /**INCOMING ORDER BỎ FEE PHẢI TRẢ LẠI CHO BUYER ĐI*/
                    incomingOrder.setFee(RESTUtil.minusDouble(incomingOrder.getFee(), feeSubstract));
                    incomingOrder.setBaseFee(RESTUtil.minusDouble(incomingOrder.getBaseFee(), feeSubstract));
                    logger_noti.info("payBack amount: " + paybackAmount);
                    incomingWallet2.setAmount(RESTUtil.addDouble(incomingWallet2.getAmount(), paybackAmount));

                    incomingWallet.estimateWallet();
                    incomingWallet2.estimateWallet();
                    waitingWallet.estimateWallet();

                    if (ofy().save().entity(incomingWallet2).now() == null) {
                        logger_noti.severe("Update money back to buyer failed");
                    } else {
                        logger_noti.info("Update money payback successfully");
                    }
                }
                if (ofy().save().entity(incomingWallet).now() == null || ofy().save().entity(waitingWallet).now() == null) {
                    logger_noti.severe("Update wallet failed: incomingwallet: " + RESTUtil.gson.toJson(incomingWallet) + " waitingWallet: " + RESTUtil.gson.toJson(waitingWallet));
                }

//            PUSHER CHANGE WALLET BALANCE.
                result = pusher.trigger("walletbalance-" + incomingWallet.getUserId(), incomingWallet.getType(), Collections.singletonMap("data", RESTUtil.gson.toJson(incomingWallet)));
                logger_noti.info("result + Change wallet Balance on matchEvent: incomingWallet: " + result.getMessage() + " status: " + result.getStatus());
                result = pusher.trigger("walletbalance-" + waitingWallet.getUserId(), waitingWallet.getType(), Collections.singletonMap("data", RESTUtil.gson.toJson(waitingWallet)));
                logger_noti.info("result + Change wallet Balance on matchEvent: waitingWallet: " + result.getMessage() + " status: " + result.getStatus());

                saveTransaction(pairStr, side, executedQuantity, matchedPrice, resp);
            }
            logger_noti.info("Incoming: " + RESTUtil.gson.toJson(incomingOrder) + " waitingOrder: " + RESTUtil.gson.toJson(waitingOrder) + " executedQuantity: " + executedQuantity + " remainingQuantity: " + remainingQuantity + " Side: " + side);

//            CALCULATE THE ACTUAL RATE.
            waitingOrder.setActual_rate((RESTUtil.addDouble(waitingOrder.getActual_rate() * waitingOrder.getFilled(), executedQuantity * matchedPrice)) / RESTUtil.addDouble(waitingOrder.getFilled(), executedQuantity));
            incomingOrder.setActual_rate(RESTUtil.addDouble(incomingOrder.getActual_rate() * incomingOrder.getFilled(), executedQuantity * matchedPrice) / RESTUtil.addDouble(incomingOrder.getFilled(), executedQuantity));
//        CALCULATE THE NEW QUANTITY OF ORDERS.
            logger_noti.info("waiting amou8");
            waitingOrder.setFilled(RESTUtil.addDouble(waitingOrder.getFilled(), executedQuantity));
            waitingOrder.setAmount(RESTUtil.minusDouble(waitingOrder.getAmount(), executedQuantity));
            incomingOrder.setFilled(RESTUtil.addDouble(incomingOrder.getFilled(), executedQuantity));
            incomingOrder.setAmount(RESTUtil.minusDouble(incomingOrder.getAmount(), executedQuantity));
            if (waitingOrder.getAmount() == 0) {
                waitingOrder.setStatus(1);
            }
            if (incomingOrder.getAmount() == 0) {
                incomingOrder.setStatus(1);
            }
            waitingOrder.setUpdated_at(Generate.currentTime());
            incomingOrder.setUpdated_at(Generate.currentTime());
            if (waitingOrder.getValid().equals("processing")) {
                waitingOrder.setValid("created");
                flag_waitingOrder = true;
            }
            if (incomingOrder.getValid().equals("processing")) {
                incomingOrder.setValid("created");
                flag_incomingOrder = true;
            }
            logger_noti.info("NEW ORDER: WaitingOrder: " + RESTUtil.gson.toJson(waitingOrder) + " incomingOrder: " + RESTUtil.gson.toJson(incomingOrder));

            if (ofy().save().entity(waitingOrder).now() == null || ofy().save().entity(incomingOrder).now() == null) {
                logger_noti.severe("CANNOT UPDATE NEW WAITING ORDER of: " + waitingOrder.getId() + "  and id: " + incomingOrder.getId());
                return;
            }
            logger_noti.info("UPDATE ORDERS SUCCESSFULLY: WaitingOrder: " + RESTUtil.gson.toJson(waitingOrder) + " incomingOrder: " + RESTUtil.gson.toJson(incomingOrder));

            /**TRIGGER EVENT OPENINGORDER - ORDERCREATED*/
            if (flag_incomingOrder) {
                result = pusher.trigger("orderCreated-" + incomingOrder.getUserId(), "success", Collections.singletonMap("data", RESTUtil.gson.toJson(incomingOrder)));
                logger_noti.info("result + incoming_orderCreated: " + result.getMessage() + " status: " + result.getStatus());
            }
            if (flag_waitingOrder) {
                result = pusher.trigger("orderCreated-" + waitingOrder.getUserId(), "success", Collections.singletonMap("data", RESTUtil.gson.toJson(waitingOrder)));
                logger_noti.info("result + waiting_orderCreated: " + result.getMessage() + " status: " + result.getStatus());
            }

            Result result = pusher.trigger("openedOrder", waitingOrder.getUserId(), Collections.singletonMap("message", "new data updated"));
            logger_noti.info("result openedOrder: " + result.getMessage() + " status: " + result.getStatus());

            result = pusher.trigger("openedOrder", incomingOrder.getUserId(), Collections.singletonMap("message", "new data updated"));
            logger_noti.info("result openedOrder: " + result.getMessage() + " status: " + result.getStatus());
            result = pusher.trigger("orderHistory", waitingOrder.getUserId(), Collections.singletonMap("message", "new data updated"));
            logger_noti.info("result orderHistory: " + result.getMessage() + " status: " + result.getStatus());
            result = pusher.trigger("orderHistory", incomingOrder.getUserId(), Collections.singletonMap("message", "new data updated"));
            logger_noti.info("result orderHistory: " + result.getMessage() + " status: " + result.getStatus());
            resp.setStatus(200);
            return;
        } catch (JSONException e) {
            logger_noti.severe("JSon syntax exception!!");
        }
    }

    /**
     * CREATE TRANSACTION FOR MATCHED ORDERS.
     */
    private void saveTransaction(String pairStr, String side, double executedQuantity, double matchedPrice, HttpServletResponse resp) {
        long interval = 30 * 60 * 1000;

        Transaction transaction = new Transaction(pairStr, side);
        transaction.setQuantity(executedQuantity);
        transaction.setPrice(matchedPrice);
        transaction.setTotal_cost(executedQuantity * matchedPrice);
        if (transaction.validate().size() != 0) {
            logger_noti.severe("validate transaction:" + RESTUtil.gson.toJson(transaction.validate()));
            return;
        }
        if (ofy().save().entity(transaction).now() == null) {
            logger_noti.severe("Save transaction failed: " + RESTUtil.gson.toJson(transaction));
        }
        /**TRIGGER TRANSACTION EVENT.*/
        Result result = pusher.trigger("transaction", "change", Collections.singletonMap("message", "new data updated"));
        logger_noti.info("result: " + result.getMessage() + " status: " + result.getStatus());
        logger_noti.info("save transaction successfully: " + RESTUtil.gson.toJson(transaction));

        /**UPDATE MARKET PAIR AND TRIGGER EVENT FOR CLIENTS.*/
        MarketPair marketPair = ofy().load().type(MarketPair.class).id(pairStr).now();
        if (marketPair == null) {
            logger_noti.severe("MarketPair does not exist! " + pairStr);
            return;
        }
        marketPair.setLast_price(transaction.getPrice());
        if ("BUY".equalsIgnoreCase(side)) marketPair.setBid_price(transaction.getPrice());
        if ("SELL".equalsIgnoreCase(side)) marketPair.setAsk_price(transaction.getPrice());
        marketPair.setVolume(marketPair.getVolume() + transaction.getTotal_cost());
        logger_noti.info("marketPair: " + RESTUtil.gson.toJson(marketPair));


        /**SET HIGH/LOW PRICE IN 24H FOR MARKETPAIR.*/
        long stop_point = Generate.currentTime();
        long start_point = stop_point - 24 * 3600 * 1000;
        List<Transaction> list = ofy().load().type(Transaction.class).filter("timelong >=", start_point).filter("timelong <=", stop_point).chunkAll().list();
        Generate.MyComparator comparator = new Generate.MyComparator();
        Collections.sort(list, comparator);
        logger_noti.info("List transaction in market Pair: " + RESTUtil.gson.toJson(list));
        if (list.size() > 0) {
            Transaction highest = list.get(0);
            Transaction lowest = list.get(list.size() - 1);
            marketPair.setHigh_24h(highest.getPrice());
            marketPair.setLow_24h(lowest.getPrice());
        }
        if (list.size() == 0) {
            marketPair.setHigh_24h(0);
            marketPair.setLow_24h(0);
        }
        /**HANDLE THE CALCULATION PERCENTAGE CHANGE*/
        Date beginning = new Date(stop_point);
        long beginningLongTime = Generate.getBeginningOfDay(beginning);
        Transaction prevDayLastTx = ofy().load().type(Transaction.class).filter("timelong <", beginningLongTime).order("-timelong").first().now();
        if (prevDayLastTx != null) {
            double lastPrice_prevDay = prevDayLastTx.getPrice();
            BigDecimal changes = BigDecimal.valueOf(marketPair.getLast_price()).subtract(BigDecimal.valueOf(lastPrice_prevDay));
            double percentChange = changes.divide(BigDecimal.valueOf(lastPrice_prevDay), 12, RoundingMode.HALF_EVEN).multiply(BigDecimal.valueOf(100)).doubleValue();
            marketPair.setPercentageChange(percentChange);
        }
        logger_noti.info("PrevDayTxList: " + RESTUtil.gson.toJson(prevDayLastTx));
        if (ofy().save().entity(marketPair).now() == null) logger_noti.severe("Cannot update marketPair");
        logger_noti.info("marketPair: " + RESTUtil.gson.toJson(marketPair));
        /**PUSH TO CHANNEL MARKETPAIR, EVENT = PAIR.*/
        result = pusher.trigger("marketPair", pairStr, Collections.singletonMap("data", RESTUtil.gson.toJson(marketPair)));
        logger_noti.info("result: " + result.getMessage() + " status: " + result.getStatus());
        logger_noti.info("save marketPair successfully: " + RESTUtil.gson.toJson(marketPair));

        /**TRIGGER CHART-DATA-30M EVENT.GET BEACON FIRSTLY THEN GENERATE DATA BASE ON THE BEACON - SUSPENDED.*/
        RedirectChart_runnable runnable = new RedirectChart_runnable(transaction);
        logger_noti.info("Before redirect!");
        executor.execute(runnable);
        logger_noti.info("After redirect!");

    }
}

class RedirectChart_runnable implements Runnable {
    Transaction transaction;

    public RedirectChart_runnable(Transaction transaction) {
        this.transaction = transaction;
    }
    @Override
    public void run() {
        try {
            Jsoup.connect("https://1-dot-bittrexcln.appspot.com/api/v1/candlestick/30m")
                    .method(Connection.Method.POST)
                    .ignoreContentType(false)
                    .ignoreHttpErrors(false)
                    .requestBody(RESTUtil.gson.toJson(transaction))
                    .timeout(0)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

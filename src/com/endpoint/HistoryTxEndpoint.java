package com.endpoint;

import com.entities.*;
import com.util.RESTUtil;
import com.util.RespMsg;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;
import static javafx.scene.input.KeyCode.L;

public class HistoryTxEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(HistoryTxEndpoint.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        int page, limit;
        try {
            page = Integer.parseInt(req.getParameter("page"));
            limit = Integer.parseInt(req.getParameter("limit"));
        } catch (Exception e) {
            logger.warning("page limit");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "bad resquest", "integer parse");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (page < 1 || limit < 1) {
            logger.warning("page limit");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad Request", "Page or limit <1");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        if (credential == null) {
            logger.warning("credential null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find credential");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        User user = ofy().load().type(User.class).id(credential.getUserEmail()).now();
        logger.info(RESTUtil.gson.toJson(user));
        if (user == null) {
            logger.warning("user null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find user");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        String strCase = uriSplit[uriSplit.length - 1];

        switch (strCase) {
            case "depositpending":
                deposit(req, resp, user, page, limit, "pending");
                break;
            case "deposithistory":
                deposit(req, resp, user, page, limit, "complete");
                break;
            case "withdrawalpending":
                withdrawal(req, resp, user, page, limit, "pending");
                break;
            case "withdrawalhistory":
                withdrawal(req, resp, user, page, limit, "complete");
                break;
            default:
                resp.setStatus(400);
                RespMsg msg = new RespMsg(404, "Not found", "Can not find request");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
        }

    }

    private void deposit(HttpServletRequest req, HttpServletResponse resp, User user, int page, int limit, String type) throws ServletException, IOException {
        List<Wallet> list = ofy().load().type(Wallet.class).filter("userId", user.getEmail()).list();
        logger.info("List wallet: " + list.size() + " data: " + RESTUtil.gson.toJson(list));
        List<Object> listResp = new ArrayList<>();

        List<Object> listWalletId = new ArrayList<>();
        for (Wallet wallet : list) {
            listWalletId.add(wallet.getId());
        }
        int totalItem = ofy().load().type(DepositTx.class).order("-created_at").filter("status", type).filter("walletId in", listWalletId).count();
        PageLimit data = new PageLimit(page, limit, totalItem);
        if (data.validate().size() != 0) {
            resp.setStatus(400);
            logger.info("validate page limit");
            return;
        }
        if (totalItem!=0){
            List<DepositTx> listTx = ofy().load().type(DepositTx.class).order("-created_at").filter("status", type).filter("walletId in", listWalletId).limit(data.getLimit()).offset(data.getLimit() * (data.getPage()-1)).list();
            logger.warning("List: "+RESTUtil.gson.toJson(listTx));
            for (DepositTx depositTx : listTx) {
                HashMap<String, String> map = new HashMap<>();
                map.put("date", depositTx.getCreated_at());
                map.put("symbol", AddressType.getSymbol(depositTx.getType()));
                map.put("amount", depositTx.getAmount());
                map.put("status", depositTx.getStatus());
                listResp.add(map);
            }
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("page", data.getPage());
        map.put("limit", data.getLimit());
        map.put("totalPage", data.getTotalPage());
        map.put("totalItem", data.getTotalItem());
        map.put("data", listResp);
        logger.info(RESTUtil.gson.toJson(map));
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(map));
    }

    private void withdrawal(HttpServletRequest req, HttpServletResponse resp, User user, int page, int limit, String type) throws ServletException, IOException {
        List<Wallet> list = ofy().load().type(Wallet.class).filter("userId", user.getEmail()).list();
        logger.info("List wallet: " + list.size() + " data: " + RESTUtil.gson.toJson(list));
        List<Object> listPending = new ArrayList<>();
        List<Object> listResp = new ArrayList<>();

        List<Object> listWalletId = new ArrayList<>();
        for (Wallet wallet : list) {
            listWalletId.add(wallet.getId());

        }
        int totalItem = ofy().load().type(WithdrawalTx.class).order("-created_at").filter("status", type).filter("walletId in", listWalletId).count();
        PageLimit data = new PageLimit(page, limit, totalItem);
        if (data.validate().size() != 0) {
            resp.setStatus(400);
            logger.info("validate page limit");
            return;
        }
        if (totalItem!=0){
            List<WithdrawalTx> listTx = ofy().load().type(WithdrawalTx.class).order("-created_at").filter("status", type).filter("walletId in", listWalletId).limit(data.getLimit()).offset(data.getLimit() * (data.getPage()-1)).list();
            for (WithdrawalTx withdrawalTx : listTx) {
                HashMap<String, String> map = new HashMap<>();
                map.put("date", withdrawalTx.getCreated_at());
                map.put("symbol", AddressType.getSymbol(withdrawalTx.getType()));
                map.put("amount", withdrawalTx.getAmount());
                map.put("status", withdrawalTx.getStatus());
                listResp.add(map);
            }
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("page", data.getPage());
        map.put("limit", data.getLimit());
        map.put("totalPage", data.getTotalPage());
        map.put("data", listResp);
        map.put("totalItem", data.getTotalItem());
        logger.info(RESTUtil.gson.toJson(map));
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(map));
    }

}

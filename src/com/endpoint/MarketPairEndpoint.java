package com.endpoint;

import com.entities.MarketPair;
import com.entities.Transaction;
import com.googlecode.objectify.Key;
import com.util.Generate;
import com.util.RESTUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class MarketPairEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(MarketPair.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pair = req.getParameter("pair");
        if (pair != null) getSinglePair(req, resp, pair);
        if (pair == null) getAllPair(req, resp);

    }

    private void getAllPair(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<MarketPair> list = ofy().load().type(MarketPair.class).list();
        logger.info("all pair: " + RESTUtil.gson.toJson(list));
        resp.getWriter().println(RESTUtil.gson.toJson(list));
    }

    private void getSinglePair(HttpServletRequest req, HttpServletResponse resp, String pair) throws IOException {
        MarketPair marketPair = ofy().load().type(MarketPair.class).id(pair).now();

        if (marketPair == null) {
            RESTUtil.showEmptyResult(req, resp, "This pair does not exist!");
            return;
        }

//        long stop_point = Generate.currentTime();
//        long start_point = stop_point - 24 * 3600 * 1000;
//        List<Transaction> list = ofy().load().type(Transaction.class).filter("timelong >=", start_point).filter("timelong <=", stop_point).list();
//        Generate.MyComparator comparator = new Generate.MyComparator();
//        Collections.sort(list, comparator);
//        logger.info("List transaction in market Pair: " + RESTUtil.gson.toJson(list));
//        if (list.size() > 0) {
//            Transaction highest = list.get(0);
//            Transaction lowest = list.get(list.size() - 1);
//            marketPair.setHigh_24h(highest.getPrice());
//            marketPair.setLow_24h(lowest.getPrice());
//        }
//        if (list.size() == 0) {
//            marketPair.setHigh_24h(0);
//            marketPair.setLow_24h(0);
//        }
        logger.info("market pair: " + RESTUtil.gson.toJson(marketPair));
        resp.getWriter().println(RESTUtil.gson.toJson(marketPair));
    }
}

package com.endpoint;

import com.entities.*;
import com.util.*;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ETHEndpoint extends HttpServlet {
    private static Logger logger = Logger.getLogger(ETHEndpoint.class.getSimpleName());
    private static String HOST_URL = "https://1-dot-digitalwalletservice.appspot.com";
    private static String CREATE_ADDRESS_URI = "/v2/eth/accounts/0x746ad0f8082dd4585d532adc6601e4e94971e556/addresses";
    private static String TRANSFER_ETH_URI = "/v2/eth/transfer/0x746ad0f8082dd4585d532adc6601e4e94971e556/transaction";
    private static String API_KEY = "d0179161-6cec-4691-aaa1-181ca13143c3";
    private static String API_SECRET_KEY = "4b133b7e-9dda-4d7c-a54a-f6f6c1770f670.8091180048930605";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] uriSplit = req.getRequestURI().split("/");
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        User user = (User) req.getAttribute("user");
        logger.info(RESTUtil.gson.toJson(user));
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", "ETH").filter("userId", credential.getUserEmail()).first().now();
        if (wallet == null) {
            logger.warning("wallet null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find Wallet");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if ("withdrawal".equalsIgnoreCase(uriSplit[uriSplit.length - 2])) {
            withdrawalEth(req, resp, wallet);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /**
         * LET USER DEPOSIT/WITHDRAWAL MONEY TO BITTREXCLONE. DEPOSIT /deposit----WITHDRAWAL /withdrawalEth
         * **/
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 6) {
            logger.warning("URL's length is invalid");
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "URL is invalid");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        Wallet wallet = ofy().load().type(Wallet.class).filter("type","ETH").filter("userId",credential.getUserEmail()).first().now();
        if (wallet == null) {
            logger.severe("Cannot find wallet");
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "Cannot find Wallet");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        if ("deposit".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            depositEth(req, resp,wallet);
            return;
        }
    }

    private void withdrawalEth(HttpServletRequest req, HttpServletResponse resp,Wallet wallet) throws IOException {
        BigInteger GAS_LIMIT = BigInteger.valueOf(21000);
        BigInteger GAS_PRICE = BigInteger.valueOf(26_000_000_000L);
        BigInteger fee = GAS_LIMIT.multiply(GAS_PRICE);

//
//        String content = RESTUtil.readStringInput(req.getInputStream());
//        logger.info("Content: " + content);
//        JSONObject obj = new JSONObject(content);
//        String receiverAddress = obj.getString("receiverAddress");
//        String amountStr = obj.getString("amount");

        String amountStr = (String) req.getAttribute("amount");
        String receiverAddress = (String) req.getAttribute("receiverAddress");
        HashMap<String, String> mapStr = new HashMap<>();
        mapStr.put("amount", amountStr);
        mapStr.put("receiverAddress", receiverAddress);

        String content = RESTUtil.gson.toJson(mapStr);


//        CHECK THE RECEIVING ADDRESS.
        if (receiverAddress == null || amountStr == null) {
            logger.warning("Missing receiver data");
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "Missing receiver data");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
//        CHECK AMOUNT ETH FORMAT BEFORE SENDING
        BigInteger amount = ConvertCurrency.ETHtoWei(amountStr);
        if (amount == null) {
            logger.warning("Amount is not an appropriate number");
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "Amount is not an appropriate number");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
//        CHECK THE BALANCE IS WHETHER ENOUGH OR NOT(FEE CALCULATED).
        BigInteger totalAmount = amount.add(fee);
        BigInteger currentBal = ConvertCurrency.ETHtoWei(wallet.getAmount());
        if (currentBal==null){
            logger.warning("Cannot convert currentBal from Database");
            resp.setStatus(500);
            RespMsg msg = new RespMsg(500, "Internal Server Error", "Database error. Please contact admin");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        int compareCode = currentBal.compareTo(totalAmount);
        if (compareCode == -1) {
            logger.warning("Balance is not enough for withdrawalEth");
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "Balance is not enough for withdrawalEth");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
//        STARTING TO SEND FROM BITTREXCLN ACCOUNT TO ADDRESS.
//        CREATE HMAC
        HashMap<String, String> headers = new HashMap<>();
        String timeStamp = String.valueOf(Generate.currentTime() / 1000);
        String method = "POST";
        logger.info("Content of receiver: " + content);
        String rawSignature = timeStamp + method + TRANSFER_ETH_URI + content;
        logger.info("Raw Signature Before Coverting: " + rawSignature);
        String HMACSignature = RESTUtil.toHMAC256(rawSignature, API_SECRET_KEY);
        logger.info("HMAC: " + HMACSignature);
        headers.put("CB-ACCESS-SIGN", HMACSignature);
        headers.put("CB-ACCESS-KEY", API_KEY);
        headers.put("CB-ACCESS-TIMESTAMP", timeStamp);

        //OPEN CONNECTION TO COINBASE API TO TRANSFER ETH TO ADDRESS.

        Connection.Response rsp = Jsoup.connect(HOST_URL + TRANSFER_ETH_URI)
                .headers(headers).timeout(50*1000)
                .ignoreContentType(true)
                .method(Connection.Method.POST)
                .ignoreHttpErrors(true)
                .requestBody(content)
                .execute();
        logger.info("response from GAE creating eth address: " + rsp.statusMessage());
        logger.info("Link: "+HOST_URL+TRANSFER_ETH_URI);
        if (rsp.statusCode() != 200) {
            resp.setStatus(rsp.statusCode());
            resp.getWriter().println(rsp.statusMessage());
            return;
        }
//        FORM A WITHDRAWAL TRANSACTION FOR SAVING TO DATABASE AND RETURN TO USER.
        String rspContent = rsp.body();
        logger.info("rsp content: " + rspContent);
        JSONObject jsonObj = new JSONObject(rspContent);
        WithdrawalTx withdrawalTx = new WithdrawalTx();
        withdrawalTx.setAmount(jsonObj.getJSONObject("amount").getString("amount"));
//        withdrawalTx.setCreated_at(jsonObj.getString("created_at"));
        withdrawalTx.setCreated_at(Generate.stringTime());
        withdrawalTx.setHash(jsonObj.getJSONObject("network").getString("hash"));
        withdrawalTx.setId(jsonObj.getString("id"));
        withdrawalTx.setStatus("pending");
        withdrawalTx.setType(AddressType.ETH.toInt());
        withdrawalTx.setReceiving_address(jsonObj.getJSONObject("to").getString("address"));
        if (withdrawalTx.getHash()==null){
            logger.warning("Hash is null, tx does not exist");
            resp.setStatus(500);
            RespMsg msg = new RespMsg(500, "Server Error", "Withdrawal fail, contact admin for support!");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        if (ofy().save().entity(wallet).now()==null){
            logger.severe("Cannot update new Wallet to database");
            return;
        }
        if (withdrawalTx.validate().size()!=0){
            logger.warning(RESTUtil.gson.toJson(withdrawalTx.validate()));
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(withdrawalTx));
            return;
        }
        if (ofy().save().entity(withdrawalTx).now()==null) logger.warning("fail to save withdrawalTx Id of Tx is : "+withdrawalTx.getId());
//        UPDATE BALANCE OF WALLET AFTER WITHDRAWAL.

        double newBalance = wallet.getAmount()-ConvertCurrency.weiToETH(totalAmount);
        wallet.setAmount(newBalance);
        wallet.estimateWallet();
        if (ofy().save().entity(wallet).now()==null){
            logger.severe("Cannot update new Wallet to database");
        }

        resp.setStatus(200);
        resp.getWriter().println(RESTUtil.gson.toJson(withdrawalTx));
    }

    private void depositEth(HttpServletRequest req, HttpServletResponse resp,Wallet wallet) throws IOException {

        //CREATE AN ETH ADDRESS FOR USER TO MAKE DEPOSIT.
        HashMap<String, String> headers = new HashMap<>();
        String timeStamp = String.valueOf(Generate.currentTime() / 1000);
        String method = "POST";

        String rawSignature = timeStamp + method + CREATE_ADDRESS_URI;
        logger.info("Raw Signature Before Coverting: " + rawSignature);
        String HMACSignature = RESTUtil.toHMAC256(rawSignature, API_SECRET_KEY);
        logger.info("HMAC: " + HMACSignature);
        headers.put("CB-ACCESS-SIGN", HMACSignature);
        headers.put("CB-ACCESS-KEY", API_KEY);
        headers.put("CB-ACCESS-TIMESTAMP", timeStamp);

        //OPEN CONNECTION TO COINBASE API TO RETRIEVE ADDRESS.

        Connection.Response rsp = Jsoup.connect(HOST_URL + CREATE_ADDRESS_URI)
                .headers(headers)
                .ignoreContentType(true)
                .method(Connection.Method.POST)
                .ignoreHttpErrors(true)
                .execute();
        logger.info("response from GAE creating eth address: " + rsp);
        if (rsp.statusCode() != 201) {
            resp.setStatus(rsp.statusCode());
            resp.getWriter().println(rsp.statusMessage());
            return;
        }
        String content = rsp.body();
        logger.info("content: " + content);
        JSONObject obj = new JSONObject(content);
        String ethAddr = obj.getString("address");
        Address address = new Address(ethAddr, AddressType.ETH.toInt(), wallet.getId());
        if (ofy().save().entity(address).now() == null) {
            logger.severe("Cannot save address to database");
            resp.setStatus(500);
            RespMsg msg = new RespMsg(500, "Server Error", "Cannot save address to database");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        resp.getWriter().println(RESTUtil.gson.toJson(address));
    }
}

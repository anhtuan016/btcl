package com.endpoint;

import com.entities.*;
import com.util.Generate;
import com.util.RESTUtil;
import com.util.RespMsg;

import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class Notification extends HttpServlet {
    static String AddressXRP = "rwpnQ3c7LWzHD8c4fTXPKZU5NakF3KmnSa";
    Logger logger = Logger.getLogger(Notification.class.getSimpleName());

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 5) {
            logger.severe("URL's length is invalid");
            return;
        }
        if ("eth".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            notificationETH(req, resp);
            return;
        }
        if ("btc".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            notificationBTC(req, resp);
            return;
        }
        if ("xrp".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            notificationXRP(req, resp);
            return;
        }
        if ("bch".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            notificationBCH(req, resp);
            return;
        }
        if ("btcrpc".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            notificationBTCRPC(req, resp);
            return;
        }
        if ("usdt".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            notificationUSDT(req, resp);
            return;
        }

    }

    private void notificationBTC(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String content = RESTUtil.readStringInput(req.getInputStream());
        logger.info("content received: " + content);
        JSONObject obj = new JSONObject(content);
        if (!"wallet:addresses:new-payment".equalsIgnoreCase(obj.getString("type"))) {
            logger.warning("type of notification is not wallet:addresses:new-payment");
            return;
        }
        JSONObject data = obj.getJSONObject("data");
        if (data.getString("address") == null || data.getString("address").length() == 0) {
            logger.warning("notification lack receiving address");
            return;
        }
        if ("pending".equalsIgnoreCase(obj.getString("status"))) {
            logger.info("notification pending received");
            String addressStr = data.getString("address");
            logger.info("addressStr: " + addressStr);
            JSONObject additionalData = obj.getJSONObject("additionalData");
            String amount = additionalData.getJSONObject("amount").getString("amount");
            Address address = ofy().load().type(Address.class).id(addressStr).now();
            if (address == null) {
                logger.warning("CANNOT FOUND ADDRESS");
                resp.setStatus(500);
                return;
            }
            Wallet wallet = ofy().load().type(Wallet.class).id(address.getWalletId()).now();
            if (amount == null || wallet == null) {
                logger.warning("amount or wallet null");
                return;
            }
            logger.info("amount: " + amount);
            logger.info("wallet: " + RESTUtil.gson.toJson(wallet));
            double newBal = wallet.getAmount_pending() + Double.parseDouble(amount);
            wallet.setAmount_pending(newBal);
            if (ofy().save().entity(wallet).now() == null) {
                logger.severe("Updating new  wallet failed");
                return;
            }
            logger.info("wallet after notification: " + RESTUtil.gson.toJson(wallet));
            DepositTx depositTx = new DepositTx();
            depositTx.setId(Generate.randomId());
            depositTx.setAmount(amount);
            depositTx.setType(1);
            depositTx.setCreated_at(Generate.stringTime());
            depositTx.setReceiving_address(obj.getJSONObject("data").getString("address"));
            depositTx.setStatus("complete");
            depositTx.setHash(obj.getJSONObject("additionalData").getString("hash"));
            depositTx.setWalletId(wallet.getId());

            if (depositTx.validate().size()!=0){
                logger.warning("server error:"+RESTUtil.gson.toJson(depositTx.validate()));
                return;
            }
            if (ofy().save().entity(depositTx).now() == null) {
                logger.warning("server error: not save deposit tx");
            }
        }
        if ("complete".equalsIgnoreCase(obj.getString("status"))) {
            String addressStr = data.getString("address");
            logger.info("addressStr: " + addressStr);
            JSONObject additionalData = obj.getJSONObject("additionalData");
            String amount = additionalData.getJSONObject("amount").getString("amount");
            Address address = ofy().load().type(Address.class).id(addressStr).now();
            if (address == null) {
                logger.warning("CANNOT FOUND ADDRESS");
                resp.setStatus(500);
                return;
            }
            Wallet wallet = ofy().load().type(Wallet.class).id(address.getWalletId()).now();
            double newBal;
            double newBalPending;
            if (amount == null || wallet == null) {
                logger.warning("amount or wallet null");
                return;
            }
            logger.info("amount: " + amount);
            logger.info("wallet: " + RESTUtil.gson.toJson(wallet));
            try {
                double currentBal = wallet.getAmount();
                newBal = currentBal + Double.parseDouble(amount);
                double currentBalPending = wallet.getAmount_pending();
                newBalPending = currentBalPending - Double.parseDouble(amount);

            } catch (NumberFormatException e) {
                logger.info("Number format exception when parse double, check database or notification");
                return;
            }
            wallet.setAmount_pending(newBalPending);
            wallet.setAmount(newBal);
            wallet.estimateWallet();

            if (ofy().save().entity(wallet).now() == null) {
                logger.severe("Updating new  wallet failed");
                return;
            }
        }
    }

    private void notificationETH(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String content = RESTUtil.readStringInput(req.getInputStream());
        logger.info("content received: " + content);
        JSONObject obj = new JSONObject(content);
        try {
            if ("wallet:addresses:new-payment".equalsIgnoreCase(obj.getString("type"))) {
                logger.info("Notify for deposit ETH!");
                notifyDepositEth(req, resp, obj);
                return;
            }
            if ("wallet:addresses:send".equalsIgnoreCase(obj.getString("type"))) {
                logger.info("Notify for withdrawal ETH!");
                notifyWithdrawalETH(req, resp, obj);
            }
        } catch (JSONException e) {
            logger.severe("Json Syntax exception");
            return;
        }
    }
    private void notifyWithdrawalETH(HttpServletRequest req, HttpServletResponse resp, JSONObject obj) {
        JSONObject additionalData = obj.getJSONObject("additionalData");
        String hash = additionalData.getString("hash");
        WithdrawalTx tx = ofy().load().type(WithdrawalTx.class).filter("hash", hash).first().now();
        if (tx == null) {
            logger.warning("Withdrawal tx cannot be found!");
            return;
        }
        tx.setStatus("complete");
        if (tx.validate().size()!=0){
            logger.warning(RESTUtil.gson.toJson(tx.validate()));
            return;
        }
        if (ofy().save().entity(tx).now() == null) {
            logger.severe("Update withdrawaTx status failed: txId " + tx.getId());
            return;
        }
        logger.info("update withdrawalTx completed");

    }

    private void notifyDepositEth(HttpServletRequest req, HttpServletResponse resp, JSONObject obj) throws JSONException {
        JSONObject data = obj.getJSONObject("data");
        if (data.getString("address") == null || data.getString("address").length() == 0) {
            logger.warning("notification lack received address");
            return;
        }
        if ("pending".equalsIgnoreCase(obj.getString("status"))) {
            logger.info("notification pending received");
            String addressStr = data.getString("address");
            if ("0x746ad0f8082dd4585d532adc6601e4e94971e556".equalsIgnoreCase(addressStr)) {
                logger.info("noti vi tong");
                return;
            }
            logger.info("addressStr: " + addressStr);
            JSONObject additionalData = obj.getJSONObject("additionalData");
            String amount = additionalData.getJSONObject("amount").getString("amount");
            Address address = ofy().load().type(Address.class).id(addressStr).now();
            if (address == null) {
                logger.warning("CANNOT FOUND ADDRESS");
                resp.setStatus(500);
                return;
            }
            Wallet wallet = ofy().load().type(Wallet.class).id(address.getWalletId()).now();
            if (wallet == null) {
                logger.severe("wallet is null");
                return;
            }
            if (amount == null) {
                logger.severe("amount is null");
                return;
            }
            logger.info("wallet is : " + RESTUtil.gson.toJson(wallet) + " amount is " + amount);
            double newBal = wallet.getAmount_pending() + Double.parseDouble(amount);
            wallet.setAmount_pending(newBal);

            if (ofy().save().entity(wallet).now() == null) {
                logger.severe("Updating new  wallet failed");
                return;
            }
            DepositTx depositTx = new DepositTx();
            depositTx.setId(Generate.randomId());
            depositTx.setAmount(amount);
            depositTx.setType(1);
            depositTx.setCreated_at(Generate.stringTime());
            depositTx.setReceiving_address(data.getJSONObject("data").getString("address"));
            depositTx.setStatus("complete");
            depositTx.setHash(data.getJSONObject("additionalData").getString("hash"));
            depositTx.setWalletId(wallet.getId());
            if (depositTx.validate().size()!=0){
                logger.warning("server error:"+RESTUtil.gson.toJson(depositTx.validate()));
                return;
            }
            if (ofy().save().entity(depositTx).now() == null) {
                logger.warning("server error: not save deposit tx");
            }
        }
        if ("complete".equalsIgnoreCase(obj.getString("status"))) {
            String addressStr = data.getString("address");
            if ("0x746ad0f8082dd4585d532adc6601e4e94971e556".equalsIgnoreCase(addressStr)) {
                logger.info("noti vi tong");
                return;
            }
            logger.info("addressStr: " + addressStr);
            JSONObject additionalData = obj.getJSONObject("additionalData");
            String amount = additionalData.getJSONObject("amount").getString("amount");
            Address address = ofy().load().type(Address.class).id(addressStr).now();
            if (address == null) {
                logger.warning("CANNOT FOUND ADDRESS");
                resp.setStatus(500);
                return;
            }
            Wallet wallet = ofy().load().type(Wallet.class).id(address.getWalletId()).now();
            double newBal;
            double newBalPending;

            try {
                double currentBal = wallet.getAmount();
                newBal = currentBal + Double.parseDouble(amount);
                double currentBalPending = wallet.getAmount();
                newBalPending = currentBalPending - Double.parseDouble(amount);

            } catch (NumberFormatException e) {
                logger.info("Number format exception when parse double, check database or notification");
                return;
            }
            wallet.setAmount_pending(newBalPending);
            wallet.setAmount(newBal);
            wallet.estimateWallet();

            if (ofy().save().entity(wallet).now() == null) {
                logger.severe("Updating new  wallet failed");
                return;
            }
            DepositTx depositTx = ofy().load().type(DepositTx.class).filter("hash", data.getJSONObject("additionalData").getString("hash")).first().now();
            if (depositTx == null) {
                logger.severe("DepositTx null");
                return;
            }
            depositTx.setStatus("complete");
            if (depositTx.validate().size()!=0){
                logger.warning("server error:"+RESTUtil.gson.toJson(depositTx.validate()));
                return;
            }
            if (ofy().save().entity(depositTx).now() == null) {
                logger.severe("Updating new  depositTx failed");
                return;
            }
        }
    }

    private void notificationXRP(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String content = RESTUtil.readStringInput(req.getInputStream());
        logger.info("String content: " + content);
        if (content.length() == 0) {
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad resquest", "resquest body null");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        JSONObject notification = new JSONObject(content);
        if ("wallet:addresses:send".equalsIgnoreCase(notification.getString("type"))) {
            depositXRP(req, resp, notification);
        }
        if ("wallet:addresses:new-payment".equalsIgnoreCase(notification.getString("type"))) {
            withdrawalXRP(req, resp, notification);
        }
        resp.setStatus(200);
        logger.info("notification xrp");
    }

    private void withdrawalXRP(HttpServletRequest req, HttpServletResponse resp, JSONObject notification) throws ServletException, IOException {
        WithdrawalTx withdrawalTx = ofy().load().type(WithdrawalTx.class).filter("hash", notification.getJSONObject("additionalData").getString("hash")).first().now();
        if (withdrawalTx==null){
            logger.warning("withdrawalTx null");
            return;
        }
        withdrawalTx.setStatus("complete");
        if (ofy().save().entity(withdrawalTx).now() == null) {
            logger.warning("Save withdrawalTx eror");
            return;
        }
    }

    private void depositXRP(HttpServletRequest req, HttpServletResponse resp,JSONObject notification) throws ServletException, IOException {
        if (!AddressXRP.equalsIgnoreCase(notification.getJSONObject("data").getString("address"))) {
            logger.warning("Destination not data");
            return;
        }
        if (notification.getJSONObject("additional_data").getString("destinationTag") == null) {
            logger.warning("tag null");
            return;
        }
        DestinationTag destinationTag = ofy().load().type(DestinationTag.class).id(notification.getJSONObject("additional_data").getString("destinationTag")).now();
        if (destinationTag == null) {
            logger.info("Tag: " + notification.getString("status") + " not found data");
            resp.setStatus(200);
            return;
        }
        Wallet wallet = ofy().load().type(Wallet.class).id(destinationTag.getWalletId()).now();
        if (wallet == null) {
            logger.warning("wallet null");
            return;
        }
        String strAmount = notification.getJSONObject("additional_data").getJSONObject("amount").getString("amount");
        Double walletAmount = wallet.getAmount();
        Double dbAmount = Double.parseDouble(strAmount) / 1000000;
        wallet.setAmount(walletAmount + dbAmount);
        wallet.estimateWallet();
        if (ofy().save().entity(wallet).now() == null) {
            logger.warning("server error: not save wallet");
        }
        DepositTx depositTx = new DepositTx();
        depositTx.setId(Generate.randomId());
        depositTx.setAmount(strAmount);
        depositTx.setType(3);
        depositTx.setCreated_at(Generate.stringTime());
        depositTx.setReceiving_address(notification.getJSONObject("data").getString("address"));
        depositTx.setStatus("complete");
        depositTx.setHash(notification.getJSONObject("additional_data").getString("hash"));
        depositTx.setWalletId(wallet.getId());
        if (depositTx.validate().size()!=0){
            logger.warning("server error:"+RESTUtil.gson.toJson(depositTx.validate()));
            return;
        }
        if (ofy().save().entity(depositTx).now() == null) {
            logger.warning("server error: not save deposit tx");
        }
    }

    private void notificationBCH(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String content = RESTUtil.readStringInput(req.getInputStream());
        logger.info("String content: " + content);
        if (content.length() == 0) {
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad resquest", "resquest body null");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        JSONObject JsonNotification = new JSONObject(content);
        if ("wallet:addresses:new-payment".equalsIgnoreCase(JsonNotification.getString("type"))) {
            logger.info("wallet:addresses:new-payment");
            Address address = ofy().load().type(Address.class).id(JsonNotification.getJSONObject("data").getString("address")).now();
            if (address == null) {
                logger.warning("address not found");
                return;
            }
            Wallet wallet = ofy().load().type(Wallet.class).id(address.getWalletId()).now();
            if (wallet == null) {
                logger.warning("wallet not found");
                return;
            }
            logger.info("wallet: " + RESTUtil.gson.toJson(wallet));
            if ("pending".equalsIgnoreCase(JsonNotification.getString("status"))) {
                logger.info("wallet:addresses:new-payment-pending");
                if (wallet.getId() != address.getWalletId()) {
                    logger.warning("wallet notifi");
                    return;
                }
                if (JsonNotification.getJSONObject("additionalData").getJSONObject("amount").getString("amount") == null) {
                    logger.warning("amount null");
                    return;
                }
                double newBalPending = Double.parseDouble(JsonNotification.getJSONObject("additionalData").getJSONObject("amount").getString("amount")) / 10000000 + wallet.getAmount_pending();
                wallet.setAmount_pending(newBalPending);
                if (ofy().save().entity(wallet).now() == null) {
                    logger.warning("save wallet");
                    return;
                }
                DepositTx depositTx = new DepositTx();
                depositTx.setId(Generate.randomId());
                depositTx.setAmount(JsonNotification.getJSONObject("additionalData").getJSONObject("amount").getString("amount"));
                depositTx.setType(4);
                depositTx.setCreated_at(Generate.stringTime());
                depositTx.setReceiving_address(JsonNotification.getJSONObject("data").getString("address"));
                depositTx.setStatus("pending");
                depositTx.setHash(JsonNotification.getJSONObject("additionalData").getString("hash"));
                depositTx.setWalletId(wallet.getId());
                logger.info(RESTUtil.gson.toJson(depositTx));


                if (ofy().save().entity(depositTx).now() == null) {
                    logger.warning("server error: not save deposit tx");
                }

            }
            if ("complete".equalsIgnoreCase(JsonNotification.getString("status"))) {
                logger.info("wallet:addresses:new-payment-complete");
                if (wallet.getId() != address.getWalletId()) {
                    logger.warning("wallet notifi");
                    return;
                }
                if (JsonNotification.getJSONObject("additionalData").getJSONObject("amount").getString("amount") == null) {
                    logger.warning("amount null");
                    return;
                }
                double newBalCom = Double.parseDouble(JsonNotification.getJSONObject("additionalData").getJSONObject("amount").getString("amount")) / 10000000 + wallet.getAmount();
                double newBalPen = wallet.getAmount_pending() - Double.parseDouble(JsonNotification.getJSONObject("additionalData").getJSONObject("amount").getString("amount")) / 10000000;
                wallet.setAmount_pending(newBalPen);
                wallet.setAmount(newBalCom);
                wallet.estimateWallet();


                if (ofy().save().entity(wallet).now() == null) {
                    logger.warning("save wallet");
                    return;
                }
                DepositTx depositTx = ofy().load().type(DepositTx.class).filter("hash", JsonNotification.getJSONObject("additionalData").getString("hash")).first().now();
                if (depositTx == null) {
                    logger.warning("depositTx null");
                    return;
                }
                depositTx.setStatus("complete");
                depositTx.setCreated_at(Generate.stringTime());
                if (ofy().save().entity(depositTx).now() == null) {
                    logger.warning("save wallet");
                    return;
                }
            }
        }

        if ("wallet:addresses:send".equalsIgnoreCase(JsonNotification.getString("type"))) {
            logger.info("wallet:addresses:send");
            Address address = ofy().load().type(Address.class).id(JsonNotification.getJSONObject("data").getString("address")).now();
            if (address == null) {
                logger.warning("address not found");
                return;
            }
            Wallet wallet = ofy().load().type(Wallet.class).id(address.getWalletId()).now();
            logger.info("wallet: " + RESTUtil.gson.toJson(wallet));
            if ("pending".equalsIgnoreCase(JsonNotification.getString("status"))) {
                logger.info("wallet:addresses:send-pending");
//                WithdrawalTx withdrawalTx = ofy().load().type(WithdrawalTx.class).filter("hash", JsonNotification.getJSONObject("additionalData").getString("hash")).first().now();
//                withdrawalTx.setStatus("pending");
//                if (ofy().save().entity(withdrawalTx).now() == null) {
//                    logger.warning("Save withdrawalTx eror");
//                    return;
//                }
            }

            if ("complete".equalsIgnoreCase(JsonNotification.getString("status"))) {
                logger.info("wallet:addresses:send-complete");
                if (wallet.getId() != address.getWalletId()) {
                    logger.warning("wallet notifi");
                    return;
                }
                if (JsonNotification.getJSONObject("additionalData").getJSONObject("amount").getString("amount") == null) {
                    logger.warning("amount null");
                    return;
                }
                WithdrawalTx withdrawalTx = ofy().load().type(WithdrawalTx.class).filter("hash", JsonNotification.getJSONObject("additionalData").getString("hash")).first().now();
                withdrawalTx.setStatus("complete");
                if (ofy().save().entity(withdrawalTx).now() == null) {
                    logger.warning("Save withdrawalTx eror");
                    return;
                }
            }


        }

    }

    private void notificationBTCRPC(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String content = RESTUtil.readStringInput(req.getInputStream());
        logger.info("content received: " + content);
        JSONObject obj = new JSONObject(content);
        try {
            if ("wallet:addresses:new-payment".equalsIgnoreCase(obj.getString("type"))) {
                logger.info("Notify for deposit btc!");
                notifyDepositBtc(req, resp, obj);
                return;
            }
            if ("wallet:addresses:send".equalsIgnoreCase(obj.getString("type"))) {
                logger.info("Notify for withdrawal btc!");
                notifyWithdrawalBtc(req, resp, obj);
            }
        } catch (JSONException e) {
            logger.severe("Json Syntax exception");
            return;
        }
    }

    private void notifyDepositBtc(HttpServletRequest req, HttpServletResponse resp, JSONObject obj) {
        JSONObject data = obj.getJSONObject("data");
        if (data.getString("address") == null || data.getString("address").length() == 0) {
            logger.warning("notification lack received address");
            return;
        }
        if ("pending".equalsIgnoreCase(obj.getString("status"))) {
            logger.info("notification pending received");
            String addressStr = data.getString("address");
            logger.info("addressStr: " + addressStr);
            JSONObject additionalData = obj.getJSONObject("additionalData");
            String amount = additionalData.getJSONObject("amount").getString("amount");
            Address address = ofy().load().type(Address.class).id(addressStr).now();
            if (address == null) {
                logger.warning("CANNOT FOUND ADDRESS");
                resp.setStatus(500);
                return;
            }
            Wallet wallet = ofy().load().type(Wallet.class).id(address.getWalletId()).now();
            if (wallet == null) {
                logger.severe("wallet is null");
                return;
            }
            if (amount == null) {
                logger.severe("amount is null");
                return;
            }
            logger.info("wallet is : " + RESTUtil.gson.toJson(wallet) + " amount is " + amount);
            double newBal = wallet.getAmount_pending() + Double.parseDouble(amount);
            wallet.setAmount_pending(newBal);

            if (ofy().save().entity(wallet).now() == null) {
                logger.severe("Updating new  wallet failed");
                return;
            }
        }
        if ("complete".equalsIgnoreCase(obj.getString("status"))) {
            String addressStr = data.getString("address");

            logger.info("addressStr: " + addressStr);
            JSONObject additionalData = obj.getJSONObject("additionalData");
            String amount = additionalData.getJSONObject("amount").getString("amount");
            Address address = ofy().load().type(Address.class).id(addressStr).now();
            if (address == null) {
                logger.warning("CANNOT FOUND ADDRESS");
                resp.setStatus(500);
                return;
            }
            Wallet wallet = ofy().load().type(Wallet.class).id(address.getWalletId()).now();
            double newBal;
            double newBalPending;

            try {
                double currentBalance = wallet.getAmount();
                newBal = currentBalance + Double.parseDouble(amount);
                double currentBalPending = wallet.getAmount();
                newBalPending = currentBalPending - Double.parseDouble(amount);

            } catch (NumberFormatException e) {
                logger.info("Number format exception when parse double, check database or notification!");
                return;
            }
            wallet.setAmount_pending(newBalPending);
            wallet.setAmount(newBal);
            wallet.estimateWallet();

            if (ofy().save().entity(wallet).now() == null) {
                logger.severe("Updating new  wallet failed");
                return;
            }
        }
    }

    private void notifyWithdrawalBtc(HttpServletRequest req, HttpServletResponse resp, JSONObject obj) {
        JSONObject additionalData = obj.getJSONObject("additionalData");
        String hash = additionalData.getString("hash");
        WithdrawalTx tx = ofy().load().type(WithdrawalTx.class).filter("hash", hash).first().now();
        if (tx == null) {
            logger.warning("Withdrawal tx cannot be found!");
            return;
        }
        tx.setStatus("complete");
        if (ofy().save().entity(tx).now() == null) {
            logger.severe(" Update withdrawaTx status failed: txId " + tx.getId());
            return;
        }
        logger.info("update withdrawalTx completed!");
    }

    private void notificationUSDT(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String content = RESTUtil.readStringInput(req.getInputStream());
        logger.info("content received: " + content);
        JSONObject obj = new JSONObject(content);
        try {
            if ("wallet:addresses:new-payment".equalsIgnoreCase(obj.getString("type"))) {
                logger.info("Notify for deposit usdt!");
                notiDepositlUsdt(req, resp, obj);
                return;
            }
            if ("wallet:addresses:send".equalsIgnoreCase(obj.getString("type"))) {
                logger.info("Notify for withdrawal usdt!");
                notiWithdrawalUsdt(req, resp, obj);
            }
        } catch (JSONException e) {
            logger.severe("Json Syntax exception");
            return;
        }
    }

    private void notiWithdrawalUsdt(HttpServletRequest req, HttpServletResponse resp, JSONObject obj) {
        JSONObject additionalData = obj.getJSONObject("additionalData");
        String status = obj.getString("status");
        if (status.equalsIgnoreCase("complete")) {
            String hash = additionalData.getString("hash");
            WithdrawalTx tx = ofy().load().type(WithdrawalTx.class).filter("hash", hash).first().now();
            if (tx == null) {
                logger.warning("Withdrawal tx cannot be found!!!!");
                return;
            }
            tx.setStatus("complete");
            if (ofy().save().entity(tx).now() == null) {
                logger.severe(" Update withdrawaTx status failed: txId " + tx.getId());
                return;
            }
            logger.info("update withdrawalTx completed!");
        }
    }

    private void notiDepositlUsdt(HttpServletRequest req, HttpServletResponse resp, JSONObject obj) {
        JSONObject data = obj.getJSONObject("data");
        if (data.getString("address") == null || data.getString("address").length() == 0) {
            logger.warning("notification lack received address");
            return;
        }
        if ("pending".equalsIgnoreCase(obj.getString("status"))) {
            logger.info("notification pending received!");
            String addressStr = data.getString("address");
            logger.info("addressStr: " + addressStr);
            JSONObject additionalData = obj.getJSONObject("additionalData");
            String amount = additionalData.getJSONObject("amount").getString("amount");
            Address address = ofy().load().type(Address.class).id(addressStr).now();
            if (address == null) {
                logger.warning("CANNOT FOUND ADDRESS");
                resp.setStatus(500);
                return;
            }
            Wallet wallet = ofy().load().type(Wallet.class).id(address.getWalletId()).now();
            if (wallet == null) {
                logger.severe("wallet is null");
                return;
            }
            if (amount == null) {
                logger.severe("amount is null");
                return;
            }
            logger.info("wallet is : " + RESTUtil.gson.toJson(wallet) + " amount is " + amount);
            double newBal = wallet.getAmount_pending() + Double.parseDouble(amount);
            wallet.setAmount_pending(newBal);

            if (ofy().save().entity(wallet).now() == null) {
                logger.severe("Updating new  wallet failed");
                return;
            }
        }
        if ("complete".equalsIgnoreCase(obj.getString("status"))) {
            String addressStr = data.getString("address");

            logger.info("addressStr: " + addressStr);
            JSONObject additionalData = obj.getJSONObject("additionalData");
            String amount = additionalData.getJSONObject("amount").getString("amount");
            Address address = ofy().load().type(Address.class).id(addressStr).now();
            if (address == null) {
                logger.warning("CANNOT FOUND ADDRESS!");
                resp.setStatus(500);
                return;
            }
            Wallet wallet = ofy().load().type(Wallet.class).id(address.getWalletId()).now();
            double newBal;
            double newBalPending;

            try {
                double currentBalance = wallet.getAmount();
                newBal = currentBalance + Double.parseDouble(amount);
                double currentBalPending = wallet.getAmount();
                newBalPending = currentBalPending - Double.parseDouble(amount);

            } catch (NumberFormatException e) {
                logger.info("Number format exception when parse double, check database or notification!!");
                return;
            }
            wallet.setAmount_pending(newBalPending);
            wallet.setAmount(newBal);
            wallet.estimateWallet();

            if (ofy().save().entity(wallet).now() == null) {
                logger.severe("Updating new  wallet failed");
                return;
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 5) {
            logger.severe("URL's length is invalid");
            return;
        }

        if ("btc".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            updateWithdrawalBTCTx(req, resp);
            return;
        }
    }

    private void updateWithdrawalBTCTx(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String content = RESTUtil.readStringInput(req.getInputStream());
        logger.info("content update tx : " + content);
        JSONObject obj = new JSONObject(content);
        String status = obj.getString("status");
        logger.info("status: " + status);
        double amount = Double.parseDouble(obj.getString("amount"));
        logger.info("amount: " + amount);
        if ("pending".equalsIgnoreCase(status)) {
            WithdrawalTx tx = RESTUtil.gson.fromJson(content, WithdrawalTx.class);
            ofy().save().entity(tx).now();
            Wallet wallet = ofy().load().type(Wallet.class).id(tx.getWalletId()).now();
            if (wallet == null) {
                logger.severe("wallet null");
                return;
            }
            logger.info("Wallet : " + RESTUtil.gson.toJson(wallet));
            wallet.setAmount(wallet.getAmount() - amount);
            wallet.estimateWallet();
            ofy().save().entity(wallet).now();
            return;
        }
        if ("failed".equalsIgnoreCase(status)) {
            WithdrawalTx tx = RESTUtil.gson.fromJson(content, WithdrawalTx.class);
            tx.setHash("failed");
            tx.setStatus("failed");
            logger.info("TX failed: " + RESTUtil.gson.toJson(tx));
            ofy().save().entity(tx).now();
            return;
        }
    }
}

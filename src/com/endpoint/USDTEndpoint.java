package com.endpoint;

import com.entities.*;
import com.google.gson.Gson;
import com.util.ConvertCurrency;
import com.util.Generate;
import com.util.RESTUtil;
import com.util.RespMsg;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class USDTEndpoint extends HttpServlet {
    static Logger logger = Logger.getLogger(USDTEndpoint.class.getSimpleName());

    static String URL_API = "https://1-dot-digitalwalletservice.appspot.com";
    static String URL_send = "/v2/usdt/transfer/12amxorrnzp3sLeDrXThn5bhxVaB9LrArM/transaction";
    static String URL_create_address = "/v2/usdt/accounts/61e9ad59-71fe-43a6-95a1-6f757cdf9fe1/addresses";
    static String API_SECRET_KEY = "4b133b7e-9dda-4d7c-a54a-f6f6c1770f670.8091180048930605";
    static String API_KEY = "d0179161-6cec-4691-aaa1-181ca13143c3";
    static String ADDRESS = "12amxorrnzp3sLeDrXThn5bhxVaB9LrArM";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        User user = (User) req.getAttribute("user");
        logger.info(RESTUtil.gson.toJson(user));
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", "USDT").filter("userId", credential.getUserEmail()).first().now();
        if (wallet == null) {
            logger.warning("wallet null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find Wallet");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if ("withdrawal".equals(uriSplit[uriSplit.length - 2]))
            withdrawalUSDT(req, resp, wallet);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 6) {
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "URL is invalid");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        logger.info(RESTUtil.gson.toJson(credential));
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", "USDT").filter("userId", credential.getUserEmail()).first().now();
        if (wallet == null) {
            logger.warning("wallet null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find Wallet");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        String key = uriSplit[uriSplit.length - 1];
        switch (key) {
//            case "withdrawal":
//                withdrawalUSDT(req, resp, wallet);
//                break;
            case "deposit":
                depositUSDT(req, resp, wallet);
                break;
            default:
                resp.setStatus(400);
                RespMsg msg = new RespMsg(400, "Not found", "Cannot find Wallet");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
        }
    }
    private void depositUSDT(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws IOException {
        //CREATE AN BTC ADDRESS FOR USER TO MAKE DEPOSIT.
        HashMap<String, String> headers = new HashMap<>();
        String timeStamp = String.valueOf(Generate.currentTime() / 1000);
        String method = "POST";

        String rawSignature = timeStamp + method + URL_create_address;
        logger.info("Raw Signature Before Coverting: " + rawSignature);
        String HMACSignature = RESTUtil.toHMAC256(rawSignature, API_SECRET_KEY);
        logger.info("HMAC: " + HMACSignature);
        headers.put("CB-ACCESS-SIGN", HMACSignature);
        headers.put("CB-ACCESS-KEY", API_KEY);
        headers.put("CB-ACCESS-TIMESTAMP", timeStamp);

        //Create address
        Connection.Response response = Jsoup.connect(URL_API + URL_create_address)
                .headers(headers)
                .timeout(60*1000)
                .ignoreContentType(true)
                .method(Connection.Method.POST)
                .ignoreHttpErrors(true)
                .execute();
        logger.info("response creating USDT address: " + response);
        if (response.statusCode() != 201) {
            resp.setStatus(response.statusCode());
            resp.getWriter().println(response.statusMessage());
            return;
        }
        String content = response.body();
        logger.info("content: " + content);
        JSONObject obj = new JSONObject(content);
        String btcAddr = obj.getString("address");
        Address address = new Address(btcAddr, AddressType.USDT.toInt(), wallet.getId());
        if (ofy().save().entity(address).now() == null) {
            logger.severe("Cannot save address to database!");
            resp.setStatus(500);
            RespMsg msg = new RespMsg(500, "Server Error", "Cannot save address to database!");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        resp.getWriter().println(RESTUtil.gson.toJson(address));
    }
    private void withdrawalUSDT(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws IOException {
        String content = RESTUtil.readStringInput(req.getInputStream());
        double min_fee = 0.9;
        logger.info("Content: " + content);
        JSONObject obj = new JSONObject(content);
        String receiverAddress = obj.getString("receiverAddress");
        String amountStr = obj.getString("amount");
        double totalSentAmount;
        double oldBal;
        //        CHECK THE RECEIVING ADDRESS.
        if (receiverAddress == null || amountStr == null) {
            logger.warning("Missing receiver data!");
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "Missing receiver data!");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        try {
            oldBal = wallet.getAmount();
            totalSentAmount = min_fee + Double.parseDouble(amountStr);
        } catch (NumberFormatException e) {
            logger.severe("NumberformatException, Check database and content received!");
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Error Encounterd", "Number format is not well-form!");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        if (oldBal < totalSentAmount) {
            logger.warning("Wallet of user does not have enough money (minimum fee calculated)!");
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Forbidden", "Your Wallet does not have usdt to make withdrawal (fee included)! ");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }

        HashMap<String, String> headers = new HashMap<>();
        String timeStamp = String.valueOf(Generate.currentTime() / 1000);
        String method = "POST";
        logger.info("Content of receiver: " + content);
        String rawSignature = timeStamp + method + URL_send + content;
        logger.info("Raw Signature Before Coverting: " + rawSignature);
        String HMACSignature = RESTUtil.toHMAC256(rawSignature, API_SECRET_KEY);
        logger.info("HMAC: " + HMACSignature);
        headers.put("CB-ACCESS-SIGN", HMACSignature);
        headers.put("CB-ACCESS-KEY", API_KEY);
        headers.put("CB-ACCESS-TIMESTAMP", timeStamp);

        Connection.Response rsp = Jsoup.connect(URL_API + URL_send)
                .headers(headers).timeout(50 * 1000)
                .ignoreContentType(true)
                .method(Connection.Method.POST)
                .ignoreHttpErrors(true)
                .requestBody(content)
                .execute();
        logger.info("response " + rsp.statusMessage());
        logger.info("Link: " + URL_API + URL_send);
        if (rsp.statusCode() != 200) {
            resp.setStatus(rsp.statusCode());
            resp.getWriter().println(rsp.statusMessage());
            return;
        }
        String rspContent = rsp.body();
        logger.info("rsp content: " + rspContent);
        JSONObject jsonObj = new JSONObject(rspContent);
        WithdrawalTx withdrawalTx = new WithdrawalTx();
        withdrawalTx.setWalletId(wallet.getId());
        withdrawalTx.setAmount(jsonObj.getJSONObject("amount").getString("amount"));
        withdrawalTx.setCreated_at(jsonObj.getString("created_at"));
        withdrawalTx.setHash(jsonObj.getJSONObject("network").getString("hash"));
        withdrawalTx.setId(jsonObj.getString("id"));
        withdrawalTx.setStatus("pending");
        withdrawalTx.setType(AddressType.USDT.toInt());
        withdrawalTx.setReceiving_address(jsonObj.getJSONObject("to").getString("address"));

        if (withdrawalTx.getHash() == null) {
            logger.warning("Hash is null, tx does not exist!");
            resp.setStatus(500);
            RespMsg msg = new RespMsg(500, "Server Error", "Withdrawal fail, contact admin for support!");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        //update balance wallet
        double newBalance = wallet.getAmount() - totalSentAmount;
        wallet.setAmount(newBalance);
        if (ofy().save().entity(wallet).now() == null) {
            logger.severe("Cannot update new Wallet to database!");
            RESTUtil.showInternalError(req, resp,"Server error. Please try again!");
            return;
        }

        if (ofy().save().entity(withdrawalTx).now() == null) {
            logger.warning("fail to save withdrawalTx Id of Tx is : " + withdrawalTx.getId());
        }
        resp.setStatus(200);
        resp.getWriter().println(RESTUtil.gson.toJson(withdrawalTx));
    }

}


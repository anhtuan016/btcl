package com.endpoint;

import com.entities.*;
import com.util.Generate;
import com.util.RESTUtil;
import com.util.RespMsg;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.Connection.*;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.spi.LocaleNameProvider;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class XRPEndpoint extends HttpServlet {
    static Logger LOGGER = Logger.getLogger(XRPEndpoint.class.getSimpleName());

     String URL_API = "https://2-dot-digitalwalletservice.appspot.com";
     String URL_send = "/v2/xrp/transfer/57eed841-081a-4b56-92c0-9b5554cad3f3/transaction";
     String API_SECRET_KEY = "43c5ed3d-94a1-483c-8757-985d2ab0618c0.5503693877471405";
     String API_KEY = "bda710aa-77b4-4c51-ac06-8569a9090260";
     String Address = "rwpnQ3c7LWzHD8c4fTXPKZU5NakF3KmnSa";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        User user = (User) req.getAttribute("user");
        LOGGER.info(RESTUtil.gson.toJson(user));
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", "XRP").filter("userId", credential.getUserEmail()).first().now();
        if (wallet == null) {
            LOGGER.warning("wallet null");
           RESTUtil.showMissingParams(req, resp,"Cannot find wallet");
            return;
        }
        if ("withdrawal".equals(uriSplit[uriSplit.length-2]))
        withdrawalXRP(req, resp, wallet);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 6) {
            RESTUtil.showMissingParams(req, resp);
            return;
        }
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        LOGGER.info(RESTUtil.gson.toJson(credential));
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", "XRP").filter("userId", credential.getUserEmail()).first().now();
        if (wallet == null) {
            LOGGER.warning("wallet null");
            RESTUtil.showMissingParams(req, resp,"Cannot find wallet");
            return;
        }
        if (uriSplit[uriSplit.length - 1].equals("deposit")) {
            depositXRP(req, resp, wallet);
            return;
        }
       RESTUtil.showWrongURLError(req,resp);
    }

    private void withdrawalXRP(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws ServletException, IOException {
        double fee = 0.000015;
//        String content = RESTUtil.readStringInput(req.getInputStream());
//        LOGGER.info("strContent: " + content);
//        JSONObject Object = new JSONObject(content);
//        if (Object.getString("receiverAddress") == null || Object.getString("amount") == null) {
//            resp.setStatus(400);
//            RespMsg msg = new RespMsg(400, "Bad request", "Missing receiver data");
//            resp.getWriter().print(RESTUtil.gson.toJson(msg));
//            return;
//        }
        String strAmount = (String) req.getAttribute("amount");
        String strAddress = (String) req.getAttribute("receiverAddress");
        HashMap<String, Object> mapStr = new HashMap<>();
        HashMap<String, String> data = new HashMap<>();
        data.put("to",strAddress);
        data.put("amount",strAmount);
        mapStr.put("multip",false);
        mapStr.put("data", data);

        String content = RESTUtil.gson.toJson(mapStr);
        double amount = Double.parseDouble(strAmount);
        if (amount < 0.000001) {
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Amount is not an appropriate number");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        Double lAmount = wallet.getAmount() - (amount + fee);
        if (lAmount <= 0) {
            LOGGER.warning("amount balance");
            RespMsg msg = new RespMsg(500, "Server Error", "Withdrawal fail, contact admin for support!");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        wallet.setAmount(lAmount);
        if (ofy().save().entity(wallet).now() == null) {
            LOGGER.warning("save null");
            RespMsg msg = new RespMsg(500, "Server Error", "Withdrawal fail, contact admin for support!");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
        String method = "POST";
        String sign = RESTUtil.toHMAC256(timestamp + method + URL_send + content, API_SECRET_KEY);
        HashMap<String, String> map = new HashMap<>();
        map.put("CB-ACCESS-KEY", API_KEY);
        map.put("CB-ACCESS-SIGN", sign);
        map.put("CB-ACCESS-TIMESTAMP", timestamp);

//        LOGGER.warning(Connection.class.getProtectionDomain().getCodeSource().getLocation().toString());

        Connection.Response response = Jsoup.connect(URL_API + URL_send)
                .ignoreContentType(true)
                .timeout(60 * 1000)
                .headers(map)
                .method(Method.POST)
                .ignoreHttpErrors(true)
                .requestBody(content)
                .execute();

        if (response.statusCode() != 200) {
            LOGGER.warning("connect coinbase error");
            resp.setStatus(response.statusCode());
            resp.getWriter().print("connect coinbase error");
            return;
        }

        String strResp = response.body();
        JSONObject jsonObject = new JSONObject(strResp);
        LOGGER.info(strResp);
        WithdrawalTx tx = new WithdrawalTx();
        tx.setAmount(jsonObject.getJSONObject("data").getJSONObject("amount").getString("amount"));
        tx.setStatus("pending");
        tx.setCreated_at(Generate.stringTime());
        tx.setType(3);
        tx.setHash(jsonObject.getJSONObject("data").getJSONObject("network").getString("hash"));
        tx.setId(Generate.randomId());
        tx.setReceiving_address(jsonObject.getJSONObject("data").getJSONObject("to").getString("address"));
        tx.setWalletId(wallet.getId());

        if (tx.getHash() == null) {
            LOGGER.warning("Hash is null, tx does not exist");
            resp.setStatus(500);
            RespMsg msg = new RespMsg(500, "Server Error", "Withdrawal fail, contact admin for support!");
            resp.getWriter().println(RESTUtil.gson.toJson(msg));
            return;
        }
        if (tx.validate().size() != 0) {
            LOGGER.warning(RESTUtil.gson.toJson(tx.validate()));
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(tx));
            return;
        }
        if (ofy().save().entity(tx).now() == null) {
            LOGGER.warning("fail to save withdrawalTx Id of Tx is : " + tx.getId());
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(tx));
            return;
        }
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(tx));
    }


    private void depositXRP(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws ServletException, IOException {
        Address address = ofy().load().type(Address.class).id(Address).now();
        if (address == null) {
            LOGGER.warning("Address null");
            RESTUtil.showMissingParams(req, resp,"Address is null, check again!");
            return;
        }
        DestinationTag tag = new DestinationTag(wallet.getId(), address.getAddress());
        if (ofy().save().entity(tag).now() == null) {
            LOGGER.warning("save tag");
            RESTUtil.showInternalError(req, resp, "Server error. Please try again!");
            return;
        }
        LOGGER.info("resp address");
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(tag));
    }
}

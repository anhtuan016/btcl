package com.endpoint;

import com.entities.*;
import com.util.ConvertCurrency;
import com.util.Generate;
import com.util.RESTUtil;
import com.util.RespMsg;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class BTCEndpoint extends HttpServlet {
    public Logger logger = Logger.getLogger(BTCEndpoint.class.getSimpleName());
    public static String HOST_URL = "https://1-dot-digitalwalletservice.appspot.com";
    public static String BTC_ACCOUNT = "3c0dfcc9-0a79-45ef-9724-79ce945f44d3";
    public static String CREATE_ADDRESS_URI = "/v2/btc/accounts/" + BTC_ACCOUNT + "/addresses";
    public static String TRANSFER_BTC_URI = "/v2/btc/transfer/" + BTC_ACCOUNT + "/transaction";
    public static String API_KEY = "d0179161-6cec-4691-aaa1-181ca13143c3";
    public static String API_SECRET_KEY = "4b133b7e-9dda-4d7c-a54a-f6f6c1770f670.8091180048930605";
    public static String CHECKING_FEE_URL = "http://35.231.12.200:8080/ServletBitcoinj/wallet/transfer/" + BTC_ACCOUNT;
    static ExecutorService executor;

//    static {
//        executor = Executors.newFixedThreadPool(5);
//    }

//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//
//        /**
//         * LET USER DEPOSIT/WITHDRAWAL MONEY TO BITTREXCLONE. DEPOSIT /deposit----WITHDRAWAL /withdrawal
//         * **/
//        String uriSplit[] = req.getRequestURI().split("/");
//        if (uriSplit.length != 6) {
//            logger.warning("URL's length is invalid");
//            resp.setStatus(403);
//            RespMsg msg = new RespMsg(403, "Forbidden", "URL is invalid");
//            resp.getWriter().println(RESTUtil.gson.toJson(msg));
//            return;
//        }
//        UserCredential credential = (UserCredential) req.getAttribute("credential");
//        Wallet wallet = ofy().load().type(Wallet.class).filter("type","BTC").filter("userId",credential.getUserEmail()).first().now();
//        if (wallet == null) {
//            logger.severe("Cannot find wallet");
//            resp.setStatus(403);
//            RespMsg msg = new RespMsg(403, "Forbidden", "Cannot find Wallet");
//            resp.getWriter().println(RESTUtil.gson.toJson(msg));
//            return;
//        }
//        if ("deposit".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
//            depositBtc(req, resp, wallet);
//            return;
//        }
//        if ("withdrawal".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
//            withdrawalBtc(req, resp, wallet);
//            return;
//        }
//    }
//
//    private void withdrawalBtc(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws IOException {
//
//        String content = RESTUtil.readStringInput(req.getInputStream());
//        long min_fee=27600;
//        logger.info("Content: " + content);
//        JSONObject obj = new JSONObject(content);
//        String receiverAddress = obj.getString("receiverAddress");
//        String amountStr = obj.getString("amount");
//        double totalSentAmount;
//        double oldBal;
//        //        CHECK THE RECEIVING ADDRESS.
//        if (receiverAddress == null || amountStr == null) {
//            logger.warning("Missing receiver data");
//            resp.setStatus(403);
//            RespMsg msg = new RespMsg(403, "Forbidden", "Missing receiver data");
//            resp.getWriter().println(RESTUtil.gson.toJson(msg));
//            return;
//        }
//
//        try {
//            oldBal = wallet.getAmount();
//            totalSentAmount = ConvertCurrency.satoshiToBTC(min_fee) + Double.parseDouble(amountStr);
//        } catch (NumberFormatException e) {
//            logger.severe("NumberformatException, Check database and content received");
//            resp.setStatus(403);
//            RespMsg msg = new RespMsg(403, "Error Encounterd", "Number format is not well-form");
//            resp.getWriter().println(RESTUtil.gson.toJson(msg));
//            return;
//        }
//        if (oldBal < totalSentAmount) {
//            logger.warning("Wallet of user does not have enough money (minimum fee calculated)");
//            resp.setStatus(403);
//            RespMsg msg = new RespMsg(403, "Forbidden", "Your Wallet does not have BTC to make withdrawal (fee included) ");
//            resp.getWriter().println(RESTUtil.gson.toJson(msg));
//            return;
//        }
//
////        IN CASE OF ENOUGH FOR MINIMUM FEE LEVEL, PREPARE A PROCESSING REQUEST.
//        logger.warning("wallet : " +RESTUtil.gson.toJson(wallet));
//        WithdrawalTx tx = new WithdrawalTx(AddressType.BTC.toInt(), amountStr, Generate.stringTime(), receiverAddress, "processing", "processing");
//        tx.setWalletId(wallet.getId());
//        if (tx.validate().size()!=0){
//            logger.warning(RESTUtil.gson.toJson(tx.validate()));
//            resp.setStatus(500);
//            RespMsg msg = new RespMsg(500, "Server Error", "validate amount ");
//            resp.getWriter().println(RESTUtil.gson.toJson(msg));
//            return;
//        }
//        if (ofy().save().entity(tx).now() == null) {
//            logger.severe("SAVING TRANSACTION FAIL");
//            resp.setStatus(500);
//            RespMsg msg = new RespMsg(500, "Server Error", "Error occurred, please try to withdraw again ");
//            resp.getWriter().println(RESTUtil.gson.toJson(msg));
//            return;
//        }
//        logger.info("PREPARE RUNNABLE");
//        RunnableWithdrawalBTC runnableWithdrawalBTC = new RunnableWithdrawalBTC(content,tx,wallet,oldBal);
//        executor.execute(runnableWithdrawalBTC);
//        logger.info("EXECUTE RUNNABLE");
//
//        resp.setStatus(200);
//        resp.getWriter().println(RESTUtil.gson.toJson(tx));
//        return;
//
//    }
//
//    private void depositBtc(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws IOException {
//        //CREATE AN BTC ADDRESS FOR USER TO MAKE DEPOSIT.
//        HashMap<String, String> headers = new HashMap<>();
//        String timeStamp = String.valueOf(Generate.currentTime() / 1000);
//        String method = "POST";
//
//        String rawSignature = timeStamp + method + CREATE_ADDRESS_URI;
//        logger.info("Raw Signature Before Coverting: " + rawSignature);
//        String HMACSignature = RESTUtil.toHMAC256(rawSignature, API_SECRET_KEY);
//        logger.info("HMAC: " + HMACSignature);
//        headers.put("CB-ACCESS-SIGN", HMACSignature);
//        headers.put("CB-ACCESS-KEY", API_KEY);
//        headers.put("CB-ACCESS-TIMESTAMP", timeStamp);
//
//        //OPEN CONNECTION TO COINBASE API TO RETRIEVE ADDRESS.
//
//        Connection.Response rsp = Jsoup.connect(HOST_URL + CREATE_ADDRESS_URI)
//                .headers(headers)
//                .ignoreContentType(true)
//                .method(Connection.Method.POST)
//                .ignoreHttpErrors(true)
//                .execute();
//        logger.info("response from GAE creating eth address: " + rsp);
//        if (rsp.statusCode() != 201) {
//            resp.setStatus(rsp.statusCode());
//            resp.getWriter().println(rsp.statusMessage());
//            return;
//        }
//        String content = rsp.body();
//        logger.info("content: " + content);
//        JSONObject obj = new JSONObject(content);
//        String btcAddr = obj.getString("address");
//        Address address = new Address(btcAddr, AddressType.BTC.toInt(), wallet.getId());
//        if (ofy().save().entity(address).now() == null) {
//            logger.severe("Cannot save address to database!");
//            resp.setStatus(500);
//            RespMsg msg = new RespMsg(500, "Server Error", "Cannot save address to database");
//            resp.getWriter().println(RESTUtil.gson.toJson(msg));
//            return;
//        }
//        resp.getWriter().println(RESTUtil.gson.toJson(address));
//    }

}

package com.endpoint;

import com.entities.*;
import com.util.Generate;
import com.util.RESTUtil;
import com.util.RespMsg;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class BCHEndpoint extends HttpServlet {
    private Logger logger = Logger.getLogger(BCHEndpoint.class.getSimpleName());
    private String HOST_URL = "https://1-dot-digitalwalletservice.appspot.com";
    private String BCH_ACCOUNT = "4c31feb9-1c5b-49a5-a83a-e44142622461";
    private String CREATE_ADDRESS_URI = "/v2/bch/accounts/" + BCH_ACCOUNT + "/addresses";
    private String TRANSFER_BCH_URI = "/v2/bch/transfer/" + BCH_ACCOUNT+ "/transaction";
    private String API_KEY = "d0179161-6cec-4691-aaa1-181ca13143c3";
    private String API_SECRET_KEY = "4b133b7e-9dda-4d7c-a54a-f6f6c1770f670.8091180048930605";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] uriSplit = req.getRequestURI().split("/");
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        User user = (User) req.getAttribute("user");
        logger.info(RESTUtil.gson.toJson(user));
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", "BCH").filter("userId", credential.getUserEmail()).first().now();
        if (wallet == null) {
            logger.warning("wallet null");
            resp.setStatus(404);
            RespMsg msg = new RespMsg(404, "Not found", "Cannot find Wallet");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if ("withdrawal".equals(uriSplit[uriSplit.length-2])){
            withdrawalBCH(req, resp, wallet);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /**
         * LET USER DEPOSIT/WITHDRAWAL MONEY TO BITTREXCLONE. DEPOSIT /deposit----WITHDRAWAL /withdrawal
         * **/
        String uriSplit[] = req.getRequestURI().split("/");
        if (uriSplit.length != 6) {
            logger.warning("URL's length is invalid");
            RESTUtil.showWrongURLError(req, resp);
            return;
        }
        UserCredential credential = (UserCredential) req.getAttribute("credential");
        if (credential == null) {
            logger.warning("credential null");
            RESTUtil.showMissingParams(req, resp, "Cannot find credential");
            return;
        }
        Wallet wallet = ofy().load().type(Wallet.class).filter("type", "BCH").filter("userId", credential.getUserEmail()).first().now();
        if (wallet == null) {
            logger.severe("Cannot find wallet");
           RESTUtil.showMissingParams(req, resp,"Cannot find wallet");
            return;
        }
        if ("deposit".equalsIgnoreCase(uriSplit[uriSplit.length - 1])) {
            depositBCH(req, resp, wallet);
            return;
        }

    }

    private void withdrawalBCH(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws  IOException {
        double fee = 0.001;
        String strAmount = (String) req.getAttribute("amount");
        String strAddress = (String) req.getAttribute("receiverAddress");
        HashMap<String, Object> mapStr = new HashMap<>();
        HashMap<String, String> data = new HashMap<>();
        data.put("to",strAddress);
        data.put("amount",strAmount);
        mapStr.put("multip",false);
        mapStr.put("data", data);

        String content = RESTUtil.gson.toJson(mapStr);
        double dbAmount=0;
        try {
            dbAmount = wallet.getAmount() - (Double.parseDouble(strAmount)-fee);
        }catch (JSONException ex){
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "address or amount null");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (dbAmount <= 0) {
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Amount is not an appropriate number");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        } else {
            wallet.setAmount(dbAmount);
        }
        if (ofy().save().entity(wallet).now() == null) {
            logger.warning("fail to save wallet");
            RESTUtil.showInternalError(req, resp,"Save wallet fail, please try again!");
            return;
        }
        HashMap<String, String> hmHeaders = new HashMap<>();
        String timeStamp = String.valueOf(Generate.currentTime() / 1000);
        String method = "POST";
        String sign = RESTUtil.toHMAC256(timeStamp + method + TRANSFER_BCH_URI + content, API_SECRET_KEY);

        hmHeaders.put("CB-ACCESS-KEY", API_KEY);
        hmHeaders.put("CB-ACCESS-TIMESTAMP", timeStamp);
        hmHeaders.put("CB-ACCESS-SIGN", sign);
        Connection.Response response=null;
        try {
            response  = Jsoup.connect(HOST_URL + TRANSFER_BCH_URI)
                    .ignoreContentType(true)
                    .headers(hmHeaders)
                    .method(Connection.Method.POST)
                    .requestBody(content)
                    .execute();
        }catch (IOException ex) {
            logger.warning(HOST_URL + " not resp ");
            return;
        }

        logger.info("Transaction: " + response.body());
        JSONObject jsonObject = new JSONObject(response.body());

        WithdrawalTx tx = new WithdrawalTx();

        try {
            tx.setAmount(jsonObject.getJSONObject("amount").getString("amount"));
            tx.setStatus(jsonObject.getString("status"));
            tx.setCreated_at(Generate.stringTime());
            tx.setType(4);
            tx.setHash(jsonObject.getJSONObject("network").getString("hash"));
            tx.setId(Generate.randomId());
            tx.setReceiving_address(jsonObject.getJSONObject("to").getString("address"));
            tx.setWalletId(wallet.getId());
        }catch (JSONException ex){
            logger.info("JsonException" );
            resp.getWriter().print(RESTUtil.gson.toJson(jsonObject));
            return;
        }
        if (tx.validate().size()!=0){
            logger.warning(RESTUtil.gson.toJson(tx.validate()));
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(tx));
            return;
        }


        if (ofy().save().entity(tx).now() == null) {
            logger.warning("fail to save withdrawalTx Id of Tx is : " + tx.getId());
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(tx));
        }
        resp.setStatus(200);
        resp.getWriter().print(RESTUtil.gson.toJson(tx));

    }

    private void depositBCH(HttpServletRequest req, HttpServletResponse resp, Wallet wallet) throws IOException{
        Address add = ofy().load().type(Address.class).filter("walletId", wallet.getId()).first().now();
        if (add == null) {

            HashMap<String, String> hmHeaders = new HashMap<>();
            String timeStamp = String.valueOf(Generate.currentTime() / 1000);
            String method = "POST";
            String sign = RESTUtil.toHMAC256(timeStamp + method + CREATE_ADDRESS_URI, API_SECRET_KEY);

            hmHeaders.put("CB-ACCESS-KEY", API_KEY);
            hmHeaders.put("CB-ACCESS-TIMESTAMP", timeStamp);
            hmHeaders.put("CB-ACCESS-SIGN", sign);


            Connection.Response response = Jsoup.connect(HOST_URL+CREATE_ADDRESS_URI)
                    .headers(hmHeaders)
                    .ignoreContentType(true)
                    .method(Connection.Method.POST)
                    .ignoreHttpErrors(true)
                    .execute();

            if (response.statusCode() != 201) {
                logger.warning("API coinbase resp error");
                RESTUtil.showInternalError(req, resp, "Server error! Please try again!");
                return;
            }
            String rsp = response.body();
            logger.info("resp api: " + rsp);

            JSONObject object = new JSONObject(rsp);
            Address address = new Address(object.getString("address"),4,wallet.getId());
            if (ofy().save().entity(address).now() == null) {
                RESTUtil.showInternalError(req, resp, "Server error! Please try again!");
                return;
            }
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(address));
        } else {
            resp.setStatus(200);
            resp.getWriter().print(RESTUtil.gson.toJson(add));
        }
    }
}

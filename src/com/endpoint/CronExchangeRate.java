package com.endpoint;

import com.entities.ExchangeRateBTC;
import com.entities.ExchangeRateUSD;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CronExchangeRate extends HttpServlet {
    final long BTC_ID = 1;
    final long ETH_ID = 1027;
    final long XRP_ID = 52;
    final long BCH_ID = 1831;
    final long USDT_ID = 825;
    final String URL = "https://api.coinmarketcap.com/v2/ticker/";
    JSONObject obj = null;
    String rsp = null;
    Logger logger = Logger.getLogger(CronExchangeRate.class.getSimpleName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            getExchangeRate(req, resp, BTC_ID, "BTC");
            getExchangeRate(req, resp, ETH_ID, "ETH");
            getExchangeRate(req, resp, XRP_ID, "XRP");
            getExchangeRate(req, resp, BCH_ID, "BCH");
            getExchangeRate(req, resp, USDT_ID, "USDT");
        } catch (IOException e) {
            resp.setStatus(400);
            logger.severe("Cannot update Exchange Rate: "+e.getMessage());
            return;
        }

    }

    public void getExchangeRate(HttpServletRequest req, HttpServletResponse resp, long id, String name) throws IOException {
        logger.info("URL: "+URL + id);
        try {
            rsp = Jsoup.connect(URL + id+"/")
                    .ignoreContentType(true)
                    .method(Connection.Method.GET)
                    .execute()
                    .body();
        }catch (IOException e){
            logger.severe("IOE: "+e.getMessage());
            return;
        }
        obj = new JSONObject(rsp);
        ExchangeRateUSD rate = new ExchangeRateUSD(id, name);
        rate.setPrice(obj.getJSONObject("data").getJSONObject("quotes").getJSONObject("USD").getDouble("price"));
        rate.setPercent_change_1h(obj.getJSONObject("data").getJSONObject("quotes").getJSONObject("USD").getDouble("percent_change_1h"));
        rate.setPercent_change_24h(obj.getJSONObject("data").getJSONObject("quotes").getJSONObject("USD").getDouble("percent_change_24h"));
        rate.setLast_updated(obj.getJSONObject("data").getLong("last_updated"));
        if (ofy().save().entity(rate).now() == null) {
            resp.setStatus(400);
            logger.severe("Cannot Save Exchange Rate of " + name);
        }
        try {
            rsp = Jsoup.connect(URL + id+"/?convert=BTC")
                    .ignoreContentType(true)
                    .method(Connection.Method.GET)
                    .execute()
                    .body();
            obj = new JSONObject(rsp);
            ExchangeRateBTC rateBTC = new ExchangeRateBTC(id,name);
            rateBTC.setPrice(obj.getJSONObject("data").getJSONObject("quotes").getJSONObject("BTC").getDouble("price"));
            rateBTC.setPercent_change_24h(obj.getJSONObject("data").getJSONObject("quotes").getJSONObject("BTC").getDouble("percent_change_24h"));
            logger.info("RateBTC: "+rateBTC);
            if (ofy().save().entity(rateBTC).now()==null){
                logger.severe("Cannot update rateBTC");
                return;
            }
        }catch (IOException e){
            logger.severe("IOE2: "+e.getMessage());
            return;
        }

        resp.setStatus(200);

    }
}

package com.endpoint;

import com.entities.Transaction;
import com.googlecode.objectify.Key;
import com.util.RESTUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class TransactionEndpoint extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pair = req.getParameter("pair");
        int page = 1;
        int limit = 10;
        String pageStr = req.getParameter("page");
        String limitStr = req.getParameter("limit");
        try {
            page = Integer.parseInt(pageStr);
            limit = Integer.parseInt(limitStr);
        } catch (NumberFormatException e) {
            page = 1;
            limit = 10;
        }
        int totalItem = ofy().load().type(Transaction.class).filter("pair", pair).count();
        List<Transaction> list = ofy().load().type(Transaction.class).filter("pair", pair).order("-timelong").offset((page - 1) * limit).limit(limit).list();
        HashMap<String, Object> data = new HashMap<>();
        data.put("totalItem", totalItem);
        data.put("data", list);
        resp.getWriter().println(RESTUtil.gson.toJson(data));
    }
}

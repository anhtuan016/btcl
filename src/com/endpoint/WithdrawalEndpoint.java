package com.endpoint;



import com.entities.*;
import com.util.*;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class WithdrawalEndpoint extends HttpServlet{
    Logger logger = Logger.getLogger(WithdrawalEndpoint.class.getSimpleName());
     String url = "https://1-dot-bittrexcln.appspot.com/api/v1/balance/withdrawal/";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("withdrawal: get");
        String[] uriSplit = req.getRequestURI().split("/");
        String id = uriSplit[uriSplit.length-1];
        if (!uriSplit[uriSplit.length-2].equals("withdrawal")){
            logger.warning("url form");
            RESTUtil.showWrongURLError(req,resp);
            return;
        }
        if (uriSplit[uriSplit.length-1].length()!=20){
            logger.warning("code null");
            RESTUtil.showWrongURLError(req,resp);
            return;
        }
        WithdrawalTx tx = ofy().load().type(WithdrawalTx.class).id(id).now();

        logger.info("id withdrawal tx: "+id);

        if (tx==null){
            logger.warning("WithdrawalTx null");
            RESTUtil.showMissingParams(req,resp,"Transaction not found!");
            return;
        }
        if (!tx.getStatus().equals("pending")){
            RESTUtil.showBadRequest(req,resp,"Transaction is not pending.");
            return;
        }
        if (Generate.currentTime()-Long.parseLong(tx.getCreated_at())>15*60*1000){
            tx.setStatus("expired");
            if (ofy().save().entity(tx).now()==null){
                RESTUtil.showInternalError(req,resp);
                return;
            }
            logger.warning("withdrawal time has expired!");
            RESTUtil.showForbiddenError(req,resp,"Withdrawal time has expired!");
            return;
        }
        logger.info(RESTUtil.gson.toJson(tx));
        if (AddressType.getSymbol(tx.getType())==null){
            logger.warning("type null");
            RESTUtil.showInternalError(req,resp,"Server error");
            return;
        }
        tx.setStatus("complete");
        if (ofy().save().entity(tx).now()==null){
            RESTUtil.showInternalError(req,resp);
            return;
        }
        User user = (User) req.getAttribute("user");
        logger.info(RESTUtil.gson.toJson(user));
        req.setAttribute("user",user);
        req.setAttribute("receiverAddress",tx.getReceiving_address());
        req.setAttribute("amount",tx.getAmount());
        if (AddressType.getSymbol(tx.getType())==null)return;
        logger.info("/api/v1/wallet/"+AddressType.getSymbol(tx.getType()).toLowerCase()+"withdrawal");

        RequestDispatcher rd = req.getRequestDispatcher("/api/v1/wallet/"+AddressType.getSymbol(tx.getType()).toLowerCase()+"/withdrawal");
        rd.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = RESTUtil.readStringInput(req.getInputStream());
        logger.info("data: " + data);
        if (data.length()==0){
            logger.warning("data null");
            RESTUtil.showJsonError(req,resp);
            return;
        }
        JSONObject object = new JSONObject(data);
        String strType;
        String strAddress;
        String strAmount;
        try {
            strAddress = object.getString("receiverAddress");
        }catch (JSONException e){
            logger.warning("data address null");
            RESTUtil.showJsonError(req,resp,"Address not found!");
            return;
        }
        try {
            strAmount = object.getString("amount");
        }catch (JSONException e){
            logger.warning("data amount null");
            RESTUtil.showJsonError(req,resp,"Amount not found!");
            return;
        }
        try {
            strType = object.getString("type");
        }catch (JSONException e){
            logger.warning("data type null");
            RESTUtil.showJsonError(req,resp,"Type not found!");
            return;
        }
        if (AddressType.getStringType(strType)==null){
            logger.warning("data type null");
            RESTUtil.showJsonError(req,resp,"Address type invalid!");
            return;
        }
        User user  = (User) req.getAttribute("user");
        logger.info(RESTUtil.gson.toJson(user));
        if (user.getTwofactorauth()==1){
            String strAuthen;
            try {
                strAuthen = object.getString("authentication");
            }catch (JSONException e){
                logger.warning("TOTP not found");
                RESTUtil.showForbiddenError(req, resp, "TOTP is invalid");
                return;
            }
            if (!TwoFactorUtils.isValid(user.getSecret_key(), Integer.parseInt(strAuthen))) {
                logger.warning("TOTP is invalid");
                RESTUtil.showForbiddenError(req, resp, "TOTP is invalid");
                return;
            }

            if (ofy().load().type(WithdrawalWhitelist.class).filter("address",strAddress).first().now()==null){
                logger.warning("receiverAddress null");
                RESTUtil.showJsonError(req,resp,"The address does not exist in the whitelist.");
                return;
            }
        }
        Wallet wallet = ofy().load().type(Wallet.class).filter("userId",user.getEmail()).filter("type",AddressType.getStringType(strType)).first().now();
        if (wallet==null) {
            logger.warning("data wallet null");
            RESTUtil.showMissingParams(req,resp,"Wallet not exist!");
            return;
        }
        logger.info(RESTUtil.gson.toJson(wallet));
        WithdrawalTx tx = new WithdrawalTx();
        tx.setId(Generate.strCode());
        tx.setWalletId(wallet.getId());
        tx.setHash(Generate.randomId());
        tx.setReceiving_address(strAddress);
        tx.setCreated_at(Generate.stringTime());
        tx.setType(AddressType.getType(strType));
        tx.setAmount(strAmount);
        tx.setStatus("pending");
        logger.info(RESTUtil.gson.toJson(tx));
        if (tx.validate().size()!=0){
            logger.warning("validate withdrawal error");
            resp.setStatus(400);
            RespMsg msg = new RespMsg(400, "Bad request", "Validate withdrawal error");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        if (ofy().save().entity(tx).now()==null){
            logger.warning("save error");
            RESTUtil.showInternalError(req,resp,"Withdrawal fail, contact admin for support!");
            return;
        }
        try {
            SendMail.Activation(user.getEmail(), url +tx.getId(),"BittrexClone withdrawal verification");
            logger.info("Link acti: "+url + tx.getId());
        } catch (Exception ex) {
            logger.warning("Cannot send activation mail");
            RESTUtil.showForbiddenError(req, resp, "Could not send mail to: " + user.getEmail());
            return;
        }
        RESTUtil.showSuccess(req,resp,"Check mail to continue withdrawal!");
    }
}

package com.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.logging.Logger;

public class ConvertCurrency {
    private static Logger LOGGER = Logger.getLogger(ConvertCurrency.class.getSimpleName());

    public static BigInteger ETHtoWei(String eth){
        BigInteger finalResult=null;
        double ethAmount;
        try {
             ethAmount = Double.parseDouble(eth);

        }catch (NumberFormatException e){
            LOGGER.severe("Amount of eth was sent by user is not well-form");
            return null;
        }
        finalResult  = BigDecimal.valueOf(ethAmount).multiply(BigDecimal.valueOf(1000_000_000_000_000_000L)).toBigInteger();
        return  finalResult;
    }
    public static BigInteger ETHtoWei(double eth){
        BigInteger finalResult=null;
        finalResult  = BigDecimal.valueOf(eth).multiply(BigDecimal.valueOf(1000_000_000_000_000_000L)).toBigInteger();
        return  finalResult;
    }
    public static double weiToETH(BigInteger wei){
        double finalResult;
        BigDecimal ethBigDecimal = new BigDecimal(wei).divide(BigDecimal.valueOf(1000_000_000_000_000_000L));
        LOGGER.info("eth in BigDecimal: "+ethBigDecimal);
        finalResult = ethBigDecimal.doubleValue();

        return finalResult;
    }
    public static Long BTCToSatoshi(String btc){
        long finalResult;
        try {
            finalResult = (long) Double.parseDouble(btc)*1000*1000*100;
        }catch (NumberFormatException e){
            return null;
        }
     LOGGER.info("finalresult: "+finalResult);
        return finalResult;
    }
    public static double satoshiToBTC(long satoshi){
        double finalResult=0;

            finalResult = ((double)satoshi)/(100*1000*1000);

        LOGGER.info("finalresult: "+finalResult);
        return finalResult;
    }
}

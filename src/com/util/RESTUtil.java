package com.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RESTUtil {
    public static Gson gson = new Gson();
    public static Gson gsonExpose = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    private static Logger LOGGER = Logger.getLogger(RESTUtil.class.getName());
    public static Gson gson_noEncode = new GsonBuilder().disableHtmlEscaping().create();


    public static String readStringInput(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuffer buffer = new StringBuffer();
        String line;
        while ((line = br.readLine()) != null) {
            buffer.append(line);
        }
        LOGGER.setLevel(Level.INFO);
        LOGGER.info("String: " + buffer.toString());
        return buffer.toString();
    }

    public static String toHMAC256(String convertString, String key) {
        StringBuffer result = new StringBuffer();
        try {
            final Charset asciiCs = Charset.forName("US-ASCII");
            final Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            final SecretKeySpec secretKeySpec = new SecretKeySpec(asciiCs.encode(key).array(), "HmacSHA256");
            sha256_HMAC.init(secretKeySpec);
            final byte[] mac_data = sha256_HMAC.doFinal(asciiCs.encode(convertString).array());
            for (final byte element : mac_data) {
                result.append(Integer.toString((element & 0xff) + 0x100, 16).substring(1));
            }
            return result.toString();
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void showWrongURLError(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setStatus(400);
        RespMsg msg = new RespMsg(400, "Bad request", "URL is not well-form");
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showJsonError(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setStatus(403);
        RespMsg msg = new RespMsg(403, "Forbidden", "Json-data is not well-form");
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showJsonError(HttpServletRequest req, HttpServletResponse resp, String msgs) throws IOException {
        resp.setStatus(403);
        RespMsg msg = new RespMsg(403, "Forbidden", msgs);
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showMissingParams(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setStatus(404);
        RespMsg msg = new RespMsg(404, "Not Found", "Parameters are missing");
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showMissingParams(HttpServletRequest req, HttpServletResponse resp, String msgs) throws IOException {
        resp.setStatus(404);
        RespMsg msg = new RespMsg(404, "Not Found", msgs);
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showEmptyResult(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setStatus(204);
        RespMsg msg = new RespMsg(204, "No Content", "Data is empty");
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showEmptyResult(HttpServletRequest req, HttpServletResponse resp, String msg1) throws IOException {
        resp.setStatus(204);
        RespMsg msg = new RespMsg(204, "No Content", msg1);
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showInternalError(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setStatus(500);
        RespMsg msg = new RespMsg(500, "Internal Server Error", "Error encountered! Please contact admin for more information");
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showInternalError(HttpServletRequest req, HttpServletResponse resp, String msgs) throws IOException {
        resp.setStatus(500);
        RespMsg msg = new RespMsg(500, "Internal Server Error", msgs);
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showForbiddenError(HttpServletRequest req, HttpServletResponse resp, String msgs) throws IOException {
        resp.setStatus(403);
        RespMsg msg = new RespMsg(403, "Forbidden", msgs);
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showBadRequest(HttpServletRequest req, HttpServletResponse resp, String msgs) throws IOException {
        resp.setStatus(400);
        RespMsg msg = new RespMsg(400, "Bad Request", msgs);
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showSuccess(HttpServletRequest req, HttpServletResponse resp, String msgs) throws IOException {
        resp.setStatus(200);
        RespMsg msg = new RespMsg(200, "Success", msgs);
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static void showCreated(HttpServletRequest req, HttpServletResponse resp, String msgs) throws IOException {
        resp.setStatus(201);
        RespMsg msg = new RespMsg(201, "Created", msgs);
        resp.getWriter().println(RESTUtil.gson.toJson(msg));
    }

    public static double minusDouble(double a, double b) {
        double result = 0;
        BigDecimal subtrahend = BigDecimal.valueOf(a);
        BigDecimal minuend = BigDecimal.valueOf(b);
        result = subtrahend.subtract(minuend).doubleValue();
        return result;
    }

    public static double addDouble(double... numbers) {
        double result = 0;
        BigDecimal init = BigDecimal.valueOf(0);
        for (double a : numbers) {
            BigDecimal bd = BigDecimal.valueOf(a);
            init = init.add(bd);
        }
        result = init.doubleValue();
        return result;
    }

    public static int compareTo(double number1, double number2) {
        int result = 0;
        BigDecimal num1 = BigDecimal.valueOf(number1);
        BigDecimal num2 = BigDecimal.valueOf(number2);
        result = num1.compareTo(num2);
        return result;
    }


}

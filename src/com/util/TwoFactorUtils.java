package com.util;

import com.warrenstrange.googleauth.GoogleAuthenticator;

public class TwoFactorUtils {
    static GoogleAuthenticator gAuth = new GoogleAuthenticator();

    public static void addOtpAuthPath(String keyid, String secret, StringBuilder sb) {
        sb.append("otpauth://totp/techlancer%3A").append(keyid).append("%3Fsecret%3D").append(secret).append("%26issuer%3Dtechlancer");
    }

    public static String generateQrUrl(String keyid, String secret) {
        StringBuilder sb = new StringBuilder();
//        sb.append("https://www.google.com/chart?chs=200x200&chld=M|0&cht=qr&chl=");
        sb.append("https://chart.googleapis.com/chart?chs=200x200&chld=M|4&cht=qr&chl=");
        addOtpAuthPath(keyid, secret, sb);
        return sb.toString();
    }

    public static boolean isValid(String secret, int totp) {
        return gAuth.authorize(secret, totp);
    }
}

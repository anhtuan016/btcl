package com.util;

import com.sendgrid.*;


import java.io.IOException;

public class SendMail {
    public static void Activation(String to, String uri,String strSubject ) {

        String codeHTML = "<div>\n" +
                "\n" + "    <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid #e7e8ef 3.0pt;font-size:10pt;font-family:Calibri\" width=\"600\">\n" +
                "\n" + "        <tbody><tr style=\"border:#e7e8ef;padding:0 0 0 0\">\n" +
                "\n" + "            <td style=\"background-color:#465770;padding-left:15pt\" colspan=\"2\">\n" +
                "\n" + "                <br>\n" +
                "\n" + "                <img alt=\"[BITTREXCLONE]\" src=\"https://logos.textgiraffe.com/logos/logo-name/Coins-designstyle-coins-m.png\" class=\"CToWUd\"><br>\n" +
                "\n" + "                <br>\n" +
                "\n" + "            </td>\n" +
                "\n" + "        </tr>\n" +
                "\n" + "    " + "    <tr>\n" +
                "\n" + "            <td width=\"25\" style=\"border:white\">\n" +
                "\n" + "                &nbsp;\n" +
                "\n" + "            </td>\n" +
                "\n" + "            <td style=\"border:white\">\n" +
                "\n" + "                <br>\n" +
                "\n" + "                <h1><span style=\"font-size:19.0pt;font-family:Verdana;color:black\">BittrexClone Verification</span></h1>\n" +
                "\n" + "                <br>\n" +
                "\n" + "            </td>\n" +
                "\n" + "        </tr>\n" +
                "\n" + "        <tr>\n" +
                "\n" + "            <td width=\"25\" style=\"border:white\">\n" +
                "\n" + "                &nbsp;\n" +
                "\n" + "            </td>\n" +
                "\n" + "            <td style=\"border:white\">\n" +
                "\n" + "                <div style=\"color:#818181;font-size:10.5pt;font-family:Verdana\">\n" +
                "\n" + "                    Dear <a href=\"mailto:" + to + "\" target=\"_blank\">" + to + "</a>,<br>\n" +
                "\n" + "                    <br>\n" +
                "\n" + "                    Thank you for signing up with <a href=\"https://bittrex-205207.firebaseapp.com/home\" >bittrex-205207.firebaseapp.com</a>.  To provide you the best service possible, we require you to verify your email address.  If\n" +
                "\n" + "                    you are receiving this email and have never signed up with us, please feel free to ignore this email.  To finish your verification, please follow\n" +
                "\n" + "                    the directions below.<br>\n" +
                "\n" + "                    <br>\n" +
                "\n" + "                    Please click on the link below or copy and paste it into your browser to proceed with your registration.<br>\n" +
                "\n" + "                    <br>\n" +
                "\n" + "                    Click:     <a  href=\" " + uri + "\" >\"" + uri + "\"</a>\n" +
                "\n" + "                    <br>\n" +
                "\n" + "                    <br>\n" +
                "\n" + "                    Best regards,<br>\n" +
                "\n" + "                    BittrexClone Team<br>\n" +
                "\n" + "                </div>\n" +
                "\n" + "            </td>\n" +
                "\n" + "        </tr>\n" +
                "\n" + "        <tr>\n" +
                "\n" + "            <td colspan=\"2\" style=\"height:30pt;background-color:#e7e8ef;border:none\">\n" +
                "\n" + "                <center>You are receiving this email because you registered on <a href=\"https://bittrexclone.com\" style=\"color:#5b9bd5\" target=\"_blank\" >bittrexclone.com</a><br></center>\n" +
                "\n" + "            </td>\n" +
                "\n" + "        </tr>\n" +
                "\n" + "    </tbody></table>\n" +
                "\n" + "</div></div>";
//        String subject = "BittrexClone Account Verification";

        senderFAKE(to, strSubject, codeHTML);
    }

    public static void VerificationIP(String to, String uri, String ip) {
        String code = "<div>\n" +
                "\n" +
                "    <table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid #e7e8ef 3.0pt;font-size:10pt;font-family:Calibri\" width=\"600\">\n" +
                "\n" +
                "        <tbody><tr style=\"border:#e7e8ef;padding:0 0 0 0\">\n" +
                "\n" +
                "            <td style=\"background-color:#465770;padding-left:15pt\" colspan=\"2\">\n" +
                "\n" +
                "                <br>\n" +
                "\n" +
                "                <img alt=\"[BITTREXCLONE]\" src=\"https://logos.textgiraffe.com/logos/logo-name/Coins-designstyle-coins-m.png\" class=\"CToWUd\"><br>\n" +
                "\n" +
                "                <br>\n" +
                "\n" +
                "            </td>\n" +
                "\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr>\n" +
                "\n" +
                "            <td width=\"25\" style=\"border:white\">\n" +
                "\n" +
                "                &nbsp;\n" +
                "\n" +
                "            </td>\n" +
                "\n" +
                "            <td style=\"border:white\">\n" +
                "\n" +
                "                <br>\n" +
                "\n" +
                "                <h1><span style=\"font-size:19.0pt;font-family:Verdana;color:black\">BittrexClone IP Verification</span></h1>\n" +
                "\n" +
                "                <br>\n" +
                "\n" +
                "            </td>\n" +
                "\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr>\n" +
                "\n" +
                "            <td width=\"25\" style=\"border:white\">\n" +
                "\n" +
                "                &nbsp;\n" +
                "\n" +
                "            </td>\n" +
                "\n" +
                "            <td style=\"border:white\">\n" +
                "\n" +
                "                <div style=\"color:#818181;font-size:10.5pt;font-family:Verdana\">\n" +
                "\n" +
                "                    Dear <a href=\"mailto:" + to + "\" target=\"_blank\">" + to + "</a>,<br>\n" +
                "\n" +
                "                    <br>\n" +
                "\n" +
                "                    This is to notify you that we have detected a login from an IP address that has not been seen on your account before.  \n" +
                "\n" +
                "                    This will happen if you have a dynamic IP address, which is common.  You will need to login again from the same device using the link below.                    \n" +
                "\n" +
                "                    <br>\n" +
                "\n" +
                "                    <font size=\"3\">\n" +
                "\n" +
                "                        <a style=\"color:#5b9bd5\" href=\"" + uri + "\" target=\"_blank\">CLICK HERE TO LOGIN</a>\n" +
                "\n" +
                "                    </font>\n" +
                "\n" +
                "                    <br>\n" +
                "\n" +
                "                    It is common for your IP address to change even if you are using the same device from the same location.  \n" +
                "\n" +
                "                    However, if you are unsure please compare the IP address from this login event (<b>" + ip + "</b>) to the your ip address. \n" +
                "\n" +
                "                    <br>\n" +
                "\n" +
                "                    Best regards,\n" +
                "\n" +
                "                    <br>\n" +
                "\n" +
                "                    BittrexClone Team\n" +
                "\n" +
                "                    <br>\n" +
                "\n" +
                "                </div>\n" +
                "\n" +
                "            </td>\n" +
                "\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr>\n" +
                "\n" +
                "            <td colspan=\"2\" style=\"height:30pt;background-color:#e7e8ef;border:none\">\n" +
                "\n" +
                "                <center>You are receiving this email because you registered on <a href=\"https://bittrex-205207.firebaseapp.com/home\" style=\"color:#5b9bd5\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=vi&amp;q=https://bittrex.com&amp;source=gmail&amp;ust=1524554429119000&amp;usg=AFQjCNE-AB3OGh9B1_ytV4Ew-ciKMDnzrg\">bittrex.com</a><br></center>\n" +
                "\n" +
                "            </td>\n" +
                "\n" +
                "        </tr>\n" +
                "\n" +
                "    </tbody></table>\n" +
                "</div></div>";
        String subject = "BittrexClone New IP Address Verification";
        senderFAKE(to, subject, code);
    }



    public static boolean senderFAKE(String to, String subject, String codeHTML) {
        SendGrid sg = new SendGrid("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");
        Email from = new Email("bittrexclone@gmail.com");
//        String subject = "Sending with SendGrid is Fun";
        com.sendgrid.Email email = new com.sendgrid.Email(to);
//        Content content = new Content("text/plain", subject);

        Content  content =new Content("text/html",subject);
        content.setValue(codeHTML);
        Mail mail = new Mail(from, subject, email, content);
        Request request = new Request();
        try {
            request.setMethod(com.sendgrid.Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response r = sg.api(request);
        } catch (IOException ex) {
           return false;
        }
        return true;
    }


}

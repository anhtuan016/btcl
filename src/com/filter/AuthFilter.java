package com.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

import com.util.RESTUtil;
import com.util.RespMsg;

import com.entities.UserCredential;

public class AuthFilter implements Filter {
    Logger logger = Logger.getLogger(AuthFilter.class.getSimpleName());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        request.setCharacterEncoding("UTF-8");
        String tokenKey = req.getHeader("Authentication");
        if (tokenKey==null){
            logger.warning("Token key is missing");
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Bad Request", "Missing TokenKey? Please login-first!");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }
        UserCredential credential = UserCredential.loadCredential(tokenKey);
        if (credential == null) {
            logger.warning("Credential expired or not found");
            logger.warning("tokenKey: " + tokenKey);
            resp.setStatus(403);
            RespMsg msg = new RespMsg(403, "Bad Request", "Your login session was above 60 mins,please re-login!");
            resp.getWriter().print(RESTUtil.gson.toJson(msg));
            return;
        }

        req.setAttribute("credential", credential);
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}

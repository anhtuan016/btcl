package com.filter;

import com.entities.*;
import com.util.*;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class IPFilter implements Filter {
    Logger logger = Logger.getLogger(IPFilter.class.getSimpleName());
    static String strIpAddress = "https://1-dot-bittrexcln.appspot.com/api/v1/user/ipverification?id=";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String[] str = req.getRequestURI().split("/");

        if (str[str.length - 1].equals("login")) {
            String data = RESTUtil.readStringInput(req.getInputStream());
            if (data.length() == 0) {
                logger.warning("data null");
                resp.setStatus(400);
                RespMsg msg = new RespMsg(400, "bad request", "Data is not well-form");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
                return;
            }
            HashMap<String, String> map = RESTUtil.gson.fromJson(data, HashMap.class);
            if (map.get("email") == null || map.get("password") == null) {
                logger.warning("email or password null");
                resp.setStatus(400);
                RespMsg msg = new RespMsg(400, "Bad request", "Please enter email and password");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
                return;
            }
            User user = ofy().load().type(User.class).id(map.get("email")).now();
            if (user == null) {
                logger.warning("user null ");
                RESTUtil.showMissingParams(req, resp, "Email is not exist!");
                return;
            }
            if (!user.getPassword().equals(Generate.md5(map.get("password")))) {
                logger.warning("wrong password");
                resp.setStatus(404);
                RespMsg msg = new RespMsg(404, "Not found", "Password is incorrect");
                resp.getWriter().print(RESTUtil.gson.toJson(msg));
                return;
            }
            Integer[] listStatus = {1, 2};
            UserActivity userAtvt = ofy().load().type(UserActivity.class).filter("ipAddress", req.getRemoteAddr()).filter("activity", "VERIFY_NEW_IP").filter("userName", user.getEmail()).filter("status in", listStatus).first().now();
            if (userAtvt == null) {
                logger.warning("Check mail VERIFY NEW IP");
                UserActivity userAc = new UserActivity(req, user.getEmail());
                if (ofy().save().entity(userAc).now() == null) {
                    logger.warning("Cannot save user Activity");
                    RESTUtil.showForbiddenError(req, resp, "Cannot save user Activity");
                    return;
                }
                try {
                    SendMail.VerificationIP(user.getEmail(), strIpAddress + String.valueOf(userAc.getId()),userAc.getIpAddress());
                    logger.info("Send:" + user.getEmail());
                } catch (Exception ex) {
                    logger.warning("Cannot send activation mail");
                    RESTUtil.showForbiddenError(req, resp, "Cannot send activation mail");
                    return;
                }
                RESTUtil.showInternalError(req, resp, "Check mail VERIFY NEW IP");
                return;
            }
            if (user.getTwofactorauth() == 1) {
                String secret_key = user.getSecret_key();
                int totpStr;
                try {
                    totpStr = Integer.parseInt(map.get("totp"));
                    logger.info("TOTP: " + totpStr);
                }catch (NullPointerException ex){
                    RESTUtil.showBadRequest(req,resp,"TOTP is invalid");
                    return;
                }catch (NumberFormatException e){
                    RESTUtil.showBadRequest(req,resp,"TOTP is invalid");
                    return;
                }

            if (!TwoFactorUtils.isValid(secret_key, totpStr)) {
                logger.warning("totp is invalid");
                RESTUtil.showBadRequest(req,resp,"Totp is invalid");
                return;
            }
            }
            req.setAttribute("email", map.get("email"));
            logger.info("login:" + user.getEmail());
            chain.doFilter(request, response);
            return;
        }


        if (str[str.length - 1].equals("withdrawal")) {
            logger.info("filter request");
            if (req.getHeader("Authentication") == null) {
                logger.warning("Authentication null");
                return;
            }
            logger.info("get tokenkey");
            String tokenKey = req.getHeader("Authentication");
            logger.info("UserCredential");
            UserCredential credential = ofy().load().type(UserCredential.class).filter("secretToken", tokenKey).first().now();
            if (credential == null) {
                logger.warning("credential null");
                return;
            }
            logger.info("get user");
            User user = ofy().load().type(User.class).id(credential.getUserEmail()).now();
            if (user == null) {
                logger.warning("user null");
                return;
            }
            UserActivity userActivity = ofy().load().type(UserActivity.class).filter("ipAddress", req.getRemoteAddr()).filter("activity", "VERIFY_NEW_IP").filter("userName", user.getEmail()).first().now();
            if (userActivity == null) {
                logger.warning("userActivity null");
                return;
            }
            if (user.getTwofactorauth() == 1) {
                logger.warning("Check IP");
                if (userActivity.getStatus() != 2) {
                    resp.setStatus(400);
                    RESTUtil.showBadRequest(req, resp, "IP is not exist in WhitelistIP (2-Factor-Authentication enabled)!");
                    return;
                }
            }
            req.setAttribute("user", user);
            chain.doFilter(request, response);
            return;
        }


        if (str[str.length - 2].equals("withdrawal")) {
            logger.info("filter request");
            WithdrawalTx tx = ofy().load().type(WithdrawalTx.class).id(str[str.length - 1]).now();
            if (tx == null) {
                logger.warning("WithdrawalTx null");
                RESTUtil.showMissingParams(req, resp, "Transaction cannot be found!");
                return;
            }
            logger.info("get user");
            Wallet wallet = ofy().load().type(Wallet.class).id(tx.getWalletId()).now();
            if (wallet == null) {
                logger.warning("wallet null");
                return;
            }
            User user = ofy().load().type(User.class).id(wallet.getUserId()).now();
            if (user == null) {
                logger.warning("user null");
                return;
            }
            UserActivity userActivity = ofy().load().type(UserActivity.class).filter("ipAddress", req.getRemoteAddr()).filter("activity", "VERIFY_NEW_IP").filter("userName", user.getEmail()).first().now();

            if (userActivity == null) {
                logger.warning(" ip is not activate!");
                return;
            }
            UserCredential credential = ofy().load().type(UserCredential.class).id(user.getEmail()).now();
            logger.info("User:" + RESTUtil.gson.toJson(user));
            req.setAttribute("credential", credential);
            req.setAttribute("user", user);
            chain.doFilter(request, response);
            return;
        }
        logger.warning("url filter.");

    }

    @Override
    public void destroy() {

    }
}

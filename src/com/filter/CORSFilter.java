package com.filter;

import com.entities.*;
import com.googlecode.objectify.ObjectifyService;
import entitiesSupport.APICredentical;
import entitiesSupport.SupportCurrencies;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CORSFilter implements Filter {
    static {
        ObjectifyService.register(User.class);
        ObjectifyService.register(UserCredential.class);
        ObjectifyService.register(UserActivity.class);
        ObjectifyService.register(Wallet.class);
        ObjectifyService.register(Address.class);
        ObjectifyService.register(WithdrawalTx.class);
        ObjectifyService.register(DepositTx.class);
        ObjectifyService.register(Order.class);
        ObjectifyService.register(DestinationTag.class);
        ObjectifyService.register(PriceLevelAsk.class);
        ObjectifyService.register(PriceLevelBid.class);
        ObjectifyService.register(Transaction.class);
        ObjectifyService.register(MarketPair.class);
        ObjectifyService.register(WithdrawalWhitelist.class);
        ObjectifyService.register(CandleStickItem30m.class);
        ObjectifyService.register(ExchangeRateUSD.class);
        ObjectifyService.register(ExchangeRateBTC.class);
        ObjectifyService.register(APICredentical.class);
        ObjectifyService.register(SupportCurrencies.class);

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        request.setCharacterEncoding("UTF-8");
//        response.setContentType("application/json");
        resp.addHeader("Access-Control-Allow-Origin", "*");
        resp.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
        resp.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authentication");
//        response.setContentType("application/json");
        if (req.getMethod().equals("OPTIONS")) {
            resp.setStatus(HttpServletResponse.SC_OK);
            return;
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}

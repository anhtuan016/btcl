package com.entities;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.util.*;


import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;


@Entity
public class Order {
    @Id
    private long id;
    @Index
    private double baseFee;
    @Index
    private String pair;
    @Index
    private String type; //Side on matchengine: BUY/SELL
    @Index
    private double amount;
    @Index
    private double price; //MULTIPLE TO 10^12 WHEN POST TO MATCHINGENGINE.
    @Index
    private String userId;
    @Index
    private double filled;
    @Index
    private long created_at;
    @Index
    private long updated_at;
    @Index
    private int status; //0-Cancel,1-Complete,2-Waiting.
    @Index
    private double actual_rate;
    @Index
    private double fee;
    @Index
    private String valid; //processing,failed,created!


    public Order() {
        id = System.currentTimeMillis() + ThreadLocalRandom.current().nextInt(1, 999);
        created_at = Generate.currentTime();
        updated_at = Generate.currentTime();
        filled = 0;
        status = 2;
        valid = "processing";
    }

    public Order(long id, String pair, double amount, double price, String userId, int status) {
        this.id = id;
        this.pair = pair;
        this.amount = amount;
        this.price = price;
        this.userId = userId;
        this.status = status;
    }

    public Order(double amount, double price) {
        this.amount = amount;
        this.price = price;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public double getBaseFee() {
        return baseFee;
    }

    public void setBaseFee(double baseFee) {
        this.baseFee = baseFee;
    }

    public double getActual_rate() {
        return actual_rate;
    }

    public void setActual_rate(double actual_rate) {
        this.actual_rate = actual_rate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getFilled() {
        return filled;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public void setFilled(double filled) {
        this.filled = filled;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public HashMap<String, String> validate() {
        HashMap<String, String> errors = new HashMap<>();
        if (this.amount < 0) {
            errors.put("amount", "amount <0!");
        }
        if (this.price < 0) {
            errors.put("price", "price <0!");
        }
        return errors;
    }
}



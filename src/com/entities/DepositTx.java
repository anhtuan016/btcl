package com.entities;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.HashMap;
import java.util.UUID;
@Entity
public class DepositTx {
    @Id
    private String id;
    @Index
    private int type;
    @Index
    private String amount;
    @Index
    private String created_at;
    @Index
    private String receiving_address;
    @Index
    private String status;
    @Index
    private String hash;
    @Index
    private long walletId;


    public DepositTx() {
    }

    public DepositTx(String id, int type, String amount, String created_at, String receiving_address, String status, String hash) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.created_at = created_at;
        this.receiving_address = receiving_address;
        this.status = status;
        this.hash = hash;
    }

    public DepositTx(int type, String amount, String created_at, String receiving_address, String status, String hash) {
        this.id = UUID.randomUUID().toString();
        this.type = type;
        this.amount = amount;
        this.created_at = created_at;
        this.receiving_address = receiving_address;
        this.status = status;
        this.hash = hash;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getReceiving_address() {
        return receiving_address;
    }

    public void setReceiving_address(String receiving_address) {
        this.receiving_address = receiving_address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public long getWalletId() {
        return walletId;
    }

    public void setWalletId(long walletId) {
        this.walletId = walletId;
    }

    public HashMap<String, String> validate() {
        HashMap<String, String> errors = new HashMap<>();
        if (this.amount==null) {
            errors.put("amount", "amount null!");
        }
        if (Double.parseDouble(this.amount)<0) {
            errors.put("amount", "amount <0!");
        }
        return errors;
    }

}

//package com.entities;
//
//
//import com.endpoint.BTCEndpoint;
//import com.util.ConvertCurrency;
//import com.util.Generate;
//import com.util.RESTUtil;
//import org.json.JSONObject;
//import org.jsoup.Connection;
//import org.jsoup.Jsoup;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.logging.Logger;
//
//import static com.googlecode.objectify.ObjectifyService.ofy;
//
//public class RunnableWithdrawalBTC implements Runnable {
//    private String content;
//    private WithdrawalTx tx;
//    private Wallet wallet;
//    private double oldBal;
//    private static Logger logger = Logger.getLogger(RunnableWithdrawalBTC.class.getSimpleName());
//
//    public RunnableWithdrawalBTC(String content, WithdrawalTx tx, Wallet wallet, double oldBal) {
//        this.content = content;
//        this.tx = tx;
//        this.wallet = wallet;
//        this.oldBal = oldBal;
//    }
//
//
//    @Override
//    public void run() {
//        JSONObject obj = new JSONObject(content);
//        String receiverAddress = obj.getString("receiverAddress");
//        String amountStr = obj.getString("amount");
////        COMPARE BALANCE AND SENTAMOUNT + FEE.
//
//        try {
//            double amount = Double.parseDouble(amountStr);
//            String feeStr = Jsoup.connect(BTCEndpoint.CHECKING_FEE_URL).timeout(0).ignoreContentType(true).method(Connection.Method.PUT).requestBody(content).execute().body();
//            double fee = ConvertCurrency.satoshiToBTC(Long.parseLong(feeStr.trim()));
//            double total_sent = amount + fee;
//
//
//            if (wallet.getAmount() < total_sent) {
//                rollBackTx(tx);
//                return;
//            }
////                PREPARE HMAC AUTHENTICATION.
//            HashMap<String, String> headers = new HashMap<>();
//            String timeStamp = String.valueOf(Generate.currentTime() / 1000);
//            String method = "POST";
//            String rawSignature = timeStamp + method + BTCEndpoint.TRANSFER_BTC_URI + content;
//            String HMACSignature = RESTUtil.toHMAC256(rawSignature, BTCEndpoint.API_SECRET_KEY);
//            headers.put("CB-ACCESS-SIGN", HMACSignature);
//            headers.put("CB-ACCESS-KEY", BTCEndpoint.API_KEY);
//            headers.put("CB-ACCESS-TIMESTAMP", timeStamp);
//            String rsp = Jsoup.connect(BTCEndpoint.HOST_URL + BTCEndpoint.TRANSFER_BTC_URI).timeout(0)
//                    .headers(headers)
//                    .ignoreContentType(true)
//                    .method(Connection.Method.POST)
//                    .requestBody(content)
//                    .execute().body();
//            JSONObject jsonTx = new JSONObject(rsp);
//            tx.setHash(jsonTx.getJSONObject("network").getString("hash"));
//            tx.setStatus("pending");
//            String resp = Jsoup.connect("https://1-dot-bittrexcln.appspot.com/api/v1/notification/btc")
//                    .ignoreContentType(true)
//                    .method(Connection.Method.PUT)
//                    .requestBody(RESTUtil.gson.toJson(tx))
//                    .execute()
//                    .body();
//
//
//        } catch (IOException | NumberFormatException e) {
//            rollBackTx(tx);
//        }
//    }
//
//    public void rollBackTx(WithdrawalTx tx) {
//        tx.setStatus("failed");
//        try {
//            String resp = Jsoup.connect("https://1-dot-bittrexcln.appspot.com/api/v1/notification/btc")
//                    .ignoreContentType(true)
//                    .method(Connection.Method.PUT)
//                    .requestBody(RESTUtil.gson.toJson(tx))
//                    .execute()
//                    .body();
//        } catch (IOException e) {
//
//        }
//    }
//}

package com.entities;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.util.Generate;


import java.util.HashMap;

import java.util.UUID;


@Entity
public class Transaction {
    @Id
    private String id;
    @Index
    private String pair;
    @Index
    private String created_at;
    @Index
    private String incoming_type;
    @Index
    private double price;
    @Index
    private double quantity;
    @Index
    private double total_cost;
    @Index
    private long timelong;

    public Transaction() {
        created_at = Generate.getTimeUTC();
        id = UUID.randomUUID().toString();
        timelong = System.currentTimeMillis();
    }

    public Transaction(String pair, String incoming_type) {
        created_at = Generate.getTimeUTC();
        timelong = System.currentTimeMillis();
        id = UUID.randomUUID().toString();
        this.pair = pair;
        this.incoming_type = incoming_type;

    }

    public long getTimelong() {
        return timelong;
    }

    public void setTimelong(long timelong) {
        this.timelong = timelong;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getIncoming_type() {
        return incoming_type;
    }

    public void setIncoming_type(String incoming_type) {
        this.incoming_type = incoming_type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(double total_cost) {
        this.total_cost = total_cost;
    }

    public HashMap<String, String> validate() {
        HashMap<String, String> errors = new HashMap<>();
        if (this.price<0  ) {
            errors.put("price", "price is not valid!");
        }
        if (this.quantity<0) {
            errors.put("quantity", "quantity is not valid!");
        }if (this.total_cost<0) {
            errors.put("total_cost", "total_cost is not valid!");
        }
        return errors;
    }
}

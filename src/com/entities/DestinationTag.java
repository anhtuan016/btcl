package com.entities;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.util.Generate;
@Entity
public class DestinationTag {
    @Id
    private String tag;
    @Index
    private String created_at;
    @Index
    private long walletId;
    @Index
    private String address;

    public DestinationTag(long walletId,String address){
        tag= Generate.numberRandom();
        created_at=Generate.getTimeUTC();
        this.walletId = walletId;
        this.address=address;
    }
    public DestinationTag(){
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public long getWalletId() {
        return walletId;
    }

    public void setWalletId(long walletId) {
        this.walletId = walletId;
    }
}

package com.entities;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.util.Generate;

import javax.servlet.http.HttpServletRequest;

@Entity
public class UserActivity {
    @Id
    private long id;
    @Index
    private String timeStamp;
    @Index
    private String ipAddress;
    @Index
    private String activity;
    @Index
    private String userName;
    @Index
    private String userAgent;
    @Index
    private int status;


    public UserActivity(HttpServletRequest request,String activity,String userEmail){
        id= Generate.currentTime();
        timeStamp=Generate.formetDate(id);
        this.activity=activity;
        ipAddress=getClientIp(request);
        userName=userEmail;
        userAgent=getClientUserAgent(request);
        status=0;
    }
    public UserActivity(HttpServletRequest request,String userEmail){
        id= Generate.currentTime();
        timeStamp=Generate.formetDate(id);
        this.activity="UNKNOWN_IP_LOGIN";
        ipAddress=getClientIp(request);
        userName=userEmail;
        userAgent=getClientUserAgent(request);
        status=0;
    }

    public UserActivity(){
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private static String getClientIp(HttpServletRequest request) {
        return request.getRemoteAddr();
    }
    private static String getClientUserAgent(HttpServletRequest request) {
        return request.getHeader("User-Agent");
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}

package com.entities;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.util.Generate;


import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
public class User {
    @Id
    private String email;
    @Index
    private String name;
    @Index
    private String password;
    @Index
    private String resource;
    @Index
    private String resource_path;
    @Index
    private String avatar_url;
    @Index
    private String created_at;
    @Index
    private String country;
    @Index
    private String sponsorId;
    @Index
    private int status; //0-deleted,1-actived,2-unactived
    @Index
    private String activeCode;
    @Index
    private int twofactorauth;
    @Index
    private String secret_key;
    @Index
    private String qr_url;
    @Index
    private String birthday;
    @Index
    private String address;
    @Index
    private String identifyCard_url;
    @Index
    private String passport_url;

    public User(String name, String password, String emaill) {
        this.status = 2;
        this.activeCode = Generate.strCode();
        this.name = name;
        this.password = password;
        this.email = emaill;
        resource = "User";
        created_at = Generate.getTimeUTC();
        twofactorauth = 0;
    }

    public User() {
    }


    public String getQr_url() {
        return qr_url;
    }

    public void setQr_url(String qr_url) {
        this.qr_url = qr_url;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public int getTwofactorauth() {
        return twofactorauth;
    }

    public void setTwofactorauth(int twofactorauth) {
        this.twofactorauth = twofactorauth;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getActiveCode() {
        return activeCode;
    }

    public void setActiveCode(String activeCode) {
        this.activeCode = activeCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(String sponsorId) {
        this.sponsorId = sponsorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getResource_path() {
        return resource_path;
    }

    public void setResource_path(String resource_path) {
        this.resource_path = resource_path;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdentifyCard_url() {
        return identifyCard_url;
    }

    public void setIdentifyCard_url(String identifyCard_url) {
        this.identifyCard_url = identifyCard_url;
    }

    public String getPassport_url() {
        return passport_url;
    }

    public void setPassport_url(String passport_url) {
        this.passport_url = passport_url;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public void initiatedWallet() {
        Wallet walletBTC = new Wallet();
        walletBTC.setUserId(this.email);
        walletBTC.setType("BTC");
        walletBTC.setName("Bitcoin");
        ofy().save().entity(walletBTC).now();

        Wallet walletETH = new Wallet();
        walletETH.setUserId(this.email);
        walletETH.setType("ETH");
        walletETH.setName("Ethereum");
        ofy().save().entity(walletETH).now();

        Wallet walletXRP = new Wallet();
        walletXRP.setUserId(this.email);
        walletXRP.setType("XRP");
        walletXRP.setName("Ripple");
        ofy().save().entity(walletXRP).now();

        Wallet walletBCH = new Wallet();
        walletBCH.setUserId(this.email);
        walletBCH.setType("BCH");
        walletBCH.setName("Bitcoin-cash");
        ofy().save().entity(walletBCH).now();

        Wallet walletUSDT = new Wallet();
        walletUSDT.setUserId(this.email);
        walletUSDT.setType("USDT");
        walletUSDT.setName("Tether");
        ofy().save().entity(walletUSDT).now();
    }


    private static final String regexEmail = "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    private static final String regexPass = "(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,16}$";
    private static final String regexDate = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";


    public HashMap<String, String> validateRegister() {
        HashMap<String, String> errors = new HashMap<>();

        if (this.email==null) {
            errors.put("email", "enter email!");
        }
        if ( this.password==null) {
            errors.put("pasword", "enter password!");
        }
        if (!regexEmail(this.email)) {
            errors.put("email", "email is not valid!");
        }
        if ( !regexPassword(this.password)) {
            errors.put("pasword", "Password has to from 8-16 characters. There are at least 1 uppercase and 1 special character!");
        }else if (this.password.length()!=32){
            this.password=Generate.md5(this.password);
        }
        if (this.status<0||this.status > 2) {
            errors.put("status", "status is not valid!");
        }
        if (this.activeCode.length()==0) {
            errors.put("activeCode", "activeCode is not valid!");
        }
        return errors;
    }
    public HashMap<String, String> validateFull() {
        HashMap<String, String> errors = new HashMap<>();
        if (!regexEmail(this.email)) {
            errors.put("email", "email is not valid!");
        }
        if (this.name.length()==0) {
            errors.put("name", "name is not valid!");
        }
        if (this.address.length()==0) {
            errors.put("address", "address is not valid!");
        }
        if (this.birthday.length()==0 || !regexDate(this.birthday)) {
            errors.put("birthday", "birthday is not valid!");
        }
        if (this.status<0|| this.status > 2) {
            errors.put("status", "status is not valid!");
        }
        return errors;
    }

    public boolean regexEmail(String email) {
        Pattern pattern = Pattern.compile(regexEmail);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean regexPassword(String password) {
        Pattern pattern = Pattern.compile(regexPass);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    private boolean regexDate(String birthday) {
        Pattern pattern = Pattern.compile(regexDate);
        Matcher matcher = pattern.matcher(birthday);
        return matcher.matches();
    }
}

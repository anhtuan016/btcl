package com.entities;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.util.*;
import com.util.RESTUtil;

import java.util.ArrayList;

import java.util.HashMap;

import java.util.concurrent.ThreadLocalRandom;


@Entity
public class PriceLevelBid {
    @Id
    private String id;
    @Index
    double price;
    @Index
    double amount;
    @Index
    double total;
    @Index
    double sum;
    @Index
    String pair;

    public PriceLevelBid() {
    }

    public PriceLevelBid(long id, String pair) {
        this.id = id+pair;
        this.pair = pair;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }


    public HashMap<String, String> validate() {
        HashMap<String, String> errors = new HashMap<>();
        if (this.amount<0  ) {
            errors.put("amount", "amount is not valid!");
        }
        if (this.price<0) {
            errors.put("price", "price is not valid!");
        }
        return errors;
    }
}

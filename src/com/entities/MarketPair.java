package com.entities;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.util.Generate;

import java.util.HashMap;

@Entity
public class MarketPair {
    @Id
    private String id; //ID IS CURRENCY PAIR;
    @Index
    private double last_price;
    @Index
    private double volume;
    @Index
    private double bid_price;
    @Index
    private double ask_price;
    @Index
    private double high_24h;
    @Index
    private double low_24h;
    @Index
    private String name;
    @Index
    private long created_at;
    @Index
    private double coin0_totalbid;
    @Index
    private double coin1_totalbid;
    @Index
    private double coin0_totalask;
    @Index
    private double coin1_totalask;
    @Index
    private double percentageChange;

    public MarketPair(String id) {
        this.id = id;
        last_price = 0;
        volume = 0;
        bid_price = 0;
        ask_price = 0;
        high_24h = 0;
        low_24h = 0;
        created_at = Generate.currentTime();
        name = getName(id);
    }


    public MarketPair() {
    }

    public double getPercentageChange() {
        return percentageChange;
    }

    public void setPercentageChange(double percentageChange) {
        this.percentageChange = percentageChange;
    }

    public double getCoin0_totalbid() {
        return coin0_totalbid;
    }

    public void setCoin0_totalbid(double coin0_totalbid) {
        this.coin0_totalbid = coin0_totalbid;
    }

    public double getCoin1_totalbid() {
        return coin1_totalbid;
    }

    public void setCoin1_totalbid(double coin1_totalbid) {
        this.coin1_totalbid = coin1_totalbid;
    }

    public double getCoin0_totalask() {
        return coin0_totalask;
    }

    public void setCoin0_totalask(double coin0_totalask) {
        this.coin0_totalask = coin0_totalask;
    }

    public double getCoin1_totalask() {
        return coin1_totalask;
    }

    public void setCoin1_totalask(double coin1_totalask) {
        this.coin1_totalask = coin1_totalask;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLast_price() {
        return last_price;
    }

    public void setLast_price(double last_price) {
        this.last_price = last_price;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getBid_price() {
        return bid_price;
    }

    public void setBid_price(double bid_price) {
        this.bid_price = bid_price;
    }

    public double getAsk_price() {
        return ask_price;
    }

    public void setAsk_price(double ask_price) {
        this.ask_price = ask_price;
    }

    public double getHigh_24h() {
        return high_24h;
    }

    public void setHigh_24h(double high_24h) {
        this.high_24h = high_24h;
    }

    public double getLow_24h() {
        return low_24h;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public void setLow_24h(double low_24h) {
        this.low_24h = low_24h;
    }

    public HashMap<String, String> validate() {
        HashMap<String, String> errors = new HashMap<>();
        if (this.bid_price<0  ) {
            errors.put("bid_price", "bid_price is not valid!");
        }
        if (this.ask_price<0) {
            errors.put("ask_price", "ask_price is not valid!");
        }
        if (this.last_price<0) {
            errors.put("last_price", "last_price is not valid!");
        }
        return errors;
    }

    private String getName(String id) {
        String name = "";
        String pair[] = id.split("-");
        String beacon = pair[pair.length - 1];
        switch (beacon) {
            case "ETH":
                name = "Ethereum";
                break;
            case "BTC":
                name = "Bitcoin";
                break;
            case "USDT":
                name = "USDT";
                break;
            case "BCH":
                name = "Bitcoin Cash";
                break;
            case "XRP":
                name = "Ripple";
                break;
            default:
                name = "";
                break;
        }
        return name;
    }
}

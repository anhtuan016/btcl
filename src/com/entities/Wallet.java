package com.entities;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import org.jsoup.Jsoup;

import java.util.HashMap;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Entity
public class Wallet {
    //ID CUA VI TRUNG VOI ID CUA USER / 1 USER CHO CO 1 VI.
    @Id
    private long id;
    @Index
    private String name;
    @Index
    private String type; //"BTC/ETH/BCH/XRP/USDT"
    @Index
    private double amount;
    @Index
    private double amount_pending;
    @Index
    private String userId;
    @Index
    private double percentageChange;
    @Index
    private double est_btc;

    public Wallet() {
        id = System.currentTimeMillis();
        amount = 0;
        amount_pending = 0;
    }

    public double getPercentageChange() {
        return percentageChange;
    }

    public void setPercentageChange(double percentageChange) {
        this.percentageChange = percentageChange;
    }

    public double getEst_btc() {
        return est_btc;
    }

    public void setEst_btc(double est_btc) {
        this.est_btc = est_btc;
    }

    public static String getRegexDouble() {
        return regexDouble;
    }

    public double getAmount_pending() {
        return amount_pending;
    }

    public void setAmount_pending(double amount_pending) {
        this.amount_pending = amount_pending;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private static final String regexDouble = "C=\\d+.\\d+";

    public HashMap<String, String> validate() {
        HashMap<String, String> errors = new HashMap<>();
        if (this.amount < 0) {
            errors.put("amount", "amount is not valid!");
        }
        if (this.amount_pending < 0) {
            errors.put("amount_pending", "amount_pending is not valid!");
        }
        return errors;
    }

    private boolean regex(String strDouble) {
        Pattern pattern = Pattern.compile(regexDouble);
        Matcher matcher = pattern.matcher(strDouble);
        return matcher.matches();
    }

    public boolean estimateWallet() {
//        GET THE LASTEST TYPE-BTC TRANSACTION TO FIND EST.BTC VALUE.
        if ("BTC".equals(this.type)) {
            this.est_btc = this.amount;
            this.percentageChange = 0;
            return true;
        }
        ExchangeRateBTC rate = ofy().load().type(ExchangeRateBTC.class).filter("name", this.type).first().now();
        if (rate == null) return false;


        this.setEst_btc(this.amount * rate.getPrice());
        this.setPercentageChange(rate.getPercent_change_24h());
        return true;
    }

}

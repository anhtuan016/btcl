package com.entities.Enum;

import java.util.HashMap;

public enum CurrenryType {
    ETH(1, "Ethereum"),
    BTC(2, "Bitcoin"),
    XRP(3, "Ripple"),
    BCH(4, "Bitcoin Cash"),
    USDT(5, "Tether");

    private final int number;
    private final String currenry;

    CurrenryType(int number, String currenry) {
        this.number = number;
        this.currenry = currenry;

    }

    public int getNumber() {
        return number;
    }

    public String getCurrenry() {
        return currenry;
    }

    public static String getCurrery(int number) {
        for (CurrenryType type : CurrenryType.values()) {
            if (type.number == number) {
                return type.getCurrenry();
            }
        }
        return null;
    }
    public static String getSymbol(int number) {
        for (CurrenryType type : CurrenryType.values()) {
            if (type.number == number) {
                return type.toString();
            }
        }
        return null;
    }

    public static int getint(String number) {
        for (CurrenryType type : CurrenryType.values()) {
            if (type.currenry.equals(number)) {
                return type.number;
            }
        }
        return 0;
    }

}

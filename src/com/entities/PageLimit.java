package com.entities;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PageLimit {
    private int page;
    private int limit;
    private int totalPage;
    private int totalItem;
    private int from;
    private int to;

    public PageLimit(int page,int limit,int count){
        this.page=page;
        this.limit=limit;
        totalPage=count/limit;
        if (count%limit!=0)totalPage+=1;
        from=(page-1)*limit;
        to=from+limit;
        if (page==totalPage)to=count;
        totalItem=count;
        if (this.page==0)this.from=this.to=0;
        if (this.totalItem==0){
            this.page=0;
            this.limit=0;
            this.totalPage=0;
            this.totalItem=0;
            this.from=0;
            this.to=0;
        }
        if (page>totalPage) page=totalPage;

    }
    public PageLimit(){
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(int totalItem) {
        this.totalItem = totalItem;
    }

    private boolean check(int page,int totalPage){
        return page<=totalPage;
    }


    private static final  String regex = "/^([+-]?[1-9]\\d*|0)$/";

    public HashMap<String, String> validate() {
        HashMap<String, String> errors = new HashMap<>();
        if (this.page < 0 || regexInteger(String.valueOf(this.page)) ){
            errors.put("page", "page is not valid!");
        }
        if (this.limit< 0 || regexInteger(String.valueOf(this.limit)) ){
            errors.put("limit", "limit is not valid!");
        }
        if (this.totalPage< 0 || regexInteger(String.valueOf(this.totalPage)) ){
            errors.put("totalPage", "totalPage is not valid!");
        }
        if (this.totalItem< 0 || regexInteger(String.valueOf(this.totalItem)) ){
            errors.put("totalItem", "totalItem is not valid!");
        }
        return errors;
    }

    private boolean regexInteger(String number) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(number);
        return matcher.matches();
    }
}

package com.entities;

import com.entities.Enum.CurrenryType;

public enum  AddressType {
    ETH(1),
    BTC(2),
    XRP(3),
    BCH(4),
    USDT(5);
    private final int number;
    private AddressType(final int number) {this.number=number;}
    public  int toInt(){return number;}

    public static String getSymbol(int number) {
        for (AddressType type : AddressType.values()) {
            if (type.number == number) {
                return type.toString();
            }
        }
        return null;
    }

    public static int getType(String type) {
        for (AddressType AddressType : AddressType.values()) {
            if (AddressType.toString().equalsIgnoreCase(type)) {
                return AddressType.number;
            }
        }
        return 0;
    }
    public static String getStringType(String type) {
        for (AddressType AddressType : AddressType.values()) {
            if (AddressType.toString().equalsIgnoreCase(type)) {
                return AddressType.toString();
            }
        }
        return null;
    }
}

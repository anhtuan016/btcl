package com.entities;

import com.google.gson.annotations.Expose;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.util.Generate;

import java.util.UUID;

@Entity
public class CandleStickItem30m {
    @Id
    String id;
    @Index
    @Expose
    String date;
    @Index
    @Expose
    String open;
    @Index
    @Expose
    String close;
    @Index
    @Expose
    String high;
    @Index
    @Expose
    String low;
    @Index
    long timelong;
    @Index
    String pair;

    public CandleStickItem30m() {

    }

    public long getTimelong() {
        return timelong;
    }

    public void setTimelong(long timelong) {
        this.timelong = timelong;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CandleStickItem30m(long timelong, String date, String pair) {
        this.date = date;
        this.timelong = timelong;
        this.pair = pair;
        this.id = String.valueOf(timelong) + pair;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }
}
